from django.apps import apps
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path
from django.views.static import serve

urlpatterns = []

if settings.DEBUG:
    if settings.MEDIA_ROOT:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    if settings.STATIC_FILES:
        urlpatterns += [
            re_path(
                '^(?P<path>{})$'.format('|'.join(settings.STATIC_FILES)),
                serve,
                {'document_root': settings.STATIC_ROOT},
            )
        ]

if apps.is_installed('pixel9.apps.development'):
    urlpatterns.append(path('-dev/', include('pixel9.apps.development.urls')))

if apps.is_installed('pixel9.apps.admin') or apps.is_installed('django.contrib.admin'):
    from django.contrib import admin

    urlpatterns.append(path('-admin/', admin.site.urls))

if apps.is_installed('pixel9.apps.tests'):
    from pixel9.apps.tests import urls as tests_urls

    urlpatterns.append(path('-tests/', include(tests_urls)))
