import logging

from django.utils.termcolors import colorize

COLORS = {
    'DEBUG': 'white',
    'INFO': 'blue',
    'WARNING': 'yellow',
    'ERROR': 'red',
    'CRITICAL': 'red',
}


class ColorizeFormatter(logging.Formatter):
    def format(self, record):
        format = logging.Formatter.format(self, record)
        opts = []
        if record.levelname in ('CRITICAL', 'DEBUG'):
            opts.append('bold')
        return colorize(format, fg=COLORS.get(record.levelname, 'white'), opts=opts)
