import re
from decimal import Decimal


def remove_exponent(d):
    """Remove exponent and trailing zeros.

    >>> remove_exponent(Decimal('5E+3'))
    Decimal('5000')

    """
    return d.quantize(Decimal(1)) if d == d.to_integral() else d.normalize()


def format_price(value):
    """
    Do not display trailing zeros after 2 decimal places
    """
    value = remove_exponent(value)

    if '.' not in str(value) or re.match(r'\d+\.\d$', str(value)):
        return value.quantize(Decimal('0.00'))
    return value
