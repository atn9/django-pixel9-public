from django.apps import apps


def enqueue_or_call(func, queue_name='default', **kwargs):
    if apps.is_installed('pixel9.apps.queues'):
        from django_rq import get_queue

        get_queue(queue_name).enqueue_call(func=func, **kwargs)
    else:
        _args = kwargs.get('args', [])
        _kwargs = kwargs.get('kwargs' or {})
        func(*_args, **_kwargs)
