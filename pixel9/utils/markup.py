import hashlib
import re

from markdown import markdown


def pre_process_markdown(text):
    # Extract pre and textarea blocks
    extractions = {}

    def extraction_callback(matchobj):
        sha1 = hashlib.sha1(matchobj.group(0)).hexdigest()  # noqa: S324
        extractions[sha1] = matchobj.group(0)
        return '{extraction-%s}' % sha1

    text = re.sub(
        re.compile(r'<pre>.*?</pre>|<textarea>.*?</textarea>', re.MULTILINE | re.DOTALL),
        extraction_callback,
        text,
    )

    # in very clear cases, let newlines become <br /> tags
    def newline_callback(matchobj):
        if len(matchobj.group(1)) == 1:
            return matchobj.group(0).rstrip() + '  \n'
        else:
            return matchobj.group(0)

    text = re.sub(r'[\w\<][^\n]*(\n+)', newline_callback, text)

    # re insert pre and textarea blocks
    text = re.sub(r'{extraction-([0-9a-f]{40})\}', lambda m: extractions[m.group(1)], text)

    return text


# def replace_hr(text, replacement='<div class="hr">&nbsp;</div>'):
# return  re.sub(r'<hr\s?\/?>', replacement, text)


def custom_markdown(text, **kwargs):
    return markdown(pre_process_markdown(text), output_format='html4', **kwargs)
