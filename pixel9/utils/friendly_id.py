"""
really simplified version of `friendly_id` from
http://code.google.com/p/rollyourown/source/browse/trunk/rollyourown/commerce/utils/friendly_id.py
"""
import math

# Alpha numeric characters, only uppercase, no confusing values (eg 1/I,0/O,Z/2)
# Remove some letters if you prefer more numbers in your strings
# You may wish to remove letters that sound similar, to avoid confusion when a
# customer calls on the phone (B/P, M/N, 3/C/D/E/G/T/V)
VALID_CHARS = '3456789AFGHJKLRSUWXY'


def get_period(size):
    highest_acceptable_factor = int(math.sqrt(size))
    for p in range(13, 7, -1):
        if size % p == 0:
            return p
    for p in range(highest_acceptable_factor, 13 + 1, -1):
        if size % p == 0:
            return p
    for p in [6, 5, 4, 3, 2]:
        if size % p == 0:
            return p
    raise ValueError('No valid period could be found for size=%d.\n' 'Try avoiding prime numbers' % size)


def perfect_hash(num, size):
    period = get_period(size)
    return ((num + (size * 30 / 100 - 1)) * (size / period)) % (size + 1) + 1


def friendly_id(num, valid_chars=VALID_CHARS, minimum_length=3):
    num = int(num)
    if num < 0:
        raise ValueError('"friendly_id" need a positive integers as the first argument')
    size = len(valid_chars) ** minimum_length - 1
    while num > size:
        size *= len(valid_chars)

    num = perfect_hash(num, size)
    string = ''
    while len(valid_chars) ** len(string) <= size:
        # Prepend string (to remove all obvious signs of order)
        string = valid_chars[math.floor(num % len(valid_chars))] + string
        num = num / len(valid_chars)
    return string


def test(max_number):
    # Check all strings are unique
    from collections import defaultdict

    a = defaultdict()
    for i in range(max_number + 1):
        if friendly_id(i) in a:
            return False
        a[friendly_id(i)] = i

    return True
