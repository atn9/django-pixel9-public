import json
import re

from django.utils.html import conditional_escape, format_html, strip_tags
from django.utils.safestring import mark_safe

invalid_tags_re = re.compile(r'<(script|style|form|select)[^>]*>.*</\1>', re.S)


def custom_strip_tags(text):
    return strip_tags(invalid_tags_re.sub('', text).strip())


def meta_description_filter(text, length=254):
    return conditional_escape(re.sub(r'[\n\r\t\s]+', ' ', custom_strip_tags(text)).strip()[0:length])


_json_script_escapes = {
    ord('>'): '\\u003E',
    ord('<'): '\\u003C',
    ord('&'): '\\u0026',
}


def json_script(value, element_id):
    """
    Escape all the HTML/XML special characters with their unicode escapes, so
    value is safe to be output anywhere except for inside a tag attribute. Wrap
    the escaped JSON in a script tag.
    """
    from django.core.serializers.json import DjangoJSONEncoder

    json_str = json.dumps(value, cls=DjangoJSONEncoder)
    return format_html(
        '<script id="{}" type="application/json">{}</script>',
        element_id,
        mark_safe(json_str),
    )
