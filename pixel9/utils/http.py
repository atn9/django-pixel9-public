import json
import unicodedata
from urllib.parse import parse_qs, urlencode, urlsplit, urlunsplit

from django import http
from django.conf import settings
from django.utils.encoding import smart_str


def localize_url(url, language):
    if len(settings.LANGUAGES) > 1 and settings.USE_I18N:
        from localeurl.templatetags.localeurl_tags import chlocale

        url = chlocale(url, language)
    return url


def remove_accents(str):
    """
    Replace accented characters with their non-accented equivalent
    - not tested -
    in:  ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ
    out: AAAAAAaaaaaaOOOOOoooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn
    """
    cleaned_str = unicodedata.normalize('NFKD', smart_str(str)).encode('ascii', 'ignore')
    return smart_str(cleaned_str)


def jsonp_view(func):
    def wrap(request, *args, **kwargs):
        response = func(request, *args, **kwargs)
        if isinstance(response, dict) or isinstance(response, list):
            _json = json.dumps(response)
            callback = request.GET.get('jsoncallback')
            if not callback:
                raise http.Http404
            return http.HttpResponse(callback + '(' + _json + ')', content_type='application/json')
        else:
            return response

    return wrap


# https://stackoverflow.com/questions/6480723/urllib-urlencode-doesnt-like-unicode-values-how-about-this-workaround
def encode_obj(in_obj):
    def encode_list(in_list):
        out_list = []
        for el in in_list:
            out_list.append(encode_obj(el))
        return out_list

    def encode_dict(in_dict):
        out_dict = {}
        for k, v in in_dict.items():
            out_dict[k] = encode_obj(v)
        return out_dict

    if isinstance(in_obj, str):
        return in_obj.encode('utf-8')
    elif isinstance(in_obj, list):
        return encode_list(in_obj)
    elif isinstance(in_obj, tuple):
        return tuple(encode_list(in_obj))
    elif isinstance(in_obj, dict):
        return encode_dict(in_obj)

    return in_obj


def set_query_parameter(url, param_name, param_value):
    """Given a URL, set or replace a query parameter and return the
    modified URL.

    >>> set_query_parameter('http://example.com?foo=bar&biz=baz', 'foo', 'stuff')
    'http://example.com?foo=stuff&biz=baz'

    """
    scheme, netloc, path, query_string, fragment = urlsplit(url)
    query_params = parse_qs(query_string)

    query_params[param_name] = [param_value]
    new_query_string = urlencode(encode_obj(query_params), doseq=True)

    return urlunsplit((scheme, netloc, path, new_query_string, fragment))


def set_query_parameters(url, params):
    for k, v in list(params.items()):
        url = set_query_parameter(url, k, v)
    return url


def remove_query_parameter(url, param_name):
    scheme, netloc, path, query_string, fragment = urlsplit(url)
    query_params = parse_qs(query_string)
    if param_name in query_params:
        del query_params[param_name]
    new_query_string = urlencode(encode_obj(query_params), doseq=True)
    return urlunsplit((scheme, netloc, path, new_query_string, fragment))


def remove_query_parameters(url, params):
    for param in params:
        url = remove_query_parameter(url, param)
    return url
