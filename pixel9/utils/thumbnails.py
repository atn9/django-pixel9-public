import os
from pathlib import Path

from django.core.files.storage import FileSystemStorage
from sorl.thumbnail.base import ThumbnailBackend
from sorl.thumbnail.conf import settings
from sorl.thumbnail.helpers import serialize, tokey

EXTENSIONS = {
    'JPEG': 'jpg',
    'PNG': 'png',
    'GIF': 'gif',
    'WEBP': 'webp',
}


class CompatThumbnailBackendMixin:
    def _get_thumbnail_filename(self, source, geometry_string, options):
        return '{}{}.{}.{}'.format(
            settings.THUMBNAIL_PREFIX,
            Path(source.name).stem,
            tokey(source.key, geometry_string, serialize(options))[:6],
            EXTENSIONS.get(options['format'], options['format']),
        )


class CustomThumbnailBackend(CompatThumbnailBackendMixin, ThumbnailBackend):
    pass


class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        """Returns a filename that's free on the target storage system, and
        available for new content to be written to.

        Found at http://djangosnippets.org/snippets/976/

        This file storage solves overwrite on upload problem. Another
        proposed solution was to override the save method on the model
        like so (from https://code.djangoproject.com/ticket/11663):

        def save(self, *args, **kwargs):
            try:
                this = MyModelName.objects.get(id=self.id)
                if this.MyImageFieldName != self.MyImageFieldName:
                    this.MyImageFieldName.delete()
            except: pass
            super(MyModelName, self).save(*args, **kwargs)
        """
        # If the filename already exists, remove it as if it was a true file system
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))  # noqa: PTH107, PTH118
        return name
