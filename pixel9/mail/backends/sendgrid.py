from django.conf import settings
from django.core.mail.backends import smtp


class SMTPBackend(smtp.EmailBackend):
    def __init__(self, host=None, port=None, username=None, password=None, use_tls=None, fail_silently=False, **kwargs):
        host = host or 'smtp.sendgrid.net'
        port = 587
        use_tls = True
        username = username or settings.SENDGRID_USERNAME
        password = password or settings.SENDGRID_PASSWORD
        super().__init__(host, port, username, password, use_tls, fail_silently, **kwargs)

    def _send(self, email_message):
        return super()._send(email_message)
