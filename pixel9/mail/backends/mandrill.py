from django.conf import settings
from django_mandrill.mail.backends.mandrillbackend import (
    EmailBackend as MandrillEmailBackend,
)


class EmailBackend(MandrillEmailBackend):
    def _set_default_headers(self, message):
        for k, v in list(getattr(settings, 'MANDRILL_DEFAULT_HEADERS', {}).items()):
            message.extra_headers.setdefault(k, v)

    def _set_default_args(self, message):
        for k, v in list(getattr(settings, 'MANDRILL_DEFAULT_ARGS', {}).items()):
            message.mandrill_args.setdefault(k, v)

    def _send_raw(self, message):
        self._set_default_headers(message)
        return super()._send_raw(message)

    def _send_template(self, message):
        self._set_default_args(message)
        return super()._send_template(message)

    def _send_message(self, message):
        self._set_default_args(message)
        return super()._send_message(message)
