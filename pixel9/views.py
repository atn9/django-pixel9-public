from django import http
from django.conf import settings
from django.utils.http import url_has_allowed_host_and_scheme

from pixel9.apps.pages.utils import get_page


class RedirectableMixin:
    redirect_field_name = 'next'

    def get_redirect_url(self):
        redirect_url = self.request.POST.get(
            self.redirect_field_name,
            self.request.GET.get(self.redirect_field_name, self.request.headers.get('referer')),
        )
        if redirect_url and url_has_allowed_host_and_scheme(redirect_url, allowed_hosts=[self.request.get_host()]):
            return redirect_url
        return self.get_default_redirect_url()

    def get_default_redirect_url(self):
        return self.request.get_full_path()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context[self.redirect_field_name] = self.get_redirect_url()
        return context


# TODO register keys and add a dropdown menu in pages admin pages
class PageMixin:
    page_key = None

    def get_page_key(self):
        return self.page_key

    def get_page(self):
        key = self.get_page_key()
        if 'pixel9.apps.pages' not in settings.INSTALLED_APPS or not key:
            return None
        return get_page(key)

    def get_default_page_title(self):
        return ''

    def get_default_page_content(self):
        return ''

    def get_default_page(self):
        return {
            'title': self.get_default_page_title(),
            'content': self.get_default_page_content(),
        }

    def dispatch(self, request, *args, **kwargs):
        page = self.get_page()
        if page is None:
            page = self.get_default_page()
        self.page = page
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page'] = self.page
        return context


def ratelimited(request, exception):
    return http.HttpResponseForbidden('Limite atteinte - Limit exceeded')
