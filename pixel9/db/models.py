from django.conf import settings
from django.db import models
from django.db.models import *  # noqa: F403
from django.db.models.signals import (
    m2m_changed,
    post_delete,
    post_save,
    pre_delete,
    pre_save,
)
from django.utils import translation
from treebeard import al_tree

from pixel9.db.fields import *  # noqa: F403


class TreeModel(al_tree.AL_Node):
    # doesn't work with node_order_by
    sib_order = models.PositiveIntegerField()
    # IMPORTANT all AL_Node subclass need this
    # parent = models.ForeignKey('self', related_name='children_set', null=True, db_index=True)

    class Meta:
        abstract = True
        # IMPORTANT all NS_Node subclass need this
        # ordering = ['tree_id', 'lft']
        # IMPORTANT all MP_Node subclass need this
        # ordering = ['path']

    @classmethod
    def get_or_create_first_root_node(cls):
        """
        Implement this in subclasses to be sure all required fields are provided
        example:
          root = cls.get_first_root_node()
          if root is None:
              root = cls.add_root(name='ROOT_NODE')
          return root
        """
        raise NotImplementedError


class MultilingualModelMixin:
    @classmethod
    def field_is_multilingual(cls, name):
        lang = translation.get_language() or settings.LANGUAGE_CODE
        lang = lang[:2]
        if f'{name}_{lang}' in [f.name for f in cls._meta.fields]:
            return True

    def __getattr__(self, name):
        lang = translation.get_language() or settings.LANGUAGE_CODE
        lang = lang[:2]
        if self.field_is_multilingual(name):
            return getattr(self, f'{name}_{lang}')
        return super().__getattr__(name)


def fix_proxy_model_signals(proxy_model, base_model):
    # Signals connected to the parents of proxy models are not fired
    # when the proxy models are saved or deleted.
    # https://code.djangoproject.com/ticket/9318#comment:37
    signals = [
        pre_save,
        post_save,
        pre_delete,
        post_delete,
        m2m_changed,
    ]

    def make_sender_fn(model, signal):
        def sender_fn(sender, **kwargs):
            # signal send method passes itself
            # as a signal kwarg to its receivers
            kwargs.pop('signal')
            signal.send(sender=model, **kwargs)

        return sender_fn

    for signal in signals:
        signal.connect(
            make_sender_fn(base_model, signal),
            sender=proxy_model,
            weak=False,
        )
