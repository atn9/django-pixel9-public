from: https://github.com/ui/django-rq

* Install requirements

    pip install -r ../src/django-pixel9/pixel9/apps/queue/requirements.txt

* Add ``pixel9.apps.queues`` to ``INSTALLED_APPS`` in ``settings.py``:

.. code-block:: python

    INSTALLED_APPS = (
        # other apps
        "pixel9.apps.queues",
    )

* Configure your queues in django's ``settings.py`` (syntax based on Django's database config):

    RQ_QUEUES = {
        'default': {
            'HOST': 'localhost',
            'PORT': 6379,
            'DB': 0,
            'PASSWORD': 'some-password',
            'DEFAULT_TIMEOUT': 360,
        },
        'high': {
            'URL': os.getenv('REDISTOGO_URL', 'redis://localhost:6379'), # If you're on Heroku
            'DB': 0,
            'DEFAULT_TIMEOUT': 500,
        },
        'low': {
            'HOST': 'localhost',
            'PORT': 6379,
            'DB': 0,
            'ASYNC': True,
        }
    }

Support for django-redis and django-redis-cache
-----------------------------------------------

.. code-block:: python

    CACHES = {
        'redis-cache': {
            'BACKEND': 'redis_cache.cache.RedisCache',
            'LOCATION': 'localhost:6379:1',
            'OPTIONS': {
                'CLIENT_CLASS': 'redis_cache.client.DefaultClient',
                'MAX_ENTRIES': 5000,
            },
        },
    }

    RQ_QUEUES = {
        'high': {
            'USE_REDIS_CACHE': 'redis-cache',
        },
        'low': {
            'USE_REDIS_CACHE': 'redis-cache',
        },
    }


Running workers
---------------

    python manage.py worker high default low

If you want to run ``worker`` in burst mode, you can pass in the ``--burst`` flag::

    python manage.py worker high default low --burst
