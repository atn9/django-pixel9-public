import shlex
import subprocess
from pathlib import Path

from django.utils.autoreload import run_with_reloader

from . import worker


class Command(worker.Command):
    def run_from_argv(self, argv):
        self.argv = argv
        super().run_from_argv(argv)

    def handle(self, *args, **options):
        run_with_reloader(run_worker, self.argv, *args, **options)


def run_worker(argv, *args, **options):
    pid = options.get('pid')
    if pid:
        if Path(pid).exists():
            worker_pid = subprocess.run(['cat', pid], stdout=subprocess.PIPE).stdout.decode('utf-8')  # noqa: S603, S607
            subprocess.run(shlex.split(f'kill {worker_pid}'), stderr=subprocess.PIPE)  # noqa: S603
    # replace 'devworker' with 'worker'
    argv[1] = 'worker'
    subprocess.run(argv)  # noqa: S603
