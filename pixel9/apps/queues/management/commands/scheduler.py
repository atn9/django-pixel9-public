from django.conf import settings
from django_rq.management.commands import rqscheduler

from ...cron import cron


class Command(rqscheduler.Command):
    def handle(self, *args, **options):
        if options.get('queue') == settings.CRON_SCHEDULER_QUEUE:
            cron.schedule_jobs()
        return super().handle(*args, **options)
