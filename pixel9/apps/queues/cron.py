import logging

from django.conf import settings
from django_rq import get_scheduler

logger = logging.getLogger(__name__)


class Cron:
    # if the prefix is changed, previously scheduled cron jobs
    # will have to be deleted manually
    PREFIX = '[CRON]'

    def __init__(self, queue):
        self.scheduler = get_scheduler(name=queue)
        self.jobs = []

    def add(self, cron_string, **kwargs):
        self.jobs.append((cron_string, kwargs))

    def delete_jobs(self):
        for job in self.scheduler.get_jobs():
            if job.description.startswith(self.PREFIX):
                job.delete()

    def schedule_job(self, cron_string, **kwargs):
        # add a prefix to the description of the job to keep track of jobs
        # scheduled by this class and be able delete them later
        kwargs['description'] = '{} {}'.format(self.PREFIX, kwargs.get('description', repr(kwargs['func'])))
        logger.info('Scheduling cron job: {} {}'.format(cron_string, kwargs['description']))

        kwargs.setdefault('use_local_timezone', True)
        self.scheduler.cron(cron_string, **kwargs)

    def schedule_jobs(self):
        self.delete_jobs()
        for cron_string, kwargs in self.jobs:
            self.schedule_job(cron_string, **kwargs)


cron = Cron(settings.CRON_SCHEDULER_QUEUE)
