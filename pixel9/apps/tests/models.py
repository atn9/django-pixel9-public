from django.utils.safestring import mark_safe
from positions.managers import PositionManager

from pixel9.db import models


class AllField(models.Model):
    'A model with all field type'

    id = models.AutoField(primary_key=True)
    bool = models.BooleanField(default=False)
    char = models.CharField(
        max_length=255,
    )
    date = models.DateField()
    datetime = models.DateTimeField()
    decimal = models.DecimalField(max_digits=10, decimal_places=2)
    email = models.EmailField(null=True, blank=True)
    file = models.FileField(upload_to='files', null=True, blank=True)
    filepath = models.FilePathField(path='/tmp/', null=True, blank=True)  # noqa: S108
    float = models.FloatField(null=True, blank=True)
    foreign = models.ForeignKey(
        'Simple',
        related_name='foreign',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )
    image = models.ImageField(upload_to='images', null=True, blank=True)
    integer = models.IntegerField(null=True, blank=True)
    ip = models.GenericIPAddressField(null=True, blank=True)
    many = models.ManyToManyField('Simple', related_name='many', blank=True)
    one = models.OneToOneField('Simple', related_name='one', null=True, blank=True, on_delete=models.SET_NULL)
    positive = models.PositiveIntegerField(null=True, blank=True)
    possmall = models.PositiveSmallIntegerField(null=True, blank=True)
    slug = models.SlugField(null=True, blank=True)
    small = models.SmallIntegerField(null=True, blank=True)
    text = models.TextField(blank=True)
    time = models.TimeField()
    url = models.URLField(null=True, blank=True)
    position = models.PositionField()
    choice = models.CharField(
        max_length=100,
        choices=(
            ('1', 'choix 1'),
            ('2', 'loooooooong long loooooooooooog choix 2'),
            ('3', 'choix 3'),
        ),
        blank=True,
        default='',
    )

    objects = PositionManager()

    class Meta:
        ordering = ('position',)

    def __str__(self):
        return self.char


class Tree(models.TreeModel):
    name = models.CharField(max_length=100)
    many = models.ManyToManyField('Simple', blank=True)
    # IMPORTANT all AL_Node subclass need this
    parent = models.ForeignKey(
        'self',
        related_name='children_set',
        null=True,
        db_index=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        ordering = ('sib_order',)

    @classmethod
    def get_or_create_first_root_node(cls):
        root = cls.get_first_root_node()
        if root is None:
            root = cls.add_root(name='ROOT_NODE')
        return root

    def __str__(self):
        return self.name


class Upload(models.Model):
    file = models.FileField('aaaa', upload_to='files', help_text='aaaaa aaaa a a aaa', null=True, blank=True)
    image = models.ImageField(upload_to='images', null=True, blank=True)

    def raw_id_field_html_repr(self):
        return mark_safe(f'<img class="img-responsive img-fluid" src="{self.image.url}">')


class Simple(models.Model):
    name = models.CharField(max_length=100)
    name2 = models.CharField(max_length=100, help_text='Ceci est le 2e nom, ...')
    allfields = models.ForeignKey('AllField', related_name='simples', blank=True, on_delete=models.CASCADE)
    allfields2 = models.ManyToManyField('AllField', related_name='simples2', blank=True)
    position = models.PositionField(collection='allfields')
    upload = models.ForeignKey('Upload', null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('position',)


class TestAddPermissions(models.Model):
    pass
