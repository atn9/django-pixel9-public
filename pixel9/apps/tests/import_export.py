from import_export import resources

from .models import AllField


class AllFieldResource(resources.ModelResource):
    class Meta:
        model = AllField
        report_skipped = False
        skip_unchanged = True
