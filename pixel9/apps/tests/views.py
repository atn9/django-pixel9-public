from django.shortcuts import render


def files(request):
    return render(request, 'tests/files.html', {})
