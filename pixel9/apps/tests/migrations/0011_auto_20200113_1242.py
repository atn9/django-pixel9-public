# Generated by Django 2.2.7 on 2020-01-13 17:42

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('tests', '0010_auto_20191114_1143'),
    ]

    operations = [
        migrations.AddField(
            model_name='simple',
            name='upload',
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to='tests.Upload',
            ),
        ),
        migrations.AlterField(
            model_name='allfield',
            name='filepath',
            field=models.FilePathField(blank=True, null=True, path='/tmp/'),  # noqa: S108
        ),
    ]
