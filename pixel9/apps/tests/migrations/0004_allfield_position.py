from django.db import migrations

import pixel9.db.fields


class Migration(migrations.Migration):
    dependencies = [
        ('tests', '0003_testaddpermissions'),
    ]

    operations = [
        migrations.AddField(
            model_name='allfield',
            name='position',
            field=pixel9.db.fields.PositionField(default=-1),
            preserve_default=True,
        ),
    ]
