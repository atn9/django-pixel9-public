from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('tests', '0002_simplerelated'),
    ]

    operations = [
        migrations.CreateModel(
            name='TestAddPermissions',
            fields=[
                (
                    'id',
                    models.AutoField(
                        verbose_name='ID',
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
    ]
