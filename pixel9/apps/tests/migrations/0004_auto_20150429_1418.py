import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('tests', '0003_testaddpermissions'),
    ]

    operations = [
        migrations.AlterField(
            model_name='allfield',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='allfield',
            name='ip',
            field=models.GenericIPAddressField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='allfield',
            name='many',
            field=models.ManyToManyField(related_name='many', to='tests.Simple', blank=True),
        ),
        migrations.AlterField(
            model_name='allfield',
            name='one',
            field=models.OneToOneField(
                related_name='one',
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                blank=True,
                to='tests.Simple',
            ),
        ),
        migrations.AlterField(
            model_name='simple',
            name='allfields2',
            field=models.ManyToManyField(related_name='simples2', to='tests.AllField', blank=True),
        ),
        migrations.AlterField(
            model_name='tree',
            name='many',
            field=models.ManyToManyField(to='tests.Simple', blank=True),
        ),
    ]
