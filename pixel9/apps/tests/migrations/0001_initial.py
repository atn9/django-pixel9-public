from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='AllField',
            fields=[
                ('auto', models.AutoField(serialize=False, primary_key=True)),
                ('bool', models.BooleanField(default=False)),
                ('char', models.CharField(max_length=255)),
                (
                    'comma',
                    models.CommaSeparatedIntegerField(max_length=100, null=True, blank=True),
                ),
                ('date', models.DateField()),
                ('datetime', models.DateTimeField()),
                ('decimal', models.DecimalField(max_digits=10, decimal_places=2)),
                ('email', models.EmailField(max_length=75, null=True, blank=True)),
                ('file', models.FileField(null=True, upload_to='files', blank=True)),
                (
                    'filepath',
                    models.FilePathField(path='/tmp/django_tests/', null=True, blank=True),  # noqa: S108
                ),
                ('float', models.FloatField(null=True, blank=True)),
                ('image', models.ImageField(null=True, upload_to='images', blank=True)),
                ('integer', models.IntegerField(null=True, blank=True)),
                ('ip', models.IPAddressField(null=True, blank=True)),
                ('nullbool', models.NullBooleanField()),
                ('positive', models.PositiveIntegerField(null=True, blank=True)),
                ('possmall', models.PositiveSmallIntegerField(null=True, blank=True)),
                ('slug', models.SlugField(null=True, blank=True)),
                ('small', models.SmallIntegerField(null=True, blank=True)),
                ('text', models.TextField(blank=True)),
                ('time', models.TimeField()),
                ('url', models.URLField(null=True, blank=True)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Simple',
            fields=[
                (
                    'id',
                    models.AutoField(
                        verbose_name='ID',
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ('name', models.CharField(max_length=100)),
                (
                    'name2',
                    models.CharField(help_text='Ceci est le 2e nom, ...', max_length=100),
                ),
                (
                    'allfields',
                    models.ForeignKey(
                        related_name='simples',
                        to='tests.AllField',
                        on_delete=models.CASCADE,
                    ),
                ),
                (
                    'allfields2',
                    models.ManyToManyField(related_name='simples2', to='tests.AllField'),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tree',
            fields=[
                (
                    'id',
                    models.AutoField(
                        verbose_name='ID',
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ('sib_order', models.PositiveIntegerField()),
                ('name', models.CharField(max_length=100)),
                (
                    'many',
                    models.ManyToManyField(to='tests.Simple', null=True, blank=True),
                ),
                (
                    'parent',
                    models.ForeignKey(
                        related_name='children_set',
                        to='tests.Tree',
                        null=True,
                        on_delete=models.SET_NULL,
                    ),
                ),
            ],
            options={
                'ordering': ('sib_order',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Upload',
            fields=[
                (
                    'id',
                    models.AutoField(
                        verbose_name='ID',
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    'file',
                    models.FileField(
                        help_text='aaaaa aaaa a a aaa',
                        upload_to='files',
                        null=True,
                        verbose_name='aaaa',
                        blank=True,
                    ),
                ),
                ('image', models.ImageField(null=True, upload_to='images', blank=True)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='allfield',
            name='foreign',
            field=models.ForeignKey(
                related_name='foreign',
                blank=True,
                to='tests.Simple',
                null=True,
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='allfield',
            name='many',
            field=models.ManyToManyField(related_name='many', null=True, to='tests.Simple', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='allfield',
            name='one',
            field=models.OneToOneField(
                related_name='one',
                null=True,
                blank=True,
                to='tests.Simple',
                on_delete=models.SET_NULL,
            ),
            preserve_default=True,
        ),
    ]
