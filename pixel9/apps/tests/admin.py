from django import http
from django.contrib import admin
from django.urls import NoReverseMatch, reverse, reverse_lazy
from django.utils.safestring import mark_safe
from import_export.admin import ImportExportActionModelAdmin

from pixel9 import forms
from pixel9.apps.admin import filters

# from pixel9.apps.images.models import Image
from pixel9.apps.admin.options import RelatedModelAdminMixin, TreeModelAdmin
from pixel9.db.models import Avg, Max, Min, StdDev, Sum

# from pixel9.apps.pdf_thumbnails.widgets import AdminPdfWidget
# from pixel9.apps.pdf_thumbnails.fields import PdfField
from . import models
from .import_export import AllFieldResource


class InlineSimple(admin.StackedInline):
    model = models.Simple
    fields = ('name', 'name2', 'allfields', 'allfields2')
    autocomplete_fields = (
        'allfields',
        'allfields2',
    )
    show_original = False
    manual_ordering_field = 'position'


class InlineSimpleTabular(admin.TabularInline):
    model = models.Simple
    fields = ('name', 'name2', 'allfields', 'allfields2')
    autocomplete_fields = (
        'allfields',
        'allfields2',
    )
    show_original = False
    manual_ordering_field = 'position'
    readonly_fields = ('name2',)

    def get_column_align(self, column):
        if column in ('name2',):
            return 'center'


class InlineSimpleTabularGrid(InlineSimpleTabular):
    template = 'admin/edit_inline/tabular_grid.html'


class SimpleAdmin(admin.ModelAdmin):
    model = models.Simple
    search_fields = ('name',)
    list_filter = (
        'name2',
        'allfields',
    )
    list_display = (
        'name',
        'name2',
    )
    actions = ['delete_selected', 'test_action']
    raw_id_fields = ('upload',)
    autocomplete_fields = (
        'allfields',
        'allfields2',
    )

    fieldsets = (
        (
            None,
            {
                'fields': ('name', 'allfields', 'upload'),
            },
        ),
        (
            'Test',
            {
                'fields': (
                    'name2',
                    'allfields2',
                ),
            },
        ),
    )

    custom_views = (
        'custom',
        {
            'name': 'custom2',
            'method_name': 'custom2_view',
            'url': r'^custom2/$',
            'perms': ('test.change_simple',),
        },
    )

    def custom_view(self, request, object_id=None):
        return {'redirect': reverse('admin:index')}

    def custom2_view(self, request):
        return {'alert': 'yeah2!'}

    def test_action(self, request, queryset):
        return http.HttpResponse('<p>ahhhhhhhhhhhhhh!</p>')


class SimpleRelated(models.Simple):
    class Meta:
        proxy = True


class SimpleRelatedAdmin(RelatedModelAdminMixin, SimpleAdmin):
    rel = 'allfields'
    list_display = ('name', 'name2')
    list_editable = ('name2',)
    list_filter = []
    list_search = []
    custom_views = ('test',)
    redirect_to_related_on_delete = True

    def test_view(self, request, related_id, object_id=None):
        return http.HttpResponse(related_id)


class CharFilter(filters.InputFilter):
    title = 'Char'
    parameter_name = 'char'

    def queryset(self, request, queryset):
        if self.value() is not None:
            value = self.value()
            return queryset.filter(char=value)


class ForeignFilter(filters.AutocompleteFilter):
    title = 'Foreign'  # display title
    field_name = 'foreign'  # name of the foreign key field
    rel_model = models.AllField


class DateFilter(filters.DateFieldFilter):
    title = 'Date2'
    field_name = 'date2'
    parameter_name = 'date2'


class AllFieldAdmin(ImportExportActionModelAdmin, admin.ModelAdmin):
    model = models.AllField
    search_fields = ('char',)
    # raw_id_fields = ('foreign',)
    list_display = ('char', 'bool', 'email', 'date', 'foreign')
    list_display_hidden = ('decimal',)
    list_filter = (
        'email',
        CharFilter,
        ForeignFilter,
        'date',
        ('decimal', filters.NumericFilter),
        filters.AnnotationNumericFilter.init('manysum'),
    )
    list_per_page = 10
    inlines = [InlineSimpleTabularGrid]
    list_editable = ('email', 'date', 'foreign')
    readonly_fields = ('slug', 'related_link')
    actions = [
        'delete_selected',
    ]
    actions_on_top = True
    radio_fields = {'choice': admin.VERTICAL}
    # related_wrapper_excludes = ['foreign']
    # manual_ordering_field = 'position'
    autocomplete_fields = (
        'foreign',
        'many',
    )
    raw_id_fields = ('one',)
    resource_class = AllFieldResource
    show_changelist_content_language_toggle = True

    list_aggregates = {
        'decimal__sum': Sum('decimal'),
        'decimal__avg': Avg('decimal'),
        'decimal__min': Min('decimal'),
        'decimal__max': Max('decimal'),
        'decimal__stddev': StdDev('decimal'),
    }

    action_helpers = (
        ('action_test', 'Action description'),
        ('action_test_form_action', 'Action test form'),
        ('action_test_update_field_action', 'Action test update field'),
        ('action_test_update_fields_action', 'Action test update fields'),
        ('action_test_update_fieldsets_action', 'Action test update fieldsets'),
        ('action_add_many', 'Ajouter des many'),
        ('action_remove_many', 'Enlever des many'),
    )

    related_admins = {
        'products': (SimpleRelated, SimpleRelatedAdmin),
    }

    filter_presets = (
        (
            'default',
            'Tous',
            {},
        ),
        (
            'brand',
            'Marques',
            {
                'char': 'kjhkj',
                'foreign__id__exact': '158',
            },
        ),
    )

    search_typeahead = {
        'datasets': [
            {
                'prefetch': reverse_lazy('admin:tests_allfield_typeahead_prefetch'),
            },
        ]
    }

    custom_views = ('typeahead_prefetch',)

    formfield_options = {
        'char': {
            'help_text': 'help !',
            'placeholder': 'Enter text',
            'label': 'admin only label',
            'bootstrap3': {
                #'size': 'lg',
                #'show_help': False,
                #'show_label': False,
                #'set_required': False,
                #'addon_after': '<i class="fa fa-envelope"></i>',
                #'addon_before': '<i class="fa fa-envelope"></i>',
                'form_group_class': 'form-group test-form-group-class',
                'field_class': '',
                'label_class': '',
                'error_css_class': '',
                'required_css_class': '',
            },
        }
    }

    def changeform_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = {
            'data_store': {
                'test1': True,
                'test2': 'test2_value',
            }
        }
        return super().changeform_view(request, object_id, form_url, extra_context)

    def typeahead_prefetch_view(self, request, object_id=None):
        datums = []
        datum = {
            'name': 'Test 1',
            'extra': 'Simple',
            'type': 'Simple',
            'weight': 10,
            'inputName': 'foreign__id__exact',
            'itemId': 285,
        }
        datums.append(datum)
        for i, _datum in enumerate(datums):
            datums[i]['id'] = str(i)  # do not use an integer here, bugs with typeahead.js
        return datums

    def get_decimal_column_info(self, request):
        aggregates = self.get_list_aggregates(request)
        str = 'Sum: {}<br>Avg: {}<br>StdDev: {}<br>Min: {}<br>Max: {}'.format(
            aggregates['decimal__sum'],
            aggregates['decimal__avg'],
            aggregates['decimal__stddev'],
            aggregates['decimal__min'],
            aggregates['decimal__max'],
        )
        return str

    def action_test(self, request, queryset):
        queryset.update(date='abc')

    def action_add_many(self, request, queryset):
        return self.add_manytomany_action(request, queryset, 'many')

    def action_remove_many(self, request, queryset):
        return self.remove_manytomany_action(request, queryset, 'many')

    def get_queryset(self, request):
        return super().get_queryset(request).annotate(manysum=Sum('many__allfields__decimal'))

    def action_test_form_action(self, request, queryset):
        class TestForm(forms.Form):
            date = forms.CharField(label='')

            def save(self, queryset):
                queryset.update(date=self.cleaned_data['date'])

        return self.form_action(
            request,
            queryset,
            title="titre d'action",
            form_class=TestForm,
        )

    def action_test_update_field_action(self, request, queryset):
        return self.update_field_action(
            request=request,
            queryset=queryset,
            field='email',
        )

    def action_test_update_fields_action(self, request, queryset):
        return self.update_fields_action(
            request=request,
            queryset=queryset,
            fields=['date', 'foreign'],
        )

    def action_test_update_fieldsets_action(self, request, queryset):
        return self.update_fieldsets_action(
            request=request,
            queryset=queryset,
            fieldsets=[(None, {'fields': (['date', 'foreign'],)})],
        )

    # http://django-suit.readthedocs.org/en/develop/list_attributes.html
    def cell_attributes(self, obj, column):
        if column == 'bool':
            return {'class': 'text-muted'}

    def get_column_align(self, column):
        if column in ('char', 'email'):
            return 'right'

    def get_column_hidden(self, column):
        if column in ('email',):
            return True

    # http://django-suit.readthedocs.org/en/develop/list_attributes.html
    def row_attributes(self, obj):
        if obj.email:
            return {'class': 'info', 'data': obj.char}

    @admin.display(description='Simple')
    def related_link(self, obj):
        if not obj.pk:
            return 'Vous pourrez editer les simples une fois le AllField enregistré.'
        try:
            url = reverse('admin:tests_simplerelated_add', kwargs={'related_id': obj.pk})
        except NoReverseMatch:
            return

        return mark_safe(
            '<a data-dialog-onclose="refresh" data-dialog-size="small" data-dialog="changelist" data-dialog-title="Test!" class="btn btn-secondary" href="{url}">{text}</a>'.format(
                url=url, text='Editer les simples'
            )
        )


class UploadAdmin(admin.ModelAdmin):
    model = models.Upload


class TreeAdmin(TreeModelAdmin):
    list_display = ('name', 'parent')


admin.site.register(models.AllField, AllFieldAdmin)
admin.site.register(models.Simple, SimpleAdmin)
admin.site.register(models.Upload, UploadAdmin)
# admin.site.register(models.WithImage)
# admin.site.register(Image)
admin.site.register(models.Tree, TreeAdmin)
