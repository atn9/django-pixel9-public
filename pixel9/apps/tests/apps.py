import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)


class TestsConfig(AppConfig):
    name = 'pixel9.apps.tests'
