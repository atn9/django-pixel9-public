from django.forms.fields import FileField
from django.forms.widgets import FileInput


class PdfField(FileField):
    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        if isinstance(widget, FileInput) and 'accept' not in widget.attrs:
            attrs.setdefault('accept', 'application/pdf')
        return attrs
