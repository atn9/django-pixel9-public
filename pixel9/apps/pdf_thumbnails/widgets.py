from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe
from pdf2image.exceptions import PDFPageCountError

from .utils import get_pdf_thumbnails


class AdminPdfWidget(AdminFileWidget):
    def render(self, name, value, attrs=None, renderer=None):
        output = super().render(name, value, attrs, renderer)
        if value and hasattr(value, 'url'):
            try:
                thumbs = get_pdf_thumbnails(value, size=(1200, None))
                thumb = thumbs[0]
            except PDFPageCountError:
                pass
            else:
                output = (
                    '<div class="image-field"">'
                    '<a class="input-image-link"target="_blank" href="{}">'
                    '<img class="img-responsive" src="{}"></a>{}</div>'
                ).format(value.url, thumb.url, output)
                for thumb in thumbs:
                    thumb.close()
        return mark_safe(output)
