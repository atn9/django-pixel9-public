import hashlib
import io
import json
from pathlib import Path

import pdf2image
from django.conf import settings
from django.core.cache import cache
from django.core.files.images import ImageFile
from django.core.files.storage import get_storage_class

FILE_STORAGE = getattr(settings, 'PDF_THUMBNAILS_FILE_STORAGE', settings.DEFAULT_FILE_STORAGE)
storage = get_storage_class(FILE_STORAGE)()


class PdfThumbnailFile(ImageFile):
    def __init__(self, file, storage):
        self.file = file
        self.storage = storage
        self.name = file.name.replace(f'{self.storage.base_location}/', '')

    @property
    def url(self):
        return self.storage.url(self.name)

    def exists(self):
        return self.storage.exists(self.name)

    def path(self):
        return self.storage.path(self.name)

    def read(self, size=None):
        return self.storage.open(self.name).read()

    def write(self, content):
        return self.storage.save(self.name, content)

    def delete(self):
        return self.storage.delete(self.name)


def get_hash(bytes, **options):
    hash_data = bytes
    hash_data += json.dumps(dict(sorted(options.items()))).encode('UTF-8')
    return hashlib.sha1(hash_data).hexdigest()[:10]  # noqa S324


def get_pdf_thumbnails(file, **options):
    options.setdefault('fmt', 'jpeg')

    bytes = file.read()
    hash = get_hash(bytes, **options)
    cache_key = f'pixel9:pdf_thumbnails:paths:{hash}'
    paths = []

    cached_paths = cache.get(cache_key)
    if cached_paths:
        paths = cached_paths.split('|')
    else:
        paths = []
    if not cached_paths or not all(storage.exists(path) for path in paths):
        images = pdf2image.convert_from_bytes(bytes, **options)
        for i, image in enumerate(images):
            name = Path(file.name).stem
            extension = options['fmt'].lower()
            with io.BytesIO() as temp:
                image.save(temp, format=extension.upper())
                paths.append(storage.save(f'pdf_thumbnails/{name}-{i}.{extension}', temp))

        cache.set(cache_key, '|'.join(paths), timeout=None)

    thumbs = []
    for path in paths:
        thumbs.append(PdfThumbnailFile(storage.open(path), storage=storage))
    return thumbs


def test():
    from pixel9.apps.tests.models import AllField

    a = AllField.objects.get(pk=63)
    thumbs = get_pdf_thumbnails(a.file, size=(650, None))
    return thumbs
