from django.conf import settings


class Settings:
    ADMIN_SITE_CLASS = 'pixel9.apps.admin.sites.AdminSite'

    def __getattribute__(self, item):
        if hasattr(settings, item):
            return getattr(settings, item)
        return super().__getattribute__(item)

    def __setattr__(self, key, value):
        raise AttributeError(
            "Don't modify `admin_settings`, use "
            '`django.test.utils.override_settings` or '
            '`django.conf.settings` instead.'
        )


admin_settings = Settings()
