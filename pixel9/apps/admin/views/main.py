from django.contrib.admin.utils import quote
from django.contrib.admin.views.main import ChangeList as DjangoChangeList
from django.urls import reverse


class ChangeList(DjangoChangeList):
    def url_for_result(self, result):
        pk = getattr(result, self.pk_attname)
        if getattr(self.model_admin, 'is_related_model_admin', False):
            return reverse(
                f'admin:{self.opts.app_label}_{self.opts.model_name}_related_change',
                kwargs={
                    'object_id': quote(pk),
                    'related_id': quote(getattr(result, f'{self.model_admin.rel}_id')),
                },
                current_app=self.model_admin.admin_site.name,
            )
        else:
            return reverse(
                f'admin:{self.opts.app_label}_{self.opts.model_name}_change',
                kwargs={'object_id': quote(pk)},
                current_app=self.model_admin.admin_site.name,
            )
