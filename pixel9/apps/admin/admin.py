from django.contrib import admin
from django.contrib.admin.models import LogEntry
from django.template.loader import render_to_string


@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    search_fields = ('object_repr', 'change_message', 'user__username', 'user__email')
    list_display = (
        'object_repr',
        'content_type',
        'user',
        'change_message',
        'action_time',
    )
    list_filter = (
        'content_type',
        'user',
        'action_flag',
    )
    actions = ['delete_selected']
    load_more_paginate_by = 15

    custom_views = (
        {
            'name': 'load_more',
            'url': r'more/(?P<page>\d+)$',
            'perms': ('admin.logentry_change',),
        },
    )

    def load_more_view(self, request, page):
        min_index = (int(page) * self.load_more_paginate_by) - self.load_more_paginate_by
        max_index = (int(page) * self.load_more_paginate_by) - 1

        admin_log = LogEntry.objects.all().select_related('content_type', 'user')[min_index:max_index]

        return {
            'html': render_to_string(
                'admin/admin/logentry/includes/timeline.html',
                {
                    'admin_log': admin_log,
                    'admin_log_page': page,
                },
            )
        }


admin.site.disable_action('delete_selected')
