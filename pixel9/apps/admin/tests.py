import datetime

from django import forms
from django.test import TestCase

from pixel9.db import models


class RainbowModel(models.Model):
    'A model with all field type'

    auto = models.AutoField(primary_key=True)
    bool = models.BooleanField()
    char = models.CharField(max_length=100)
    comma = models.CommaSeparatedIntegerField(max_length=100)
    date = models.DateField()
    datetime = models.DateTimeField()
    decimal = models.DecimalField(max_digits=10, decimal_places=2)
    email = models.EmailField()
    file = models.FileField(upload_to='.')
    filepath = models.FilePathField(path='/tmp/django_tests/')  # noqa: S108
    float = models.FloatField()
    foreign = models.ForeignKey('SimpleModel', related_name='foreign')
    image = models.ImageField(upload_to='.')
    integer = models.IntegerField()
    ip = models.IPAddressField()
    many = models.ManyToManyField('SimpleModel', related_name='many')
    nullbool = models.NullBooleanField()
    one = models.OneToOneField('SimpleModel', related_name='one')
    positive = models.PositiveIntegerField()
    possmall = models.PositiveSmallIntegerField()
    slug = models.SlugField()
    small = models.SmallIntegerField()
    text = models.TextField()
    time = models.TimeField()
    url = models.URLField()
    # xml     = models.XMLField('')


class SimpleModel(models.Model):
    name = models.CharField(max_length=100)


class RainbowForm(forms.ModelForm):
    class Meta:
        model = RainbowModel


class AdminTest(TestCase):
    def setUp(self):
        s1 = SimpleModel.objects.create(name='simple1')
        s2 = SimpleModel.objects.create(name='simple2')
        self.rainbow = RainbowModel(
            bool=True,
            char='aaa',
            comma='a,b,c',
            date=datetime.date.today(),
            datetime=datetime.datetime.now(),
            decimal='1.10',
            email='test@test.com',
            file='test.html',
            filepath='test/test',
            float=float(500),
            foreign=s1,
            image='test/test.jpg',
            integer=1,
            ip='127.0.0.1',
            nullbool=None,
            one=s1,
            positive=1,
            possmall=1,
            slug='test-test',
            small=1,
            text='test\n\ntest\n\ntest\ttest',
            time=datetime.time(),
            url='http://example.com/test',
        )
        self.rainbow.save()
        self.rainbow.many.add(s1)
        self.rainbow.many.add(s2)
        self.rainbow_form = RainbowForm(instance=self.rainbow)
