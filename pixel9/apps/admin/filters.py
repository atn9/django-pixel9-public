from datetime import datetime, timedelta

from admin_auto_filters.filters import AutocompleteFilter
from django.contrib import admin
from django.contrib.admin.options import IncorrectLookupParameters
from django.contrib.admin.utils import prepare_lookup_value
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import models
from django.db.models.fields import AutoField, DecimalField, FloatField, IntegerField
from django.utils import timezone

from pixel9 import forms


# http://stackoverflow.com/questions/17392087/how-to-modify-django-admin-filters-title
def custom_title_filter(title):
    class Wrapper(admin.FieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.FieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance

    return Wrapper


# https://github.com/thomst/django-more-admin-filters/blob/master/more_admin_filters/filters.py
# Filter for annotated attributes.
# NOTE: The code is more or less the same than admin.FieldListFilter but
# we must not subclass it. Otherwise django's filter setup routine wants a real
# model field.
class BaseAnnotationFilter(admin.ListFilter):
    """
    Baseclass for annotation-list-filters.
    """

    attribute_name = None
    nullable_attribute = None
    title = None

    @classmethod
    def init(cls, attribute_name, nullable=True):
        """
        Since filters are listed as classes in ModelAdmin.list_filter we are
        not able to initialize the filter within the ModelAdmin.
        We use this classmethod to setup a filter-class for a specific annotated
        attribute::
            MyModelAdmin(admin.ModelAdmin):
                list_filter = [
                    MyAnnotationListFilter.init('my_annotated_attribute'),
                ]
        """
        attrs = dict(attribute_name=attribute_name, nullable=nullable)
        cls = type('cls.__name__' + attribute_name, (cls,), attrs)
        return cls

    def __init__(self, request, params, model, model_admin):
        if self.title is None:
            self.title = self.attribute_name
        super().__init__(request, params, model, model_admin)
        for p in self.expected_parameters():
            if p in params:
                value = params.pop(p)
                self.used_parameters[p] = prepare_lookup_value(p, value)

    def has_output(self):
        return True

    def queryset(self, request, queryset):
        try:
            return queryset.filter(**self.used_parameters)
        except (ValueError, ValidationError) as e:
            # Fields may raise a ValueError or ValidationError when converting
            # the parameters to the correct type.
            raise IncorrectLookupParameters() from e


# https://medium.com/@hakibenita/how-to-add-a-text-filter-to-django-admin-5d1db93772d8
class InputFilter(admin.SimpleListFilter):
    template = 'admin/includes/filters/input_filter.html'

    def lookups(self, request, model_admin):
        if self.parameter_name in request.GET:
            value = request.GET.get(self.parameter_name)
            return [[value, value]]
        else:
            return [['', '']]


class NumericFilterForm(forms.Form):
    LOOKUP_CHOICES = (
        ('', 'Sélectionner'),
        ('exact', '='),
        ('gt', '>'),
        ('lt', '<'),
        ('gte', '>='),
        ('lte', '<='),
    )

    def __init__(self, *args, **kwargs):
        self.name = kwargs.pop('name')
        super().__init__(*args, **kwargs)
        self.fields[self.name + '__lookup1'] = forms.ChoiceField(
            label='',
            required=False,
            choices=self.LOOKUP_CHOICES,
            widget=forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width:auto; border-right:none;',
                }
            ),
        )
        self.fields[self.name + '__value1'] = forms.FloatField(
            label='',
            required=False,
            widget=forms.NumberInput(attrs={'class': 'form-control'}),
        )
        self.fields[self.name + '__lookup2'] = forms.ChoiceField(
            label='',
            required=False,
            choices=self.LOOKUP_CHOICES,
            widget=forms.Select(
                attrs={
                    'class': 'form-control',
                    'style': 'width:auto; border-right:none;',
                }
            ),
        )
        self.fields[self.name + '__value2'] = forms.FloatField(
            label='',
            required=False,
            widget=forms.NumberInput(attrs={'class': 'form-control'}),
        )

    def __getitem__(self, name):
        # Use form.lookup1 insted of form.[name]__lookup1 in templates
        return super().__getitem__(f'{self.name}__{name}')


class NumericFilterMixin:
    request = None
    parameter_name = None
    template = 'admin/includes/filters/numeric_filter.html'
    form_class = NumericFilterForm

    def __init__(self, *args):
        if len(args) == 6:
            # FieldListFilter
            field, request, params, model, model_admin, field_path = args
        else:
            # BaseAnnotationFilter
            request, params, model, model_admin = args
            field = None
            field_path = self.field_path = self.attribute_name
        super().__init__(*args)

        if field:
            if not isinstance(field, (DecimalField, IntegerField, FloatField, AutoField)):
                raise TypeError(f'Class {type(field)} is not supported for {self.__class__.__name__}.')

        self.request = request

        if self.parameter_name is None:
            self.parameter_name = field_path

        if self.parameter_name + '__lookup1' in params:
            self.used_parameters[self.parameter_name + '__lookup1'] = params.pop(self.parameter_name + '__lookup1')
        if self.parameter_name + '__value1' in params:
            self.used_parameters[self.parameter_name + '__value1'] = params.pop(self.parameter_name + '__value1')
        if self.parameter_name + '__lookup2' in params:
            self.used_parameters[self.parameter_name + '__lookup2'] = params.pop(self.parameter_name + '__lookup2')
        if self.parameter_name + '__value2' in params:
            self.used_parameters[self.parameter_name + '__value2'] = params.pop(self.parameter_name + '__value2')

    def queryset(self, request, queryset):
        filters = {}
        lookup1 = self.used_parameters.get(self.parameter_name + '__lookup1', None)
        value1 = self.used_parameters.get(self.parameter_name + '__value1', None)
        lookup2 = self.used_parameters.get(self.parameter_name + '__lookup2', None)
        value2 = self.used_parameters.get(self.parameter_name + '__value2', None)

        field = getattr(self, 'field', None)

        if value1 is not None:
            if field and isinstance(field, IntegerField):
                value1 = self.used_parameters[self.parameter_name + '__value1'] = int(float(value1))
        if value2 is not None:
            if field and isinstance(field, IntegerField):
                value1 = self.used_parameters[self.parameter_name + '__value2'] = int(float(value2))

        if lookup1 is not None:
            if lookup1 == 'exact' and value1 is None:
                filters.update({f'{self.field_path}__isnull': True})
            elif value1 is not None:
                filters.update({f'{self.field_path}__{lookup1}': value1})
        if lookup2 is not None:
            if lookup2 == 'exact' and value2 is None:
                filters.update({f'{self.field_path}__isnull': True})
            elif value2 is not None:
                filters.update({f'{self.field_path}__{lookup2}': value2})
        return queryset.filter(**filters)

    def expected_parameters(self):
        return [
            f'{self.parameter_name}__lookup1',
            f'{self.parameter_name}__value1',
            f'{self.parameter_name}__lookup2',
            f'{self.parameter_name}__value2',
        ]

    def choices(self, changelist):
        all_choices = {
            'selected': self.value() is None,
            'query_string': changelist.get_query_string({}, self.expected_parameters()),
            'display': 'All',
        }

        lookup1 = self.used_parameters.get(self.parameter_name + '__lookup1')
        lookup2 = self.used_parameters.get(self.parameter_name + '__lookup2')
        value1 = self.used_parameters.get(self.parameter_name + '__value1')
        value2 = self.used_parameters.get(self.parameter_name + '__value2')
        lookup1_selected = lookup1 == 'exact' or (lookup1 and value1 is not None)
        lookup2_selected = lookup2 == 'exact' or (lookup2 and value2 is not None)

        return (
            all_choices,
            {
                'request': self.request,
                'parameter_name': self.parameter_name,
                'display': self.value(),
                'selected': lookup1_selected or lookup2_selected,
                'lookup1_selected': lookup1_selected,
                'lookup2_selected': lookup2_selected,
                'form': self.form_class(
                    name=self.parameter_name,
                    data={
                        self.parameter_name + '__lookup1': lookup1,
                        self.parameter_name + '__value1': value1,
                        self.parameter_name + '__lookup2': lookup2,
                        self.parameter_name + '__value2': value2,
                    },
                ),
            },
        )

    def value(self):
        str = ''
        lookup1 = self.used_parameters.get(self.parameter_name + '__lookup1')
        lookup2 = self.used_parameters.get(self.parameter_name + '__lookup2')
        value1 = self.used_parameters.get(self.parameter_name + '__value1')
        value2 = self.used_parameters.get(self.parameter_name + '__value2')
        lookup1_display = ''
        lookup2_display = ''

        if lookup1:
            for k, v in self.form_class.LOOKUP_CHOICES:
                if lookup1 == k:
                    lookup1_display = v
            if lookup1 == 'exact' and value1 is None:
                str += 'est vide'
            elif value1 is not None:
                str += f'{lookup1_display} {value1}'

        if lookup2:
            for k, v in self.form_class.LOOKUP_CHOICES:
                if lookup2 == k:
                    lookup2_display = v
            if str:
                str += ' et '

            if lookup2 == 'exact' and value2 is None:
                str += 'est vide'
            elif value2 is not None:
                str += f'{lookup2_display} {value2}'
        return str


class NumericFilter(NumericFilterMixin, admin.FieldListFilter):
    def __init__(self, field, request, params, model, model_admin, field_path):
        super().__init__(field, request, params, model, model_admin, field_path)


class AnnotationNumericFilter(NumericFilterMixin, BaseAnnotationFilter):
    def __init__(self, request, params, model, model_admin):
        super().__init__(request, params, model, model_admin)


class AutocompleteFilter(AutocompleteFilter):
    template = 'admin/includes/filters/autocomplete_filter.html'

    def __init__(self, request, params, model, model_admin):
        self.model = model
        super().__init__(request, params, model, model_admin)

    def lookups(self, request, model_admin):
        if self.parameter_name in request.GET:
            value = request.GET.get(self.parameter_name)
            repr = value
            model = self.rel_model or self.model

            try:
                obj = self.get_queryset_for_field(model, self.field_name).get(pk=value)
            except ObjectDoesNotExist:
                pass
            else:
                repr = str(obj)
            return [[value, repr]]
        else:
            return [['', '']]


class DateFieldFilter(admin.FieldListFilter):
    parameter_name = None
    template = 'admin/includes/filters/date_filter.html'

    def __init__(self, field, request, params, model, model_admin, field_path):
        super().__init__(field, request, params, model, model_admin, field_path)
        if self.parameter_name is None:
            self.parameter_name = field_path

        if self.parameter_name in params:
            value = params.pop(self.parameter_name)
            self.used_parameters[self.parameter_name] = value

    def value(self):
        return self.used_parameters.get(self.parameter_name, None)

    def expected_parameters(self):
        return [self.parameter_name]

    def queryset(self, request, queryset):
        try:
            if self.value():
                date = self.value()
                params = {
                    f'{self.parameter_name}__gte': timezone.make_aware(datetime.strptime(date, '%d/%m/%Y')),
                    f'{self.parameter_name}__lt': timezone.make_aware(datetime.strptime(date, '%d/%m/%Y'))
                    + timedelta(days=1),
                }
                return queryset.filter(**params)
            else:
                return queryset
        except ValidationError as e:
            raise IncorrectLookupParameters() from e

    def choices(self, cl):
        all_choice = {
            'selected': self.value() is None,
            'query_string': cl.get_query_string({}, [self.parameter_name]),
            'display': 'All',
        }
        return (
            all_choice,
            {
                'selected': bool(self.value()),
                'current_value': self.value(),
                'parameter_name': self.parameter_name,
                'query_string': cl.get_query_string({}),
                'display': self.value(),
            },
        )


class DateRangeFieldFilter(admin.FieldListFilter):
    parameter_name = None
    template = 'admin/includes/filters/daterange_filter.html'

    def __init__(self, field, request, params, model, model_admin, field_path):
        super().__init__(field, request, params, model, model_admin, field_path)
        if self.parameter_name is None:
            self.parameter_name = field_path

        if self.parameter_name in params:
            value = params.pop(self.parameter_name)
            self.used_parameters[self.parameter_name] = value

    def value(self):
        """
        Returns the value (in string format) provided in the request's
        query string for this filter, if any. If the value wasn't provided then
        returns None.
        """
        return self.used_parameters.get(self.parameter_name, None)

    def expected_parameters(self):
        """
        Returns the list of parameter names that are expected from the
        request's query string and that will be used by this filter.
        """
        return [self.parameter_name]

    def queryset(self, request, queryset):
        try:
            if self.value():
                date_from, date_to = self.value().split(' - ')
                params = {
                    f'{self.parameter_name}__gte': timezone.make_aware(datetime.strptime(date_from, '%d/%m/%Y')),
                    f'{self.parameter_name}__lt': timezone.make_aware(datetime.strptime(date_to, '%d/%m/%Y'))
                    + timedelta(days=1),
                }
                return queryset.filter(**params)
            else:
                return queryset
        except (ValidationError, ValueError) as e:
            raise IncorrectLookupParameters() from e

    def choices(self, cl):
        all_choice = {
            'selected': self.value() is None,
            'query_string': cl.get_query_string({}, [self.parameter_name]),
            'display': 'All',
        }
        return (
            all_choice,
            {
                'selected': bool(self.value()),
                'current_value': self.value(),
                'parameter_name': self.parameter_name,
                'query_string': cl.get_query_string({}),
                'display': self.value(),
            },
        )


admin.FieldListFilter.register(
    lambda f: isinstance(f, models.DateField),
    DateRangeFieldFilter,
    take_priority=True,
)

# do not register for IntergerField because the field might be choice field
admin.FieldListFilter.register(
    lambda f: isinstance(
        f,
        (
            models.DecimalField,
            models.FloatField,
        ),
    ),
    NumericFilter,
    take_priority=True,
)
