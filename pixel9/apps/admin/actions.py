import inspect
from collections import OrderedDict

from django.contrib import messages
from django.contrib.admin import helpers, utils
from django.contrib.admin.options import IS_POPUP_VAR
from django.core.exceptions import ValidationError
from django.shortcuts import render


class ActionHelpersMixin:
    """
    action_helpers = (
        ('method', _(u'Description'),
    )
    """

    action_helpers = []

    def get_actions(self, request):
        if self.actions is None or IS_POPUP_VAR in request.GET:
            return OrderedDict()

        actions = super().get_actions(request)

        action_helpers = self.get_action_helpers(request)
        if action_helpers:
            for name, description in action_helpers:
                actions[name] = self.get_action_helper(request, name, description)
        return actions

    def get_action_helpers(self, request):
        return getattr(self, 'action_helpers', [])

    def get_action_helper(self, request, name, description):
        func, action, _description = self.get_action(name)

        def wrapped_action(self, request, queryset):
            try:
                response = func(self, request, queryset)
            except ValidationError as e:
                self.message_user(request, '<br>'.join(e), level=messages.ERROR)
            else:
                if response is None:
                    self.success_action_msg(request, queryset, action)
                else:
                    return response

        wrapped_action.short_description = description

        return wrapped_action, action, description

    def get_current_action(self, request):
        actions = self.get_actions(request)
        for record in inspect.stack():
            if record[3] in actions:
                return record[3]

    def form_action(
        self,
        request,
        queryset,
        form_class,
        title,
        action=None,
        template_name=None,
        form_kwargs=None,
        fieldsets=None,
    ):
        form_kwargs = form_kwargs or {}
        action = action or self.get_current_action(request)

        if template_name is None:
            if fieldsets:
                template_name = 'admin/action_fieldsets.html'
            else:
                template_name = 'admin/action_form.html'

        if request.POST.get('post'):
            form = form_class(request.POST, **form_kwargs)
            if form.is_valid():
                form.save(queryset)
                return None
        else:
            form = form_class(**form_kwargs)

        context = {
            'form': form,
            'request': request,
            'queryset': queryset,
            'title': title,
            'action': action,
        }

        if fieldsets:
            context['adminform'] = helpers.AdminForm(
                form,
                fieldsets,
                self.get_prepopulated_fields(request),
                self.get_readonly_fields(request),
                model_admin=self,
            )

        context.update(self.admin_site.each_context(request))

        return render(request, template_name, context)

    def update_field_action(
        self,
        request,
        queryset,
        field,
        title=None,
        call_save=False,
        template_name=None,
        form_kwargs=None,
    ):
        form_kwargs = form_kwargs or {}
        if title is None:
            title = 'Modifier le champ {} de {} {}'.format(
                utils.label_for_field(field, self.model, self),
                queryset.count(),
                utils.model_ngettext(self.opts, queryset.count()).lower(),
            )
        return self.update_fields_action(
            request,
            queryset,
            [field],
            title=title,
            call_save=call_save,
            template_name=template_name,
            form_kwargs=form_kwargs,
        )

    def update_fields_action(
        self,
        request,
        queryset,
        fields,
        title=None,
        call_save=False,
        template_name=None,
        form_kwargs=None,
        fieldsets=None,
    ):
        form_kwargs = form_kwargs or {}
        if title is None:
            title = 'Modifier les champs {} de {} {}'.format(
                ', '.join(utils.label_for_field(field, self.model, self) for field in fields),
                queryset.count(),
                utils.model_ngettext(self.opts, queryset.count()).lower(),
            )

        def form_save_method(self, queryset):
            if call_save:
                for obj in queryset:
                    for field in fields:
                        setattr(obj, field, self.cleaned_data[field])
                    obj.save(update_fields=fields)
            else:
                data = {}
                for field in fields:
                    data[field] = self.cleaned_data[field]
                return queryset.update(**data)

        form_class = self.get_form(request, None, fields=fields)
        form_class.save = form_save_method
        return self.form_action(
            request,
            queryset,
            form_class=form_class,
            title=title,
            template_name=template_name,
            form_kwargs=form_kwargs,
            fieldsets=fieldsets,
        )

    def update_fieldset_action(
        self,
        request,
        queryset,
        fieldset,
        title=None,
        call_save=False,
        template_name=None,
        form_kwargs=None,
    ):
        form_kwargs = form_kwargs or {}
        return self.update_fieldsets_action(
            request,
            queryset,
            [fieldset],
            title=title,
            call_save=call_save,
            template_name=template_name,
            form_kwargs=form_kwargs,
        )

    def update_fieldsets_action(
        self,
        request,
        queryset,
        fieldsets,
        title=None,
        call_save=False,
        template_name=None,
        form_kwargs=None,
    ):
        form_kwargs = form_kwargs or {}
        fields = utils.flatten_fieldsets(fieldsets)
        return self.update_fields_action(
            request,
            queryset,
            fields,
            title=title,
            call_save=call_save,
            template_name=template_name,
            form_kwargs=form_kwargs,
            fieldsets=fieldsets,
        )

    def _manytomany_action(self, request, queryset, field, type_, template_name=None):
        field_label = utils.label_for_field(field, self.model, self)
        if type_ == 'add':
            title = f'Ajouter des "{field_label}"'
        elif type_ == 'remove':
            title = f'Enlever des "{field_label}"'
        elif type_ == 'set':
            title = f'Définir les "{field_label}"'

        def form_save_method(self, queryset):
            for obj in queryset:
                if type_ == 'remove':
                    currents = getattr(obj, field).all()
                    to_remove = []
                    for current in currents:
                        if current not in self.cleaned_data[field]:
                            to_remove.append(current)
                    getattr(obj, field).remove(*to_remove)
                elif type_ == 'add':
                    getattr(obj, field).add(*list(self.cleaned_data[field]))
                elif type_ == 'set':
                    getattr(obj, field).set(self.cleaned_data[field])

        form_kwargs = {}
        if type_ == 'remove':
            ids = []
            for obj in queryset:
                ids.extend(getattr(obj, field).values_list('id', flat=True))

            form_kwargs['initial'] = {
                field: ids,
            }

        form_class = self.get_form(request, None, fields=[field])
        form_class.save = form_save_method
        return self.form_action(
            request,
            queryset,
            form_class=form_class,
            title=title,
            template_name=template_name,
            form_kwargs=form_kwargs,
        )

    def add_manytomany_action(self, request, queryset, field):
        return self._manytomany_action(request, queryset, field, type_='add')

    def remove_manytomany_action(self, request, queryset, field):
        return self._manytomany_action(request, queryset, field, type_='remove')

    def set_manytomany_action(self, request, queryset, field):
        return self._manytomany_action(request, queryset, field, type_='set')

    def success_action_msg(self, request, queryset, action, prefix=''):
        if queryset.count() > 1:
            msg = '{0}{1} objets {2} ont été modifiés avec succès.'
        else:
            msg = '{0}{1} objet {2} a été modifié avec succès.'

        self.message_user(
            request,
            msg.format(
                prefix,
                queryset.count(),
                utils.model_ngettext(self.opts, queryset.count()),
            ),
            messages.SUCCESS,
        )

    def error_action_msg(self, request, queryset, prefix=''):
        msg = "{0} Aucun élément n'à été modifié."

        self.message_user(
            request,
            msg.format(
                prefix,
            ),
            messages.ERROR,
        )
