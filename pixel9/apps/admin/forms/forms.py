import json

from django import forms
from django.forms.models import BaseModelForm, ErrorList, model_to_dict
from django.utils.html import conditional_escape
from django.utils.translation import gettext_lazy as _

from pixel9.apps.auth.forms import EmailAuthenticationForm


class AllValidChoiceField(forms.TypedChoiceField):
    def valid_value(self, value):
        return True


class AdminAuthenticationForm(EmailAuthenticationForm):
    def clean(self):
        super().clean()
        if self.user and not self.user.is_staff:
            raise forms.ValidationError(_('Accès refusé'))
        return self.cleaned_data

    def get_user(self):
        return self.user


class TreeFormAdmin(forms.ModelForm):
    """
    require javascript to work properly
    only work without ``node_order_by``
    """

    _parent = AllValidChoiceField(label=_('Descendant de'), coerce=int)
    _position = AllValidChoiceField(label=_('Position'), coerce=int)

    class Meta:
        exclude = ('path', 'depth', 'numchild', 'lft', 'rgt', 'tree_id', 'parent', 'sib_order')  # noqa DJ006

    def __init__(
        self,
        data=None,
        files=None,
        auto_id='id_%s',
        prefix=None,
        initial=None,
        error_class=ErrorList,
        label_suffix=':',
        empty_permitted=False,
        instance=None,
    ):
        opts = self._meta
        self.is_sorted = hasattr(opts.model, 'node_order_by') and opts.model.node_order_by
        if not self.is_sorted:
            self.declared_fields['_parent'].widget.attrs['class'] = 'tree-positioner-parent'
            self.declared_fields['_position'].widget.attrs['class'] = 'tree-positioner-position'

        self.root_node = opts.model.get_or_create_first_root_node()

        if instance is None:
            # if we didn't get an instance, instantiate a new one
            self.instance = opts.model()
            object_data = {}
            self.build_dropdown_tree(self.declared_fields['_parent'])
            object_data['_parent'] = self.root_node.pk
            if not self.is_sorted:
                self.declared_fields['_position'].choices = [
                    (k, k + 1) for k in range(self.root_node.get_children().count() + 1)
                ]
                object_data['_position'] = 0

        else:
            object_data = model_to_dict(instance, opts.fields, opts.exclude)
            self.instance = instance
            self.build_dropdown_tree(self.declared_fields['_parent'], self.instance)
            object_data['_parent'] = self.instance.get_parent().pk

            if not self.is_sorted:
                siblings = instance.get_siblings()
                self.declared_fields['_position'].choices = [(k, k + 1) for k in range(siblings.count())]
                for key, sib in enumerate(siblings):
                    if sib == self.instance:
                        object_data['_position'] = key
                        break

        # if initial was provided, it should override the values from instance
        if initial is not None:
            object_data.update(initial)

        # we don't call BaseModelForm.__init__()
        super(BaseModelForm, self).__init__(
            data, files, auto_id, prefix, object_data, error_class, label_suffix, empty_permitted
        )

    def build_dropdown_tree(self, field, for_node=None):
        """Creates a tree-like list of choices"""

        def indent(level):
            return '. . ' * (level - 2)

        choices = [(self.root_node.pk, _('-- Aucun --'))]
        children_sums = {}
        tree = self.root_node.get_descendants()

        def is_loop_safe(node):
            if for_node is None:
                return True
            if node == for_node or node.is_descendant_of(for_node):
                return False
            return True

        def get_parent_id(node):
            return getattr(node, 'parent_id', node.parent and node.parent.pk or None)

        for node in tree:
            if is_loop_safe(node):
                choices.append((node.pk, indent(node.get_depth()) + str(node)))
                if not self.is_sorted:
                    if node.pk not in children_sums:
                        children_sums[node.pk] = 1
                    if get_parent_id(node):
                        if get_parent_id(node) in children_sums:
                            children_sums[get_parent_id(node)] += 1
                        else:
                            children_sums[get_parent_id(node)] = 2

        # if not self.is_sorted:
        # if get_parent_id(self.instance) in children_sums:
        # children_sums[get_parent_id(self.instance)] -= 1

        field.choices = choices
        if not self.is_sorted:
            field.widget.attrs['rel'] = conditional_escape(json.dumps(children_sums))
            field.widget.attrs['class'] = 'tree-positioner-parent'

    def clean(self):
        opts = self._meta
        position = self.cleaned_data['_position']
        parent_id = self.cleaned_data['_parent']
        try:
            self.parent_node = opts.model.objects.get(pk=parent_id)
        except opts.model.DoesNotExist:
            self._errors['_parent'] = ErrorList([_('Parent invalide')])
        if self.parent_node.is_child_of(self.instance):
            self._errors['_parent'] = ErrorList(
                [_("Cette instance ne peut pas être une sous-instance d'un de ses descandants")]
            )
        if not self.is_sorted and position:
            siblings = self.parent_node.get_children()
            if self.instance.pk:
                siblings = siblings.exclude(pk=self.instance.pk)
            try:
                self.sibling_node = siblings[position - 1]
            except IndexError:
                self._errors['_position'] = ErrorList([_('Position invalide')])

        return super().clean()

    def save(self, commit=True):
        position = self.cleaned_data['_position']
        del self.cleaned_data['_position']
        del self.cleaned_data['_parent']

        if self.instance.pk is None:
            # remove many_to_many data
            data = {}
            for key, value in list(self.cleaned_data.items()):
                if key in self.instance.__dict__ or key + '_id' in self.instance.__dict__:
                    data[key] = value

        if not self.is_sorted:
            if position:
                if self.instance.pk is None:
                    self.instance = self.sibling_node.add_sibling('right', **data)
                else:
                    self.instance.move(self.sibling_node, 'right')
            else:
                if self.instance.pk is None:
                    self.instance = self.parent_node.add_child(**data)
                    self.instance.move(self.parent_node, 'first-child')
                else:
                    self.instance.move(self.parent_node, 'first-child')

        super().save(commit=commit)
        return self.instance
