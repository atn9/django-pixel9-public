from django.forms.fields import SplitDateTimeField

from .widgets import AdminSplitDateTimeWidget


class AdminSplitDateTimeField(SplitDateTimeField):
    widget = AdminSplitDateTimeWidget
