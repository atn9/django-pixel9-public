"""
Form Widget classes specific to the Django admin site.
"""

from django.contrib.admin.widgets import (
    AdminFileWidget as DjAdminFileWidget,
)
from django.contrib.admin.widgets import (
    AutocompleteMixin,
    ForeignKeyRawIdWidget,
)
from django.contrib.admin.widgets import (
    RelatedFieldWidgetWrapper as DjRelatedFieldWidgetWrapper,
)
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.urls.exceptions import NoReverseMatch
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.text import Truncator
from django.utils.translation import gettext_lazy as _
from sorl.thumbnail.shortcuts import get_thumbnail

from pixel9 import forms


class AdminFileWidget(DjAdminFileWidget):
    input_text = ''
    template_with_initial = '<div>%(initial)s %(clear_template)s%(input)s</div>'
    template_with_clear = '<div class="clearable-file-input">%(clear)s <label for="%(clear_checkbox_id)s">%(clear_checkbox_label)s</label></div>'

    def render(self, name, value, attrs=None, renderer=None):
        return mark_safe(f'<div class="file-field">{super().render(name, value, attrs)}</div>')


class AdminImageWidget(DjAdminFileWidget):
    input_text = ''
    thumb_size_str = '320x240'
    template_with_initial = '<div class="caption image-field-caption"> %(clear_template)s %(input)s</div>'

    def render(self, name, value, attrs=None, renderer=None):
        output = super().render(name, value, attrs, renderer)
        if value and hasattr(value, 'url'):
            try:
                thumb = get_thumbnail(value, self.thumb_size_str, upscale=False)
            except Exception:
                output = '<div class="image-field"></div>'
            else:
                output = (
                    '<div class="image-field"">'
                    '<a class="input-image-link" target="_blank" href="{}">'
                    '<img class="img-responsive" src="{}"></a>{}</div>'
                ).format(value.url, thumb.url, output)
        else:
            output = format_html('<div class="image-field">{}</div>', output)
        return mark_safe(output)


class AdminImageWidgetSmall(AdminImageWidget):
    thumb_size_str = '100x80'


class FileNameInput(forms.CharField):
    def render(self, name, value, attrs=None):
        super_output = super().render(name, value, attrs)
        return super_output


class AdminDateWidget(forms.DateInput):
    template_name = 'admin/widgets/date.html'


class AdminTimeWidget(forms.TimeInput):
    template_name = 'admin/widgets/time.html'


class AdminSplitDateTimeWidget(forms.SplitDateTimeWidget):
    """
    A SplitDateTime Widget that has some admin-specific styling.
    """

    template_name = 'admin/widgets/split_datetime.html'

    def __init__(self, attrs=None):
        widgets = [AdminDateWidget, AdminTimeWidget]
        # Note that we're calling MultiWidget, not SplitDateTimeWidget, because
        # we want to define widgets.
        forms.MultiWidget.__init__(self, widgets, attrs)


class RelatedFieldWidgetWrapper(DjRelatedFieldWidgetWrapper):
    pass


class AdminTextareaWidget(forms.Textarea):
    def __init__(self, attrs=None):
        self.bootstrap3 = {'form_group_class': 'form-group full-width'}

        final_attrs = {'class': 'vLargeTextField', 'rows': '2'}
        if attrs is not None:
            final_attrs.update(attrs)
        super().__init__(attrs=final_attrs)


class AdminRadioSelect(forms.RadioSelect):
    pass


class AdminSelectMultiple(forms.SelectMultiple):
    def __init__(self, attrs=None, choices=()):
        self.bootstrap3 = {'form_group_class': 'form-group full-width select-multiple'}
        return super().__init__(attrs, choices)


# Mapping of lower case language codes [returned by Django's get_language()]
# to language codes supported by select2.
# See django/contrib/admin/static/admin/js/vendor/select2/i18n/*
SELECT2_TRANSLATIONS = {
    x.lower(): x
    for x in [
        'ar',
        'az',
        'bg',
        'ca',
        'cs',
        'da',
        'de',
        'el',
        'en',
        'es',
        'et',
        'eu',
        'fa',
        'fi',
        'fr',
        'gl',
        'he',
        'hi',
        'hr',
        'hu',
        'id',
        'is',
        'it',
        'ja',
        'km',
        'ko',
        'lt',
        'lv',
        'mk',
        'ms',
        'nb',
        'nl',
        'pl',
        'pt-BR',
        'pt',
        'ro',
        'ru',
        'sk',
        'sr-Cyrl',
        'sr',
        'sv',
        'th',
        'tr',
        'uk',
        'vi',
    ]
}
SELECT2_TRANSLATIONS.update({'zh-hans': 'zh-CN', 'zh-hant': 'zh-TW'})


class CustomAutocompleteMixin(AutocompleteMixin):
    def build_attrs(self, base_attrs, extra_attrs=None):
        attrs = super().build_attrs(base_attrs, extra_attrs=extra_attrs)
        attrs.update(
            {
                'data-theme': 'bootstrap',
                'data-language': 'fr',
                'data-minimum-results-for-search': 15,
                # http://stackoverflow.com/questions/14313001/select2-not-calculating-resolved-width-correctly-if-select-is-hidden
                'style': 'width:100%',
            }
        )
        return attrs


class CustomAutocompleteSelect(CustomAutocompleteMixin, forms.Select):
    def __init__(self, rel, admin_site, attrs=None, choices=(), using=None, custom_url=None):
        # for admin_auto_filters
        self.custom_url = custom_url
        super().__init__(rel, admin_site, attrs, choices, using)


class CustomAutocompleteSelectMultiple(CustomAutocompleteMixin, forms.SelectMultiple):
    def build_attrs(self, base_attrs, extra_attrs=None):
        attrs = super().build_attrs(self.attrs, extra_attrs)
        # https://github.com/select2/select2/issues/3335
        attrs.update(
            {
                'data-allow-clear': 'false',
            }
        )
        return attrs


class CustomForeignKeyRawIdWidget(ForeignKeyRawIdWidget):
    def __init__(self, rel, admin_site, attrs=None, using=None):
        self.rel = rel
        self.admin_site = admin_site
        self.db = using
        forms.TextInput.__init__(self, attrs)

    def get_context(self, name, value, attrs):
        context = super(ForeignKeyRawIdWidget, self).get_context(name, value, attrs)
        rel_to = self.rel.model
        if rel_to in self.admin_site._registry:
            # The related object is registered with the same AdminSite
            related_url = reverse(
                'admin:{}_{}_changelist'.format(
                    rel_to._meta.app_label,
                    rel_to._meta.model_name,
                ),
                current_app=self.admin_site.name,
            )

            params = self.url_parameters()
            if params:
                related_url += '?' + '&amp;'.join(f'{k}={v}' for k, v in params.items())
            context['related_url'] = mark_safe(related_url)
            context['link_title'] = _('Lookup')
            # The JavaScript code looks for this class.
            context['widget']['attrs'].setdefault('class', 'vForeignKeyRawIdAdminField')
        if context['widget']['value']:
            context['link_label'], context['link_url'] = self.label_and_url_for_value(value)

        context['label_url'] = reverse(
            'admin:{}_{}_raw_id_label'.format(
                rel_to._meta.app_label,
                rel_to._meta.model_name,
            ),
            current_app=self.admin_site.name,
        )
        return context

    def label_and_url_for_value(self, value):
        key = self.rel.get_related_field().name
        try:
            obj = self.rel.model._default_manager.using(self.db).get(**{key: value})
        except (ValueError, self.rel.model.DoesNotExist, ValidationError):
            return '', ''

        try:
            url = reverse(
                '{}:{}_{}_change'.format(
                    self.admin_site.name,
                    obj._meta.app_label,
                    obj._meta.object_name.lower(),
                ),
                args=(obj.pk,),
            )
        except NoReverseMatch:
            url = ''  # Admin not registered for target model.

        if hasattr(obj, 'raw_id_field_html_repr'):
            label = obj.raw_id_field_html_repr()
        else:
            label = format_html('<span class="raw-id-label">{}</span>', Truncator(obj).words(14))

        return label, url
