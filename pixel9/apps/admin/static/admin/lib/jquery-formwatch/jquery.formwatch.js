/*
    jQuery FormWatch: easily monitor changes in form data.
    https://github.com/kylefox/jQuery-FormWatch
    Version 0.1
*/
(function($) {

  function eventArgs() {
    return $.map(arguments, function(arg) {
      return arg;
    });
  }

  var deepDiffMapper = function () {
    return {
      VALUE_CREATED: 'created',
      VALUE_UPDATED: 'updated',
      VALUE_DELETED: 'deleted',
      VALUE_UNCHANGED: 'unchanged',
      map: function(obj1, obj2) {
        if (this.isFunction(obj1) || this.isFunction(obj2)) {
          throw 'Invalid argument. Function given, object expected.';
        }
        if (this.isValue(obj1) || this.isValue(obj2)) {
          return {
            type: this.compareValues(obj1, obj2),
            data: obj1 === undefined ? obj2 : obj1
          };
        }

        var diff = {};
        for (var key in obj1) {
          if (this.isFunction(obj1[key])) {
            continue;
          }

          var value2 = undefined;
          if (obj2[key] !== undefined) {
            value2 = obj2[key];
          }

          diff[key] = this.map(obj1[key], value2);
        }
        for (var key in obj2) {
          if (this.isFunction(obj2[key]) || diff[key] !== undefined) {
            continue;
          }

          diff[key] = this.map(undefined, obj2[key]);
        }

        for (var key in diff) {
          if(diff[key].type === this.VALUE_UNCHANGED) {
            delete diff[key];
          }
        }
        return diff;

      },
      compareValues: function (value1, value2) {
        if (value1 === value2) {
          return this.VALUE_UNCHANGED;
        }
        if (this.isDate(value1) && this.isDate(value2) && value1.getTime() === value2.getTime()) {
          return this.VALUE_UNCHANGED;
        }
        if (value1 === undefined) {
          return this.VALUE_CREATED;
        }
        if (value2 === undefined) {
          return this.VALUE_DELETED;
        }
        return this.VALUE_UPDATED;
      },
      isFunction: function (x) {
        return Object.prototype.toString.call(x) === '[object Function]';
      },
      isArray: function (x) {
        return Object.prototype.toString.call(x) === '[object Array]';
      },
      isDate: function (x) {
        return Object.prototype.toString.call(x) === '[object Date]';
      },
      isObject: function (x) {
        return Object.prototype.toString.call(x) === '[object Object]';
      },
      isValue: function (x) {
        return !this.isObject(x) && !this.isArray(x);
      }
    }
  }();

  function debug(a, b) {
    console.log(deepDiffMapper.map(a, b));
  }

  function serialize($form, ignoreFields) {
    var data = $.deparam($form.serialize());

    $form.find('input[type="file"]').each(function(){
      var $input = $(this);
      data[$input.attr('name')] = $input.val();
    });

    if (ignoreFields) {
      $.each(ignoreFields, function(i, fieldName){
        delete data[fieldName];
      });
    }
    $form.find(':submit').each(function(){
      delete data[$(this).attr('name')]
    });
    return data;
  }

  function checkChange(event) {
    var $form = $(this),
        oldData = $form.data('oldData'),
        originalData = $form.data('originalData'),
        newData = serialize($form, event.data.ignoreFields),
        args = eventArgs(newData, oldData, originalData);


    if (! _.isEqual(oldData, newData)) {
      $form.trigger('modified', args);
      $form.data('dirty', true);
      if ($.fn.watch.DEBUG) {
        debug(oldData, newData);
      }
    }

    //debug(originalData, newData);

    if ($form.data('dirty') && _.isEqual(newData, originalData)) {
      $form.trigger('reverted', args);
      $form.data('dirty', false);
    }

    $form.data('oldData', newData);
  }

  $.fn.watch = function(options) {
    options = options || {};

    return $(this).each(function() {
      var $form = $(this),
          formData = serialize($form, options.ignoreFields);

      $form.on('change keydown keyup', null, options, checkChange);
      $form.data('dirty', false);
      $form.data('oldData', formData);
      $form.data('originalData', formData);
    });
  };

  $.fn.unwatch = function() {
    return $(this).each(function() {
      var $form = $(this);

      $form.off('change keydown keyup', checkChange);
      $form.data('dirty', null);
      $form.data('oldData', null);
      $form.data('originalData', null);
    });
  };

  $.fn.watch.DEBUG = false;

})(jQuery);
