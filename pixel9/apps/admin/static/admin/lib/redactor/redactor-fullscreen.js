if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.fullscreen = {
	init: function()
	{
		this.fullscreen = false;

		this.buttonAdd('fullscreen', 'Fullscreen', $.proxy(this.toggleFullscreen, this));

		if (this.opts.fullscreen) this.toggleFullscreen();
	},
	enableFullScreen: function()
	{
		this.buttonChangeIcon('fullscreen', 'normalscreen');
		this.buttonActive('fullscreen');
		this.fullscreen = true;

		if (this.opts.toolbarExternal)
		{
			this.toolcss = {};
			this.boxcss = {};
			this.toolcss.width = this.$toolbar.css('width');
			this.toolcss.top = this.$toolbar.css('top');
			this.toolcss.position = this.$toolbar.css('position');
			this.boxcss.top = this.$box.css('top');
		}

		this.$box.addClass('redactor_box_fullscreen');

		this.fullScreenResize();
		$(window).resize($.proxy(this.fullScreenResize, this));
		$(document).scrollTop(0, 0);
	},
	disableFullScreen: function()
	{
		this.buttonRemoveIcon('fullscreen', 'normalscreen');
		this.buttonInactive('fullscreen');
		this.fullscreen = false;

		$(window).off('resize', $.proxy(this.fullScreenResize, this));

		this.$box.removeClass('redactor_box_fullscreen');

		if (this.opts.toolbarExternal)
		{
			this.$box.css('top', this.boxcss.top);
			this.$toolbar.css({
				'width': this.toolcss.width,
				'top': this.toolcss.top,
				'position': this.toolcss.position
			});
		}
	},
	toggleFullscreen: function()
	{
		if (!this.fullscreen)
		{
			this.enableFullScreen();
		}
		else
		{
			this.disableFullScreen();
		}
	},
	fullScreenResize: function()
	{
		if (!this.fullscreen) return false;

		var toolbarHeight = this.$toolbar.height();

		var pad = this.$editor.css('padding-top').replace('px', '');
		var height = $(window).height() - toolbarHeight;
		//this.$box.width($(window).width() - 2).height(height + toolbarHeight);

		if (this.opts.toolbarExternal)
		{
			this.$toolbar.css({
				'top': '0px',
				'position': 'absolute',
				'width': '100%'
			});

			this.$box.css('top', toolbarHeight + 'px');
		}

		if (!this.opts.iframe) this.$editor.height(height - (pad * 2));
		else
		{
			setTimeout($.proxy(function()
			{
				this.$frame.height(height);

			}, this), 1);
		}

		this.$editor.height(height);
	}
};
