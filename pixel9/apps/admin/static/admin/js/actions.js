(function($) {
	var lastChecked;

	$.fn.actions = function(opts) {
    var $parent = opts && opts.parent || $('body');
		var options = $.extend({}, $.fn.actions.defaults, opts);
		var actionCheckboxes = $(this);
		var list_editable_changed = false;
		var checker = function(checked) {
			if (checked) {
				showQuestion();
			} else {
				reset();
			}
			$parent.find(actionCheckboxes).prop("checked", checked)
				.parent().parent().toggleClass(options.selectedClass, checked);
		},
		updateCounter = function() {
			var sel = $parent.find(actionCheckboxes).filter(":checked").length;
			// _actions_icnt is defined in the generated HTML
			// and contains the total amount of objects in the queryset
			$parent.find(options.counterContainer).html(interpolate(
			ngettext('%(sel)s of %(cnt)s selected', '%(sel)s of %(cnt)s selected', sel), {
				sel: sel,
				cnt: _actions_icnt
			}, true));
			$parent.find(options.allToggle).prop("checked", function() {
				var value;
				if (sel == actionCheckboxes.length) {
					value = true;
					showQuestion();
				} else {
					value = false;
					clearAcross();
				}
				return value;
			});
		},
		showQuestion = function() {
			$parent.find(options.acrossClears).hide();
			$parent.find(options.acrossQuestions).show();
			$parent.find(options.allContainer).hide();
		},
		showClear = function() {
			$parent.find(options.acrossClears).show();
			$parent.find(options.acrossQuestions).hide();
			$parent.find(options.actionContainer).toggleClass(options.selectedClass);
			$parent.find(options.allContainer).show();
			$parent.find(options.counterContainer).hide();
		},
		reset = function() {
			$parent.find(options.acrossClears).hide();
			$parent.find(options.acrossQuestions).hide();
			$parent.find(options.allContainer).hide();
			$parent.find(options.counterContainer).show();
		},
		clearAcross = function() {
			reset();
			$parent.find(options.acrossInput).val(0);
			$parent.find(options.actionContainer).removeClass(options.selectedClass);
		};
		// Show counter by default
		$parent.find(options.counterContainer).show();
		// Check state of checkboxes and reinit state if needed
		$(this).filter(":checked").each(function(i) {
			$(this).parent().parent().toggleClass(options.selectedClass);
			updateCounter();
			if ($(options.acrossInput).val() == 1) {
				showClear();
			}
		});
		$parent.find(options.allToggle).show().click(function() {
			checker($(this).prop("checked"));
			updateCounter();
		});
		$parent.find(options.acrossQuestions + ' a').click(function(event) {
			event.preventDefault();
			$parent.find(options.acrossInput).val(1);
			showClear();
		});
		$parent.find(options.acrossClears + ' a').click(function(event) {
			event.preventDefault();
			$parent.find(options.allToggle).prop("checked", false);
			clearAcross();
			checker(0);
			updateCounter();
		});
		lastChecked = null;
		$parent.find(actionCheckboxes).click(function(event) {
			if (!event) { event = window.event; }
			var target = event.target ? event.target : event.srcElement;
			if (lastChecked && $.data(lastChecked) != $.data(target) && event.shiftKey === true) {
				var inrange = false;
				$parent.find(lastChecked).prop("checked", target.checked)
					.parent().parent().toggleClass(options.selectedClass, target.checked);
				$parent.find(actionCheckboxes).each(function() {
					if ($.data(this) == $.data(lastChecked) || $.data(this) == $.data(target)) {
						inrange = (inrange) ? false : true;
					}
					if (inrange) {
						$(this).prop("checked", target.checked)
							.parent().parent().toggleClass(options.selectedClass, target.checked);
					}
				});
			}
			$parent.find(target).parent().parent().toggleClass(options.selectedClass, target.checked);
			lastChecked = target;
			updateCounter();
		});
		$parent.find('form#changelist-form table#result_list tr').find('td:gt(0) :input').change(function() {
			list_editable_changed = true;
		});
		$parent.find('form#changelist-form button[name="index"]').click(function(event) {
			if (list_editable_changed) {
				return confirm(gettext("You have unsaved changes on individual editable fields. If you run an action, your unsaved changes will be lost."));
			}
		});
		$parent.find('form#changelist-form input[name="_save"]').click(function(event) {
			var action_changed = false;
			$('select option:selected', options.actionContainer).each(function() {
				if ($(this).val()) {
					action_changed = true;
				}
			});
			if (action_changed) {
				if (list_editable_changed) {
					return confirm(gettext("You have selected an action, but you haven't saved your changes to individual fields yet. Please click OK to save. You'll need to re-run the action."));
				} else {
					return confirm(gettext("You have selected an action, and you haven't made any changes on individual fields. You're probably looking for the Go button rather than the Save button."));
				}
			}
		});
	};
	/* Setup plugin defaults */
	$.fn.actions.defaults = {
		actionContainer: "div.actions",
		counterContainer: "span.action-counter",
		allContainer: "div.actions span.all",
		acrossInput: "div.actions input.select-across",
		acrossQuestions: "div.actions span.question",
		acrossClears: "div.actions span.clear",
		allToggle: '.action-checkbox-column [type="checkbox"]',
		selectedClass: "selected"
	};
})(django.jQuery);
