app = window.app || {};

app.utils = {
  normalizeUrl: function(url){
    if (url.indexOf('/') === 0) {
      return url;
    }
    return location.pathname + url;
  },

  uniqueId: function(){
    return Math.random().toString(36).substr(2, 16);
  },

  isTouchDevice: function(){
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    var mq = function(query) {
      return window.matchMedia(query).matches;
    }

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
      return true;
    }

    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
  },

  ensureUrlParam: function(url, key, value) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
      hash;

    if (re.test(url)) {
      if (typeof value !== 'undefined' && value !== null) {
        return url.replace(re, '$1' + key + "=" + value + '$2$3');
      }
      else {
        hash = url.split('#');
        url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
        if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
          url += '#' + hash[1];
        }
        return url;
      }
    }
    else {
      if (typeof value !== 'undefined' && value !== null) {
        var separator = url.indexOf('?') !== -1 ? '&' : '?';
        hash = url.split('#');
        url = hash[0] + separator + key + '=' + value;
        if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
          url += '#' + hash[1];
        }
        return url;
      }
      else {
        return url;
      }
    }
  },

  removeUrlParam: function(_url, parameter) {
    var url = _url || '';
    var urlparts= url.split('?');
    if (urlparts.length>=2) {

      var prefix= encodeURIComponent(parameter)+'=';
      var pars= urlparts[1].split(/[&;]/g);

      //reverse iteration as may be destructive
      for (var i= pars.length; i-- > 0;) {
        //idiom for string.startsWith
        if (pars[i].lastIndexOf(prefix, 0) !== -1) {
          pars.splice(i, 1);
        }
      }

      return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
    }
    return url;
  },

  // Where $.fn.find only matches descendants, findAll will test all the
  // top level elements in the jQuery object as well.
  findAll: function(elems, selector) {
    return elems.filter(selector).add(elems.find(selector));
  },

  shouldHandleClick: function(e){
    if ( e.which !== 1 || e.metaKey || e.ctrlKey || e.shiftKey || e.altKey ) {
      return;
    }

    if (e.defaultPrevented) return;

    var $el = $(e.currentTarget),
        href = $el.attr('href'),
        target = $el.attr('target');

    if (typeof href === 'undefined') {
      return false;
    }

    if (target) {
      return;
    }

    if (href.indexOf(app.conf.root) !== 0 && href.indexOf('/') === 0) {
      return false;
    }

    if (href.indexOf('avascript:') === 1) {
      return false;
    }

    if (href.indexOf('#') === 0) {
      return false;
    }

    if (href.indexOf('mailto:') === 0) {
      return false;
    }

    if (href.indexOf('http://') === 0) {
      return false;
    }

    if (href.indexOf('https://') === 0) {
      return false;
    }

    return true;
  }
};
