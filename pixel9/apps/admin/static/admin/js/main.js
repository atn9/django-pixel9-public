/* globals app bootbox Backbone jQuery NProgress */
;(function ($) {
  var router = app.router = window.page
  var utils = app.utils
  var handlers = app.handlers = {}

  bootbox.addLocale('fr', {
    OK      : 'OK',
    CANCEL  : 'Annuler',
    CONFIRM : 'Confirmer'
  });

  bootbox.setLocale('fr');

  app.isTouchDevice = app.utils.isTouchDevice()

  $.fn.relatedWidgetWrapper = function () {
    return $(this).each(function () {
      function updateLinks () {
        var $this = $(this)
        var siblings = $this.parents('.form-group,td[class^=field-]').find('.change-related, .delete-related')

        if (!siblings.length) return

        var value = $this.val()
        if (value) {
          siblings.each(function () {
            var elm = $(this)
            elm.attr('href', elm.attr('data-href-template').replace('__fk__', value))
          })
        } else {
          siblings.removeAttr('href')
        }
      }

      $(this).on('change', updateLinks)
      $.proxy(updateLinks, this)()
    })
  }

  $.ajaxSetup({
    converters: {
      // return response as an object for text/html responses
      "text html": function(html){
        return {content: html}
      }
    }
  });

  app.initWidgets = function (context) {
    var $context = $(context)
    var $autocompletes = $context.find('.admin-autocomplete').not('.inline-group .empty-form .admin-autocomplete')

    // make sure ARMRS-946-S is the first choice and not ARMRS-946-SL when pasting ARMRS-946-S
    // https://github.com/select2/select2/issues/4694
    var prefixSorter = function(results) {
      if (!results || results.length == 0)
        return results

      // Find the open select2 search field and get its value
      var field = document.querySelector('.select2-search__field')
      var term = ''
      if (field !== null) {
        term = field.value.toLowerCase()
      }
      if (term.length == 0)
        return results

      return results.sort(function(a, b) {
        aExact = a.text.toLowerCase() == term || a.text.toLowerCase().substr(term.length, 1) === ' '
        bExact = b.text.toLowerCase() == term || b.text.toLowerCase().substr(term.length, 1) === ' '
        return bExact - aExact // If one is an exact match or has a space character after the term, push to the top. Otherwise, no sorting.
      })
    }


    // attach autocompletes to closest .modal-body if it exists
    // https://select2.org/troubleshooting/common-problems
    setTimeout(function(){

      $autocompletes.each(function(){
        var $modalBody = $(this).closest('.modal-body')
        var isMultiple = $(this).is('[multiple]')

        var $select2 = $(this).djangoAdminSelect2({
          dropdownParent: $modalBody.length && $modalBody || $('body'),
          sorter: prefixSorter,
          closeOnSelect: ! isMultiple,
          dropdownCssClass: isMultiple ? 'select2-dropdown-multiple': 'select2-dropdown-single'
        })

        // unselect on enter key for select[multiple]
        if (isMultiple) {
          var data = $select2.data('select2')

          data.selection.$search[0].addEventListener('keydown', function(e){
            if (e.keyCode === 13 && data.$container.is('.select2-container--open')) {
              var $result = data.$results.find('.select2-results__option--highlighted[aria-selected=true]')
              if ($result.length) {
                e.stopImmediatePropagation()
                e.preventDefault()
                $result.trigger('mouseup').addClass('select2-results__option--highlighted');
              }
            }
          })

          data.selection.$search[0].addEventListener('keyup', function(e){
            if (e.keyCode === 13 && data.$container.is('.select2-container--open')) {
              e.stopImmediatePropagation()
              e.preventDefault()
            }
          })

        }

      })
    }, 1)

    // add placeholder to search input of autocompletes
    $autocompletes.not('[multiple]').one('select2:open', function(e) {
      $(this).data('select2').dropdown.$search.prop('placeholder', 'Rechercher...');
    })

    // keep focus on select and unselect if closeOnSelect: false
    $autocompletes.filter('[multiple]').on('select2:select select2:unselect select2:clear', function(e){
      if ( ! $(this).data('select2').options.get('closeOnSelect')) {
        $(this).focus()
      }
    });

    // prevent select[multiple] from closing or opening on unselect
    $autocompletes.filter('[multiple]').on('select2:unselect', function(e){
      var $select = $(this)
      if ( ! $select.data('select2').options.get('cloneOnSelect')) {
        $select.data('select2-unselecting', true)
        setTimeout(function(){
          $select.data('select2-unselecting', false)
        }, 20)
      }
    }).on('select2:closing', function(e){
      var $select = $(this)
      if ( ! $select.data('select2').options.get('cloneOnSelect')) {
        if ($select.data('select2-unselecting')) {
          e.preventDefault()
        }
        $(this).data('select2-unselecting', false)
      }
    }).on('select2:opening', function(e){
      var $select = $(this)
      if ( ! $select.data('select2').options.get('cloneOnSelect')) {
        if ($select.data('select2-unselecting')) {
          e.preventDefault()
        }
        $select.data('select2-unselecting', false)
      }
    })

    // fix: django inlines select2 dropdown element not visible on Chrome
    //$context.on('keydown', '.select2-search__field', function(e){
      //var target = $(e.target).closest('.select2-container')
      //// https://stackoverflow.com/questions/8840580/force-dom-redraw-refresh-on-chrome-mac
      //setTimeout(function(){ target.hide().show(0) }, 0);
    //});

    // Fix: Typing Doesn't Trigger Search
    // https://github.com/select2/select2/issues/3279
    $context.on('keydown', '.select2-selection', function(e) {
      if (e.which < 32)
        return

      var target = $(e.target).closest('.select2-container')
      if (!target.length)
        return

      target = target.prev()
      target.select2('open')

      var search = target.data('select2').dropdown.$search || target.data('select2').selection.$search
      search.focus()
    })

    // select2: select highlighted item on tab keydown
    //$autocompletes.on('select2:close', function(e) {
      //var $select = $(e.target);

      //$(document).on('keydown.select2', function(e) {
        //if (e.which === 9) { // tab
          //var highlighted = $select.data('select2').$dropdown.find('.select2-results__option--highlighted')
          //if (highlighted) {
            //var id = highlighted.data('data').id
            //var text = highlighted.data('data').text
            //$select.find('option[selected]').prop('selected', false)
            //$select.append('<option value="'+id+'">'+text+'</option>').prop('selected', true)
            //$select.val(id).trigger('change')
          //}
        //}
      //})
      //// unbind the event again to avoid binding multiple times
      //setTimeout(function() {
          //$(document).off('keydown.select2')
      //}, 1)
    //})
    //

    // use data-dismiss-modal="#MODAL_ID" instead of standard data-dismiss="modal" to prevent also closing parents modal
    // this is only needed for nested inline modals
    // see: admin/includes/modal.html
    $context.on('click', '[data-dismiss-modal]', function(){
      var modalId = $(this).data('dismiss-modal')
      $context.find(modalId).modal('hide')
    });


    $context.find('.date-field > input,[bind="date-picker"]').not('.inline-group .empty-form input').each(function() {
      var defaults = {
        format: 'dd/mm/yyyy',
        language: 'fr',
        autoclose: true,
        todayBtn: 'linked',
        todayHighlight: true,
      }
      // fix: datepickers and slip.js conflict in tabular inlines
      if ($context.is('tr')) {
        defaults['container'] = $(this).parent()
      }

      var options = $.extend({}, defaults, $(this).data())
      $(this).datepicker(options)
    })

    $context.find('.time-field > input,[bind="time-picker"]').timepicker({
      defaultTime: false,
      showSeconds: true,
      template: false,
      showInputs: false,
      minuteSteps: 1,
      secondsStep: 1,
      showMeridian: false
    })

    $context.find('[bind="daterange-picker"]').each(function(){
      var $input = $(this);

      function startDate() {
        var val = $input.val();
        if (val) {
          return val.split(' - ')[0];
        }
        return moment.now();
      }
      function endDate() {
        var val = $input.val();
        if (val) {
          return val.split(' - ')[1];
        }
        return moment.now();
      }
      $input.daterangepicker({
        locale: {
          format: 'DD/MM/YYYY',
          customRangeLabel: "Personnalisées",
          daysOfWeek: [ "Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa" ],
          monthNames: [ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre" ],
          cancelLabel: 'Annuler',
          applyLabel: 'Appliquer'
        },
        autoApply: false,
        opens: 'right',
        autoUpdateInput: false,
        showDropdowns: true,
        linkedCalendars: false,
        alwaysShowCalendars: true,
        minYear: 2000,
        maxYear: 2040,
        startDate: startDate(),
        endDate: endDate(),
        ranges: {
            'Aujourd\'hui': [moment(), moment()],
            'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Cette semaine': [moment().startOf('week'), moment().endOf('week')],
            'La semaine passée': [moment().subtract(1, 'weeks').startOf('week'), moment().subtract(1, 'weeks').endOf('week')],
            '7 derniers jours': [moment().subtract(6, 'days'), moment()],
            'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
            'Le mois passé': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            '30 derniers jours': [moment().subtract(29, 'days'), moment()],
            'Cette année': [moment().startOf('year'), moment().endOf('year')],
            'L\'année passée': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
            '365 derniers jours': [moment().subtract(365, 'days'), moment()]
          }
      });

      $input.on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
      });

      $input.on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
      });

    });

    $context.find('[data-toggle="tooltip"]').tooltip()

    $context.find('[data-toggle="popover"]').popover({
      content: function(){
        var $el = $(this)
        var contentSelector = $el.data('content-selector')
        var $content

        if (contentSelector) {
          $content = $(contentSelector)

          if (! $el.data('content')) {
            $el.data('content', $content.html())
            $content.remove()
          }
        }
        return $el.data('content')
      }
    });

    setTimeout(function () {
      // setTimeout: search doesn't work when inside a modal
      $context.find('select[multiple]').not('.admin-autocomplete').not('[name*="__prefix__"]').customMultiSelect()
    }, 10)

    $context.find('.related-widget-wrapper').find('select,input[type="hidden"]').relatedWidgetWrapper()

    autosize($context.find('textarea').not(':hidden').not('.redactor_box textarea'))

    // update position field on parent field change
    $context.find('select.tree-positioner-parent').each(function () {
      var $this = $(this)
      var $position = $('select.tree-positioner-position')
      var relObj = JSON.parse($this.attr('rel'))

      $this.change(function (e) {
        var html = ''
        for (var x = 0, y = relObj[$(this).val()]; x < y; x++) {
          var selected = (x === 0) ? ' selected="selected"' : ''
          html += '<option value="' + x + '"' + selected + '>' + (x + 1) + '</option>'
        }

        $position.html(html)
      })
    })

    $context.data('widgetsRendered', true);
  }

  // Hide popover by clicking outside
  // http://stackoverflow.com/questions/11703093/how-to-dismiss-a-twitter-bootstrap-popover-by-clicking-outside
  $(document).on('click', function(e) {
    $('[data-toggle="popover"]').each(function() {
      //the 'is' for buttons that trigger popups
      //the 'has' for icons within a button that triggers a popup
      if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
        $(this).popover('hide');

        if ($(this).data('bs.popover').inState) {
          $(this).data('bs.popover').inState.click = false // fix for BS 3.3.6
        }
      }
    })
  })

  $(document).on('formset:added', function (e, row, prefix) {
    app.initWidgets(row)
    // trigger change for jquery.formWatch
    setTimeout(function(){
      row.parents('.change-form').trigger('change')
    }, 10);
  })

  $(document).on('formset:removed', function (e, row, prefix) {
    // trigger change for jquery.formWatch
    setTimeout(function(){
      $('.' + prefix + '-group').parents('.change-form').trigger('change')
    },10);
  })

  var dirtyForm = function (context) {
    var $context = $(context || document)
    var $forms = $context.find('form')
    var $form
    var dirty

    $forms.each(function () {
      $form = $(this)
      if (!$form.data('submitting')) {
        $form.trigger('change')
        if ($form.data('dirty')) {
          dirty = true
          return
        }
      }
      $form.data('submitting', false)
    })
    return dirty
  }

  $(document).on('submit', 'form', function(){
    $(this).data('submitting', true)
  })

  var confirmUnsavedChanges = function (context) {
    if (dirtyForm(context)) {
      return window.confirm('Certains changements n\'ont pas été sauvegardés voulez-vous continuer quand même?')
    }
  }

  var handleContentDisposition = function(response, xhr) {
    var filename = '';
    var disposition = xhr.getResponseHeader('Content-Disposition');
    if (disposition) {
      if (disposition.indexOf('attachment') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
          filename = matches[1].replace(/['"]/g, '');
        }
      }
      var URL = window.URL || window.webkitURL;
      var downloadUrl = URL.createObjectURL(response);

      if (filename) {
        // use HTML5 a[download] attribute to specify filename
        var a = document.createElement("a");
        // safari doesn't support this yet
        if (typeof a.download === 'undefined') {
          window.location.href = downloadUrl;
        } else {
          a.href = downloadUrl;
          a.download = filename;
          document.body.appendChild(a);
          a.click();
        }
      } else {
        window.location.href = downloadUrl;
      }

      setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
      return true
    }
    return false
  }

  handlers.main = function (ctx, next) {
    if (ctx.init) {
      app.mainView.render()
      app.mainView.mainPanel.initWidgets()
      app.mainView.mainPanel.ensureFormErrorsVisibility()
      next()
    } else {
      $.ajax({
        url: ctx.path,
        success: function (response, status, xhr) {
          if (response.dialog) {
            return app.mainView.handleDialog(response)
          }

          if (response.content) {
            return app.mainView.render(response.content)
          }

          if (response.redirect) {
            router.redirect(response.redirect)
            $('html,body').scrollTop(0);
          }

          if (response.alert) {
            bootbox.alert(response.alert)
          }
          next()
        }
      })
    }
  }

  router('*', handlers.main)

  router.exit('*', function (ctx, next) {
    if (confirmUnsavedChanges() === false) {
      setTimeout(function(){
        router.replace(ctx.path, ctx.state, false, false)
      }, 1)
      return
    }
    $('.modal').modal('hide')
    // hide sidebar on mobiles
    $('.sidebar.collapse.in').removeClass('in')
    next()
  })

  app.MainView = Backbone.View.extend({
    el: 'body',
    initialize: function () {
      this.mainPanel = new app.MainPanelView()
    },
    render: function (content) {
      this.assign(this.mainPanel, '.main-panel', content)
    },
    assign: function (view, selector, content) {
      // http://ianstormtaylor.com/renering-views-in-backbonejs-isnt-always-simple/http://ianstormtaylor.com/rendering-views-in-backbonejs-isnt-always-simple/
      view.setElement(this.$(selector)).render(content)
    }
  })

  app.MainPanelView = Backbone.View.extend({
    events: {
      'submit form[bind="xhr"]:not(.django-popup form)': 'submitForm',
      'click [type="submit"]': 'submitBtnClick',
      'click [type="submit"][bind="xhr"]:not(.django-popup [type="submit"])': 'submitForm',
      'click .add-related': 'addRelated',
      'click .change-related': 'changeRelated',
      'click .delete-related': 'deleteRelated',
      'click .related-lookup': 'relatedLookup',
      'click .raw-id-field .clear': 'clearRawIdField',
      'click [data-dialog]': 'dialog'
    },

    submitBtnClick: function (e) {
      var $submit = $(e.currentTarget);
      var submitName = $submit.attr('name')
      var formAttr = $submit.attr('form')
      var $form

      if (formAttr) {
        $form = $('#'+formAttr)
      } else {
        $form = $submit.parents('form')
      }
      var $hidden = $('[type="hidden"][name="' + submitName + '"]')

      if (!$hidden.length) {
        $hidden = $('<input type="hidden" name="' + submitName + '"/>')
        $form.prepend($hidden);
      }

      $hidden.val($submit.val())
    },

    initWidgets: function () {
      app.initWidgets(this.$el)
    },

    scrollTop: function () {
      $('html,body').scrollTop(0)
    },

    addRelated: function (e) {
      return this.djangoPopup(e, $(e.currentTarget).attr('href'), 'add', {'_popup': '1'})
    },

    changeRelated: function (e) {
      return this.djangoPopup(e, $(e.currentTarget).attr('href'), 'change', {'_popup': '1'})
    },

    deleteRelated: function (e) {
      return this.djangoPopup(e, $(e.currentTarget).attr('href'), 'delete', {'_popup': '1'}, null, 'small')
    },

    relatedLookup: function (e) {
      var $target = $(e.currentTarget)
      if (e.type == 'click') {
        this.djangoPopup(e, $target.attr('href'), 'lookup', {'_popup': '1'})
        e.stopImmediatePropagation()
        e.preventDefault()
      }
    },

    clearRawIdField: function (e) {
      var $a = $(e.currentTarget);
      var $field = $a.parents('.raw-id-field')
      $field.addClass('raw-id-field-empty')
      $field.find('input').val('').trigger('change')
      $field.find('.link-label').html('');
      $a.addClass('hidden');
    },

    dialog: function (e) {
      return this.djangoPopup(e, $(e.currentTarget).attr('href'), $(e.currentTarget).data('dialog') || '', {}, $(e.currentTarget).data('dialog-title'))
    },

    djangoPopup: function (e, url, type, params, title, size) {
      if (e.type === 'click') {
        if (!utils.shouldHandleClick(e)) {
          return
        }
      }

      e.preventDefault()
      e.stopImmediatePropagation()

      size = size || 'large'
      var view = this
      var $target = $(e.currentTarget)
      var uniqueId = 'modal-' + app.utils.uniqueId()


      $target.data('modal-uid', uniqueId)

      if (params) {
        $.each(params, function (k, v) {
          url = utils.ensureUrlParam(url, k, v)
        })
      }

      var targetSize = $target.data('dialog-size')
      if (targetSize) {
        size = targetSize
      }

      // if the current page will be refreshed on dialog close
      // check now for dirty forms before it's to late
      if ($target.data('dialog-onclose') === 'refresh' || $target.data('dialog-onclose') === 'reload') {
        var cancel = confirmUnsavedChanges(view.$el)
        if (cancel === false) {
          return
        } else {
          view.$el.find('form').data('dirty', false)
        }
      }

      $.ajax({
        url: url,
        method: 'get',
        success: function (response, status, xhr) {
          var $el = $('<div class="django-popup"/>')
          var modalContentView = new app.ModalContentView({el: $el})

          // used to to handle relative urls inside the dialog
          modalContentView.setUrl(url)

          var dialog = bootbox.dialog({
            show: false,
            title: title || modalContentView.extractTitle(response.content) || ' ', // space to always show .modal-header
            message: modalContentView.$el,
            animate: false,
            size: size
          })

          dialog.attr('id', uniqueId)

          modalContentView.modal = dialog

          dialog.on('hide.bs.modal', function (e) {
            if (e.target === e.currentTarget) {
              var cancel = confirmUnsavedChanges($(e.currentTarget))
              if (cancel === false) {
                e.preventDefault()
                e.stopImmediatePropagation()
              }
            }
          })

          dialog.on('hidden.bs.modal', function (e) {
            var modal = $(this)

            if ($target.data('dialog-onclose') === 'refresh' || $target.data('dialog-onclose') === 'reload') {
              view.reload()
            }

            if (type === 'lookup') {
              $target.data('paused', false)
              setTimeout(function(){ $target.focus() }, 1)
            }
          })

          // used by dismissRelatedLookupPopup
          if ($target.attr('id')) {
            dialog.data('django-window', $target.attr('id').replace(new RegExp('^' + type + '_'), ''))
          }

          dialog.modal('show')
          // focus first input instead of save button
          // dialog.off("shown.bs.modal").modal("show").find('input:visible').eq(0).focus()
          //

          if (response.redirect) {
            modalContentView.handleRedirect(response.redirect)
          }

          if (response.content) {
            modalContentView.render(response.content)
          }

          modalContentView.scrollTop()
          if (!app.isTouchDevice) {
            modalContentView.$el.find('[autofocus]').first().focus()
          }
        }
      })
    },

    ensureFormErrorsVisibility: function () {
      var view = this
      view.$('fieldset.collapsable').each(function (i, elem) {
        // Don't hide if fields in this fieldset have errors
        if ($(elem).find('div.has-error').length) {
          $(elem).find('.collapse').addClass('in')
          $(elem).find('[data-toggle="collapse"]').removeClass('collapsed')
        }
      })

      view.$('.tab-pane').each(function (i, elem) {
        var $pane = $(this)
        var $toggle = view.$('[data-toggle="tab"]').filter(function () {
          return $pane.is($(this).data('target') || $(this).attr('href'))
        })

        if ($(elem).find('.has-error').length === 0) {
          $toggle.parent().removeClass('has-error')
        } else {
          $toggle.parent().addClass('has-error')
        }
      })

      view.$('.tab-content').each(function () {
        var $pane = $(this).find('.has-error').first().closest('.tab-pane')
        if ($pane.length) {
          var $toggle = view.$('[data-toggle="tab"]').filter(function () {
            return $pane.is($(this).data('target') || $(this).attr('href'))
          })
          $toggle.first().tab('show')
        }
      })
    },

    reload: function (e) {
      router.show(window.location.href)
    },


    submitForm: function (e) {
      var formAttr = $(e.currentTarget).attr('form')
      var $form

      if (formAttr) {
        $form = $('#'+formAttr)
      } else {
        $form = $(e.currentTarget).closest('form')
      }
      var form = $form[0]
      var view = this

      if ($form.attr('method').toUpperCase() === 'POST') {
        $.ajax({
          url: view.normalizeUrl($form.attr('action')),
          type: 'POST',
          data: new FormData(form),
          xhr: function() {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
              if (xhr.readyState == 2) {
                if(xhr.getResponseHeader('Content-Disposition')) {
                  xhr.responseType = "blob";
                }
              }
            }
            return xhr
          },
          success: function (response, status, xhr) {
            if (handleContentDisposition(response, xhr)) {
              return
            }

            $form.data('dirty', false)

            if (response.dialog) {
              view.handleDialog(response)
              return
            }

            if (response.content) {
              view.render(response.content)
              view.scrollTop();
              return
            }

            if (response.redirect) {
              router.redirect(response.redirect)
              view.scrollTop();
              return
            }
          },
          cache: false,
          contentType: false,
          processData: false
        });

      } else {
        var url = view.normalizeUrl(($form.attr('action') || '').split('?')[0])
        var formData = $.deparam($form.serialize())

        $.each(formData, function (key, value) {
          url = utils.ensureUrlParam(url, key, encodeURIComponent(value))
        })
        router.show(url)
      }

      return false
    },

    renderDialog: function(content, className) {
      var view = this
      var $content = $(content)
      var $title = utils.findAll($content, 'title')
      var title = $title.last().text()
      var dialog
      var validClassName = className && className !== true

      if (validClassName) {
        dialog = $('.modal.' + className)
      }

      if (dialog && dialog.length) {
        if (title) {
          dialog.find('.modal-title').text(title)
        }
        dialog.find('.modal-body').html(content)

      } else {
        dialog = bootbox.dialog({
          title: title,
          message: content,
          animate: false,
          className: className,
          // Position the dialog inside ModalContentView.$el
          // for the event handlers to function properly.
          //
          // This clips inner dialogs that are taller than ModalContentView.$el
          // because css property `overflow` is set to `hidden` on
          // .modal and .modal-body of the parent dialog (ModalContentView.$el)
          container: view.$el
        })
      }

      if (!app.isTouchDevice) {
        dialog.find('.modal-body').find('[autofocus]').first().focus()
      }
      view.initWidgets()
    },

    handleDialog: function (response) {
      var view = this
      if (response.url) {
        $.get(response.url, function(data) {
          view.renderDialog(data.content, response.dialog)
        })
      } else {
        view.renderDialog(response.content, response.dialog)
      }
    },

    extractTitle: function (content) {
      var $title = utils.findAll($(content), 'title')
      var title = $title.last().text()
      $title.remove()
      return title
    },

    setTitle: function (title) {
      document.title = title
    },

    getUrl: function () {
      return window.location.href
    },

    render: function (content) {
      // we cannot redefine window.opener on firefox
      if (typeof content !== 'undefined') {

        this.$el.html(content)
        this.setTitle(this.extractTitle(content))
        this.initWidgets()
        this.ensureFormErrorsVisibility()

        if (!app.isTouchDevice) {
          this.$el.find('[autofocus]').first().focus()
        }
      }

      return this
    },

    normalizeUrl: function (url) {
      return utils.normalizeUrl(url)
    }
  })

  app.ModalContentView = app.MainPanelView.extend({
    initialize: function () {
      this.history = []
      this.historyCursor = 0
    },

    events: function () {
      var events = {
        'submit form': 'submitForm',
        'click [href]': 'handleClick',
        'click [data-popup-opener]': 'dismissRelatedLookupPopup'
      }
      return $.extend({}, app.MainPanelView.prototype.events, events)
    },

    normalizeUrl: function (url) {
      if (url.indexOf('_popup=') !== -1) {
        url = utils.ensureUrlParam(url, '_popup', '1')
      }

      if (url.indexOf('/') === 0) {
        return url
      } else {
        return this.getUrl().split('?')[0] + url
      }
    },

    setUrl: function (url, cursor) {
      url = this.normalizeUrl(url)

      // used to to handle relative urls inside the modal
      this.$el.data('modal-content-url', url)

      if (typeof cursor !== 'undefined') {
        this.historyCursor = cursor;
        // redirections replace current url
        this.history[cursor] = url
      } else {
        this.historyCursor += 1
        this.history[this.historyCursor] = url
        this.history.splice(this.historyCursor+1, this.history.length)
      }
    },

    getUrl: function () {
      return this.$el.data('modal-content-url')
    },

    loadUrl: function (url, cursor) {
      var view = this
      var cancel = confirmUnsavedChanges(view.$el)

      url = this.normalizeUrl(url)

      if (cancel !== false) {
        this.setUrl(url, cursor)

        $.ajax({
          url: this.normalizeUrl(url),
          success: $.proxy(this.handleResponse, this)
        })
      }
    },

    hasPreviousUrl: function() {
      if (typeof this.history[this.historyCursor - 1] !== 'undefined') {
        return true
      }
    },

    hasNextUrl: function() {
      if (typeof this.history[this.historyCursor + 1] !== 'undefined') {
        return true
      }
    },

    getPreviousUrl: function() {
      if (this.hasPreviousUrl()) {
        return this.history[this.historyCursor - 1]
      }
    },

    getNextUrl: function() {
      if (this.hasNextUrl()) {
        return this.history[this.historyCursor + 1]
      }
    },

    reload: function () {
      return this.loadUrl(this.getUrl(), this.historyCursor)
    },

    scrollTop: function () {
      this.$el.parents('.modal').scrollTop(0)
    },

    setTitle: function (title) {
      var view = this;
      var $title = this.$el.parents('.modal-dialog').find('.modal-title').first()
      var $header = this.$el.parents('.modal-dialog').find('.modal-header').first()
      var url = app.utils.removeUrlParam(this.getUrl(), '_popup')
      var $history = $header.find('.history').first()
      var $previous = $('<a class="history-back material-icons">arrow_back</a>')
      var $next = $('<a class="history-forward material-icons">arrow_forward</a>')
      var $reload = $('<a class="history-reload material-icons">refresh</a>')

      if (! $history.length) {
        $history = $('<span class="history"></span>')
      }

      $reload.on('click', function(){
        view.reload()
      })

      if (this.hasPreviousUrl()) {
        $previous.attr('href', this.getPreviousUrl())
        $previous.data('cursor', this.historyCursor - 1)
        $previous.on('click', function(){
          view.loadUrl($previous.attr('href'), $previous.data('cursor'))
          return false;
        })
      } else {
        $previous.prop('disabled', true).addClass('disabled')
      }

      if (this.hasNextUrl()) {
        $next.attr('href', this.getNextUrl())
        $next.data('cursor', this.historyCursor + 1)
        $next.on('click', function(){
          view.loadUrl($next.attr('href'), $next.data('cursor'))
          return false;
        })
      } else {
        $next.prop('disabled', true).addClass('disabled')
      }

      if ($title.length) {
        $title.html('')
        $history.html('')
        $history.append($previous)
        $history.append($next)
        $history.append($reload)
        $header.prepend($history)
        url = app.utils.removeUrlParam(url, '_to_field')
        url = app.utils.removeUrlParam(url, '_popup')
        $title.append('<a target="_blank" href="'+ url +'">'+title+'&nbsp;<span class="material-icons new-tab-icon">open_in_new</span></a>');
      }
    },

    submitForm: function (e) {
      var formAttr = $(e.currentTarget).attr('form')
      var $form
      if (formAttr) {
        $form = $('#'+formAttr)
      } else {
        $form = $(e.currentTarget).closest('form')
      }
      var form = $form[0]
      var url = this.normalizeUrl($form.attr('action') || '')

      if ($form.attr('method').toUpperCase() === 'POST') {
        this.setUrl(url)

        $.ajax({
          url: url,
          type: 'POST',
          data: new FormData(form),
          success: $.proxy(this.handleFormResponse, this),
          cache: false,
          contentType: false,
          processData: false
        });
      } else {
        url = url.split('?')[0]
        var formData = $.deparam($form.serialize())

        $.each(formData, function (key, value) {
          url = utils.ensureUrlParam(url, key, encodeURIComponent(value))
        })

        this.setUrl(url)

        $.ajax({
          url: url,
          success: $.proxy(this.handleFormResponse, this)
        })
      }
      return false
    },

    handleClick: function (e) {
      if (!utils.shouldHandleClick(e)) {
        return
      }
      if ($(e.currentTarget).data('popup-opener')) {
        return
      }
      var href = $(e.currentTarget).attr('href')
      this.loadUrl(href)
      this.scrollTop()
      return false
    },

    handleResponse: function (response, status, xhr) {
      if (response.dialog) {
        return this.handleDialog(response)
      }

      if (response.closeModal) {
        return this.closeModal(response)
      }

      if (response.content) {
        return this.render(response.content)
      }

      if (response.redirect) {
        return this.handleRedirect(response.redirect)
      }
    },

    handleFormResponse: function (response, status, xhr) {
      this.scrollTop()
      return this.handleResponse(response, status, xhr)
    },

    closeModal: function(response) {
      this.modal.modal('hide')
    },

    handleRedirect: function (url) {
      var view = this
      url = this.normalizeUrl(url)

      $.get(url, function (response, status, xhr) {
        view.setUrl(url, view.historyCursor)
        view.handleResponse(response, status, xhr)
        view.scrollTop()
      })
    },

    dismissRelatedLookupPopup: function (e) {
      e.preventDefault()
      e.stopImmediatePropagation()

      var $a = $(e.currentTarget)
      var $modal = $(e.currentTarget).closest('.modal')
      var name = $modal.data('django-window')
      var $input = $('#' + name)
      var $field = $input.parents('.raw-id-field')
      var $clear = $input.parent().find('.clear')
      var id = $a.data('popup-opener')

      $modal.modal('hide')

      if (id) {
        if ($input.is('.vManyToManyRawIdAdminField') && $input.val()) {
          $input.val($input.val() + ',' + id)
        } else {
          $input.val(id)
        }
      }
      if ($input.data('label-url')) {
        $.get(app.utils.ensureUrlParam($input.data('label-url'), 'object_id', id), function(data){
          if (data.label) {
            $input.next().html(data.label);
          }
        });
      }

      if ($clear.length) {
        $clear.removeClass('hidden')
      }
      $field.removeClass('raw-id-field-empty')
      $input.trigger('change');
    }

  })

  // django admin compatibility
  // window.opener = function () {}
  window.addEvent = function () {}
  window.showAddAnotherPopup = function () {}
  window.showRelatedObjectLookupPopup = function () {}

  app.dismissAddRelatedObjectPopup = function (name, newId, newRepr) {
    var elem = document.getElementById(name)
    var $elem = $(elem)

    if ($elem.is('select')) {
      var elemName = elem.nodeName.toUpperCase()
      if (elemName === 'SELECT') {
        $elem.append('<option value="' + newId + '" selected>' + newRepr + '</option>')
      } else if (elemName === 'INPUT') {
        if (elem.className.indexOf('vManyToManyRawIdAdminField') !== -1 && elem.value) {
          elem.value += ',' + newId
        } else {
          elem.value = newId
        }
      }
    } else {
      // RawIdField
      var $clear = $elem.parent().find('.clear')
      var $field = $elem.parents('.raw-id-field')

      $elem.val(newId)

      if ($elem.data('label-url')) {
        $.get(app.utils.ensureUrlParam($elem.data('label-url'), 'object_id', newId), function(data){
          if (data.label) {
            $elem.next().html(data.label);
          }
        });
      } else {
        $elem.next().text(newRepr)
      }

      if ($clear.length) {
        $clear.removeClass('hidden')
      }
      $field.removeClass('raw-id-field-empty')
      $elem.trigger('change');
    }

    if ($elem.data('multiselect')) {
      $elem.multiSelect('refresh')
    }
    $elem.trigger('change')
  }

  app.dismissChangeRelatedObjectPopup = function (name, objId, newRepr, newId) {
    var $input = $('#' + name + ', #' + name + '_from, #' + name + '_to')
    var $elem = $input
    if ($input.is('select')) {
      $input.find('option').each(function () {
        if (this.value === objId) {
          this.innerHTML = newRepr
          this.value = newId
        }
      })
      $input.trigger('change').select2()
    } else {
      // RawIdField
      $elem.val(newId)

      if ($elem.data('label-url')) {
        $.get(app.utils.ensureUrlParam($elem.data('label-url'), 'object_id', newId), function(data){
          if (data.label) {
            $elem.next().html(data.label);
          }
        });
      } else {
        $elem.next().text(newRepr)
      }
      $input.val(newId).next().text(newRepr)
      $input.trigger('change');
    }
  }

  app.dismissDeleteRelatedObjectPopup = function (name, value) {
    var $input = $('#' + name + ', #' + name + '_from, #' + name + '_to')
    if ($input.is('select')) {
      $input.find('option').each(function () {
        if (this.value === value) {
          $(this).remove()
        }
      })
      $input.trigger('change').select2()
    } else {
      // RawIdField
      var $clear = $input.parent().find('.clear')
      var $field = $input.parents('.raw-id-field')
      $input.val('').next().html('')
      if ($clear.length) {
        $clear.addClass('hidden')
      }
      $field.addClass('raw-id-field-empty')
      $input.trigger('change');
    }
  }

  $.ajaxPrefilter(function (options) {
    // We want the browser to maintain two separate internal caches: one
    // for json data and one for normal page loads.
    // Without adding this parameter, some browsers will often
    // confuse the two.
    options.url = utils.ensureUrlParam(options.url, '_ajax', '1')
  })

  $(function () {
    $(window).on('beforeunload', function (e) {
      if (dirtyForm()) {
        $('.loading-overlay').addClass('loading-overlay-hidden')
        return 'Certains changements n\'ont pas été sauvegardés.'
      }
    })

    //  Catch Unhandled Javascript Errors
    // http://stackoverflow.com/questions/951791/javascript-global-error-handling
    window.onerror = function (msg, url, line, col, error) {
      // Note that col & error are new to the HTML 5 spec and may not be
      // supported in every browser.  It worked for me in Chrome.
      var extra = !col ? '' : '<br>Colonne: ' + col
      extra += !error ? '' : '<br>Erreur: ' + error

      bootbox.alert({
        title: "Une erreur s'est produite.",
        message: "<b>Message d'erreur:</b><br><br>" + msg + '<br>Adresse: ' + url + '<br>Ligne: ' + line + extra
      })
      return false
    }

    $(document).on('click', '.dropdown-menu input, .dropdown-menu select, .dropdown-menu label', function (e) {
      e.stopPropagation()
    })

    $('.sidebar-toggle').on('click touchend', function (e) {
      var el = $(this)
      var link = el.attr('href')
      window.location = link
    })

    $(document).on('ajaxStart', function () {
      $('.loading-overlay').removeClass('loading-overlay-hidden')
      $(document.body).css('cursor', 'progress')
      NProgress.start()
    })

    $(document).on('ajaxStop', function () {
      $('.loading-overlay').addClass('loading-overlay-hidden')
      $(document.body).css('cursor', 'auto')
      $('[role="bar"]').hide()
      NProgress.set(1)
    })

    $(document).on('submit', 'form', function(){
      $('.loading-overlay').removeClass('loading-overlay-hidden')
    });

    $(document).on('ajaxError', function (event, xhr, settings, thrownError) {
      if (thrownError === "abort") {
        return
      }
      var errorMessages = {
        0: 'Le serveur ne répond pas.',
        parseerror: 'Données invalides (parse error)',
        timeout: 'Le requête à expirée',
        500: 'Erreur coté serveur',
        404: 'Page introuvable',
        403: 'Permission refusé'
      }

      bootbox.alert({
        title: errorMessages[xhr.status] || 'Erreur inconnue',
        message: '<pre style="overflow-x:auto word-wrap:normal">' + (xhr.responseText || errorMessages[xhr.status] || 'Erreur inconnue') + '</pre>',
        size: 'large'
      }).scrollTop(0)
    })

    // fix long stacked modals scrolling
    function setModalsAndBackdropsOrder () {
      var modalZIndex = 1040
      $('body > .modal.in, .main-panel > .modal.in').each(function (index) {
        var $modal = $(this)
        modalZIndex++
        $modal.css('zIndex', modalZIndex)
        $modal.next('.modal-backdrop.in').css('zIndex', modalZIndex - 1)
      })

      $('.modal.in:visible:last').focus().next('.modal-backdrop.in')
    }

    $(document).on('hidden.bs.modal', function (e) {
      if ($('.modal.in').length) {
        $('html').addClass('modal-open')
      } else {
        $('html').removeClass('modal-open')
      }
    })

    $(document).on('shown.bs.modal', '.modal', function (event) {
      $('html').addClass('modal-open')
    })

    $(document).on('shown.bs.modal', '.modal.in', function (event) {
      var $modal = $(this)
      if (!$modal.parent().is('body')) {
        $(this).data('bs.modal').$backdrop.insertAfter($(this))
      }
    })

    // stacked modal backdrop fix
    // http://gurde.com/stacked-bootstrap-modals/
    $(document).on('shown.bs.modal', '.modal.in', function (event) {
      setModalsAndBackdropsOrder()
    }).on('hidden.bs.modal', '.modal', function (event) {
      setModalsAndBackdropsOrder()
    })

    $(document).on('shown.bs.tab', function (e) {
      var selector = $(e.target).data('target')
      if (selector) {
        autosize($(selector).find('textarea:not(:hidden)'))
      }
    })

    $(document).on('shown.bs.collapse', function(e){
      autosize($(e.target).find('textarea:not(:hidden)'))
    })

    $(document).on('click', 'a[href]', function (e) {
      var $a = $(this)
      var href = $a.attr('href')
      if ( e.which !== 1 || e.metaKey || e.ctrlKey || e.shiftKey || e.altKey ) {
        href = app.utils.removeUrlParam(href, '_popup')
        href = app.utils.removeUrlParam(href, '_to_field')
        $a.attr('href', href)
      }
    });

    $(document).on('click', 'a[href][bind="xhr"]:not(.django-popup a)', function (e) {
      var $currentTarget = $(e.currentTarget)

      if (!utils.shouldHandleClick(e)) {
        return
      }

      e.preventDefault()

      router.show(utils.normalizeUrl($currentTarget.attr('href')))
      $('html,body').scrollTop(0);
    })

    app.mainView = new app.MainView()

    router.start({
      click: false,
    })
  })
})(jQuery)
