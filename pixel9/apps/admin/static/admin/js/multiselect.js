$.fn.customMultiSelect = function(options){
  var defaults = {
    selectableHeader: '<div class="input-group-inner right-addon"><input type="text" class="form-control ms-search-input" autocomplete="off" placeholder="Rechercher dans choix"><i class="input-group-inner-addon fa fa-search"></i></div>',
    selectionHeader:  '<div class="input-group-inner right-addon"><input type="text" class="form-control ms-search-input" autocomplete="off" placeholder="Rechercher dans sélectionnés"><i class="input-group-inner-addon fa fa-search"></i></div>',
    afterInit: function(ms){
      var that = this,
      $selectableSearch = that.$selectableUl.prev().find('.form-control'),
      $selectionSearch = that.$selectionUl.prev().find('.form-control'),
      selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
      selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

      that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
      .on('keydown', function(e){
        if (e.which === 40 || e.which === 13){
          that.moveHighlight(that.$selectableUl, 1);
          return false;
        }
      });

      that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
      .on('keydown', function(e){
        if (e.which === 40 || e.which === 13){
          that.moveHighlight(that.$selectionUl, 1);
          return false;
        }
      });
    },
    afterSelect: function(){
      this.qs1.cache();
      this.qs2.cache();
    },
    afterDeselect: function(){
      this.qs1.cache();
      this.qs2.cache();
    }
  };
  return $(this).multiSelect($.extend({}, defaults, options));
};

