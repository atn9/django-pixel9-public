(function($){

  $.fn.changelistSearchTypeahead = function(typeaheadConfig){

    return $(this).each(function(){

      /* typeahead */
      var $searchInput = $(this);
      var $page = $searchInput.parents('.changelist-wrapper');
      var searchForm = $page.find('.search-form');

      var charMap = {'àâ': 'a', 'èéêë': 'e', 'ç': 'c', 'îï': 'i', 'ù': 'u', 'ô': 'o'};
      function replaceAccents(str){
        $.each(charMap, function(pattern, replace) {
          str = str.replace(new RegExp("[" + pattern + "]", "gi"), replace);
        });
        return str;
      }

      function suggestionTemplate(data) {
        var str = '';
        if (data.model) {
          str += '<div class="tt-model-'+data.model+'">';
        } else {
          str += '<div>';
        }
        if (data.image) {
          str += '<div class="tt-image">';
          str += '<img src="'+data.image+'">';
          str += '</div>';
        }
        str += '<div class="tt-body">';
        str += '<div class="tt-title">'+data.name+'</div>';
        if (data.extra) {
          str += '<div class="tt-extra">'+data.extra+'</div>';
        }
        str += '</div>';
        if (data.type) {
          str += '<div class="tt-type">'+data.type+'</div>';
        }
        str += '</div>';
        return str;
      }

      var datasets = [];

      typeaheadConfig.datasets.forEach(function(d){
        var config = {
          identify: function(obj) {
            // do not use an integer, bugs with typeahead.js
            return obj.id.toString();
          },
          sorter: function(a, b) {
            return b.weight - a.weight;
          },
          matchAnyQueryToken: true,
          queryTokenizer: function(str) {
            return Bloodhound.tokenizers.nonword(replaceAccents(str));
          },
          datumTokenizer: function(datum) {
            var name;
            if (datum.name) {
              name = replaceAccents(datum.name);
            } else {
              name = '';
            }
            return Bloodhound.tokenizers.nonword(name);
          }
        };

        if (d.local) {
          config.local = d.local;
        }
        if (d.prefetch) {
          config.prefetch = d.prefetch;
        }
        if (d.remote) {
          config.remote = d.remote;
        }

        datasets.push({
          display: d.name || 'name',
          limit: d.limit || 10,
          source: new Bloodhound(config),
          templates: {
            suggestion: suggestionTemplate
          }
        });
      });

      /* jshint ignore:start */
      $searchInput.typeahead({
        highlight: false,
        autoselect: false,
        minLength: 2,
        hint: true,
      }, ...datasets);
      /* jshint ignore:end */

      $searchInput.on('focus', function(){
        $(this).prev().addClass('focus');
      });
      $searchInput.on('blur', function(){
        $(this).prev().removeClass('focus');
      });
      $searchInput.on('mouseover', function(){
        $(this).prev().addClass('hover');
      });
      $searchInput.on('mouseout', function(){
        $(this).prev().removeClass('hover');
      });
      $searchInput.on('typeahead:close', function(){
        $(this).prev().removeClass('open');
      });

      function toggleOpen() {
        if ($(this).parent().find('.tt-open .tt-suggestion').length) {
          $(this).prev().addClass('open');
        } else {
          $(this).prev().removeClass('open');
        }
      }

      $searchInput.on('keyup', toggleOpen);
      $searchInput.on('typeahead:open', toggleOpen);

      $searchInput.on('typeahead:selected', function(event, datum){
        var $input, url, $a;
        if (datum.itemId && datum.inputName) {
          $input = searchForm.find('[name="'+ datum.inputName +'"]');
          if (! $input.length) {
            $input = $('<input type="hidden" name="'+ datum.inputName + '">');
          }
          $input.val(datum.itemId);
          searchForm.append($input);
          $searchInput.val('').typeahead('val', '').change();
          searchForm.submit();
        } else if (datum.url) {
          url = datum.url;
          if (datum.addPreservedFilters) {
            if (location.search) {
              url =  url + '?_changelist_filters=' + encodeURIComponent(location.search.replace('?', ''));
            }
          }
          $a = $('<a href="'+ url +'"></a>');
          $a.appendTo(searchForm);
          $a[0].click();
        } else {
          $searchInput.val(datum.name).typeahead('val', datum.name).change();
          searchForm.submit();
        }
      });

      function fixTypeaheadShouldSubmitOnEnterKeyWhenMouseOverMenu(){
        var typeahead = $searchInput.data('ttTypeahead');
        var shouldTriggerSearch = true;

        $searchInput.on('typeahead:cursorchange', function(e, suggestion){
          if (suggestion) {
            shouldTriggerSearch = false;
          } else {
            shouldTriggerSearch = true;
          }
        });

        typeahead.menu.$node.on('click', typeahead.selectors.selectable, function(e){
          shouldTriggerSearch = false;
          typeahead.select($(e.currentTarget));
        });

        $searchInput.on('typeahead:beforeselect', function(e){
          if (shouldTriggerSearch) {
            e.preventDefault();
          }
        });
      }
      fixTypeaheadShouldSubmitOnEnterKeyWhenMouseOverMenu();

    });
  };

})(jQuery)
