// modifed version of:
// https://github.com/farhan0581/django-admin-autocomplete-filter/blob/pre_release/admin_auto_filters/static/django-admin-autocomplete-filter/js/autocomplete_filter_qs.js
autocompleteFilter = {};

autocompleteFilter.searchReplace = function(name, value, url) {
  var new_search_hash = autocompleteFilter.searchToHash(url);
  if (value) {
    new_search_hash[decodeURIComponent(name)] = [];
    new_search_hash[decodeURIComponent(name)].push(decodeURIComponent(value));
  } else {
    delete new_search_hash[decodeURIComponent(name)];
  }
  return autocompleteFilter.hashToSearch(new_search_hash);
}

autocompleteFilter.searchAdd = function(name, value, url) {
  var new_search_hash = app.autocompleteFilter.searchToHash(url);
  if ( ! (decodeURIComponent(name) in new_search_hash)) {
    new_search_hash[decodeURIComponent(name)] = [];
  }
  new_search_hash[decodeURIComponent(name)].push(decodeURIComponent(value));
  return hash_to_search(new_search_hash);
}

// pduey: remove a variable/value pair from the current query string and return updated href
autocompleteFilter.searchRemove = function(name, value, url) {
  var new_search_hash = app.autocompleteFilter.searchToHash(url);
  if (new_search_hash[name].indexOf(value) >= 0) {
    new_search_hash[name].splice(new_search_hash[name].indexOf(value), 1);
    if (new_search_hash[name].length == 0) {
      delete new_search_hash[name];
    }
  }
  return hash_to_search(new_search_hash);
}

autocompleteFilter.searchToHash = function(url) {
  var h = {};
  var search;
  if (url) {
    search = new URL(url, window.location.origin).search
  } else {
    search = window.location.search
  }
  if (search == undefined || search.length < 1) { return h;}
  q = search.slice(1).split('&');
  for (var i = 0; i < q.length; i++) {
    var key_val = q[i].split('=');
    // replace '+' (alt space) char explicitly since decode does not
    var hkey = decodeURIComponent(key_val[0]).replace(/\+/g,' ');
    var hval = decodeURIComponent(key_val[1]).replace(/\+/g,' ');
    if (h[hkey] == undefined) {
      h[hkey] = [];
    }
    h[hkey].push(hval);
  }
  return h;
}

autocompleteFilter.hashToSearch = function(h) {
  var search = String("?");
  for (var k in h) {
    for (var i = 0; i < h[k].length; i++) {
      search += search == "?" ? "" : "&";
      search += encodeURIComponent(k) + "=" + encodeURIComponent(h[k][i]);
    }
  }
  return search;
}
