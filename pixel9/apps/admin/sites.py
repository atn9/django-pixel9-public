import random
import re
import string
from functools import update_wrapper

from django import http
from django.apps import apps
from django.conf import settings
from django.contrib.admin import AdminSite as DjAdminSite
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ImproperlyConfigured
from django.shortcuts import render
from django.template.response import SimpleTemplateResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.module_loading import import_string
from django.views.decorators.cache import never_cache
from django_ratelimit.decorators import ratelimit

from pixel9.apps.admin.forms.forms import AdminAuthenticationForm

from .options import ModelAdmin
from .settings import admin_settings

AJAX_VAR = '_ajax'
ajax_var_re = re.compile(rf'&?{AJAX_VAR}=[^&]*', flags=re.IGNORECASE)


def is_ajax(request):
    return request.headers.get('x-requested-with') == 'XMLHttpRequest'


class AdminSite(DjAdminSite):
    login_form = AdminAuthenticationForm
    login_template = 'admin/auth/login.html'
    password_change_template = 'admin/auth/password_change_form.html'  # noqa: S105
    password_change_done_template = 'admin/auth/password_change_done.html'  # noqa: S105
    app_shell_template_name = [
        'admin/app_shell.html',
        'admin/app_shell_base.html',
    ]

    def admin_view(self, view, cacheable=False):
        if getattr(view, 'never_require_login', False):
            return view

        def inner(request, *args, **kwargs):
            if request.path.endswith('jsi18n/'):
                return view(request, *args, **kwargs)

            if not self.has_permission(request):
                if is_ajax(request):
                    # if someone is already in the application and have lost his session
                    # we refresh the page to redirect him to the login page
                    return http.JsonResponse(
                        {
                            'content': '<script>window.location.reload(true);</script>',
                        }
                    )
                return self.login(request)

            request.GET._mutable = True
            if AJAX_VAR in request.GET:
                del request.GET[AJAX_VAR]

            request.POST._mutable = True
            if AJAX_VAR in request.POST:
                del request.POST[AJAX_VAR]

            request.META['QUERY_STRING'] = re.sub(ajax_var_re, '', request.META.get('QUERY_STRING', ''))

            if '_page_id' in request.POST:
                request.unique_page_id = request.POST['_page_id']
            else:
                request.unique_page_id = ''.join(
                    random.choice(string.ascii_lowercase + string.digits)  # noqa: S311
                    for i in range(8)
                )

            try:
                response = view(request, *args, **kwargs)
            except http.Http404 as e:
                if not settings.DEBUG:
                    # do not display the response of the default 404 handler
                    response = http.HttpResponseNotFound('Page introuvable')
                else:
                    raise e

            # it's data return a json response
            if isinstance(response, dict) or isinstance(response, list):
                response = http.JsonResponse(response, safe=False)

            status_code = getattr(response, 'status', getattr(response, 'status_code', None))

            if is_ajax(request):
                if status_code in (301, 302):
                    data = {}
                    data['redirect'] = response['Location']
                    response = http.JsonResponse(data, safe=False)

            if (
                response['Content-Type'].startswith('text/html')
                and status_code not in (301, 302)
                and not is_ajax(request)
                and not request.GET.get('no-layout')
                and getattr(response, 'app_shell', None) is not False
            ):
                if isinstance(response, SimpleTemplateResponse):
                    content = response.rendered_content
                else:
                    content = response.content
                    if isinstance(content, bytes):
                        content = response.content.decode('utf-8')
                kwargs['content'] = content

                titles = re.findall(r'<title>(.+?)</title>', content, re.DOTALL)
                if titles:
                    kwargs['title'] = titles[0]
                response = self.app_shell(request, *args, **kwargs)

            return response

        if not cacheable:
            inner = never_cache(inner)
        return update_wrapper(inner, view)

    def get_urls(self):
        from django.urls import path, re_path

        urlpatterns = super().get_urls()

        if apps.is_installed('pixel9.apps.queues'):
            from pixel9.apps.queues import views

            urlpatterns = [
                path('queues/', self.admin_view(views.stats), name='queues_home'),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/$',
                    self.admin_view(views.jobs),
                    name='queues_jobs',
                ),
                re_path(
                    r'^workers/(?P<queue_index>[\d]+)/$',
                    self.admin_view(views.workers),
                    name='queues_workers',
                ),
                re_path(
                    r'^workers/(?P<queue_index>[\d]+)/(?P<key>[-\w\.\:\$]+)/$',
                    self.admin_view(views.worker_details),
                    name='queues_worker_details',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/finished/$',
                    self.admin_view(views.finished_jobs),
                    name='queues_finished_jobs',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/started/$',
                    self.admin_view(views.started_jobs),
                    name='queues_started_jobs',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/deferred/$',
                    self.admin_view(views.deferred_jobs),
                    name='queues_deferred_jobs',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/failed/$',
                    self.admin_view(views.failed_jobs),
                    name='queues_failed_jobs',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/scheduled/$',
                    self.admin_view(views.scheduled_jobs),
                    name='queues_scheduled_jobs',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/rqscheduler/$',
                    self.admin_view(views.rq_scheduler_jobs),
                    name='queues_rq_scheduler_jobs',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/empty/$',
                    self.admin_view(views.clear_queue),
                    name='queues_clear',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/requeue-all/$',
                    self.admin_view(views.requeue_all),
                    name='queues_requeue_all',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/(?P<job_id>[-\w]+)/$',
                    self.admin_view(views.job_detail),
                    name='queues_job_detail',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/(?P<job_id>[-\w]+)/delete/$',
                    self.admin_view(views.delete_job),
                    name='queues_delete_job',
                ),
                re_path(
                    r'^queues/actions/(?P<queue_index>[\d]+)/$',
                    self.admin_view(views.actions),
                    name='queues_actions',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/(?P<job_id>[-\w]+)/requeue/$',
                    self.admin_view(views.requeue_job_view),
                    name='queues_requeue_job',
                ),
                re_path(
                    r'^queues/(?P<queue_index>[\d]+)/(?P<job_id>[-\w\.\:\$]+)/enqueue/$',
                    self.admin_view(views.enqueue_job),
                    name='queues_enqueue_job',
                ),
            ] + urlpatterns

        urlpatterns += [re_path(r'.*', self.admin_view(self.not_found), name='not_found')]
        return urlpatterns

    def each_context(self, request):
        context = super().each_context(request)
        context.update(
            {
                'current_site': get_current_site(request),
                'unique_page_id': getattr(request, 'unique_page_id', ''),
                'environment': getattr(settings, 'ENV', None),
            }
        )
        return context

    @method_decorator(ratelimit(key='ip', rate='10/d', method='POST'))
    @method_decorator(ratelimit(key='post:email', rate='10/d', method='POST'))
    def login(self, request, extra_context=None):
        return super().login(request, extra_context)

    def not_found(self, request, extra_context=None):
        return http.HttpResponseNotFound('Page introuvable')

    def logout(self, request, extra_context=None):
        super().logout(request, extra_context)
        return {
            'content': '<script>window.location={}</script>'.format(reverse('admin:index')),
        }

    def register(self, model_or_iterable, admin_class=None, **options):
        if not admin_class:
            admin_class = ModelAdmin
        return super().register(model_or_iterable, admin_class, **options)

    def app_shell(self, request, *args, **kwargs):
        context = self.each_context(request)
        if 'content' in kwargs:
            context['content'] = kwargs['content']
        if 'title' in kwargs:
            context['title'] = kwargs['title']
        return render(request, self.app_shell_template_name, context)

    def check_dependencies(self):
        """
        Check that all things needed to run the admin have been correctly installed.

        The default implementation checks that admin and contenttypes apps are
        installed, as well as the auth context processor.
        """
        if not apps.is_installed('pixel9.apps.admin'):
            raise ImproperlyConfigured(
                "Put 'pixel9.apps.admin' in " 'your INSTALLED_APPS setting in order to use the admin application.'
            )
        if not apps.is_installed('django.contrib.contenttypes'):
            raise ImproperlyConfigured(
                "Put 'django.contrib.contenttypes' in "
                'your INSTALLED_APPS setting in order to use the admin application.'
            )
        if 'django.contrib.auth.context_processors.auth' not in settings.TEMPLATE_CONTEXT_PROCESSORS:
            raise ImproperlyConfigured(
                "Put 'django.contrib.auth.context_processors.auth' "
                'in your TEMPLATE_CONTEXT_PROCESSORS setting in order to use the admin application.'
            )

    def i18n_javascript(self, request):
        from django.views.i18n import JavaScriptCatalog

        return JavaScriptCatalog.as_view()(request, packages='pixel9.apps.admin')


site = import_string(admin_settings.ADMIN_SITE_CLASS)()
