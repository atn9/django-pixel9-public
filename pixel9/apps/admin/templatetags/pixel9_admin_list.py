import re
from copy import copy
from inspect import getfullargspec

from django.contrib.admin.templatetags.admin_list import (
    ResultList,
    admin_actions,
    admin_list_filter,
    date_hierarchy,
    items_for_result,
    pagination,
    register,
    result_hidden_fields,
    result_list,
    results,
)
from django.contrib.admin.templatetags.admin_list import (
    result_headers as django_result_headers,
)
from django.contrib.admin.views.main import PAGE_VAR, SEARCH_VAR
from django.template.loader import get_template
from django.utils.html import format_html
from django.utils.safestring import mark_safe


@register.simple_tag
def paginator_number(cl, i):
    """
    Generates an individual page index link in a paginated list.
    """
    if i == cl.paginator.ELLIPSIS:
        return format_html('<span>{}</span> ', cl.paginator.ELLIPSIS)
    elif i == cl.page_num:
        return format_html('<span class="this-page">{}</span> ', i)
    else:
        return format_html(
            '<a href="{}"{}>{}</a> ',
            cl.get_query_string({PAGE_VAR: i}),
            mark_safe(' class="end"' if i == cl.paginator.num_pages else ''),
            i,
        )


@register.simple_tag
def paginator_info(cl):
    paginator = cl.paginator
    entries_from = ((paginator.per_page * (cl.page_num - 1)) + 1) if paginator.count > 0 else 0
    entries_to = entries_from - 1 + paginator.per_page
    if paginator.count < entries_to:
        entries_to = paginator.count
    return f'{entries_from} - {entries_to}'


def result_headers(cl):
    for header in django_result_headers(cl):
        class_attrib = header.get('class_attrib')
        if 'column-' in class_attrib:
            match = re.search(r'column-(\w+)', class_attrib)
            if match.groups():
                field_name = match.groups()[0]
                align = cl.model_admin.get_column_align(field_name)
                if align:
                    header['class_attrib'] = format_html('{} column-align-{}"', mark_safe(class_attrib[:-1]), align)
                hidden = cl.model_admin.get_column_hidden(field_name)
                if hidden:
                    header['class_attrib'] = format_html('{} hidden"', mark_safe(class_attrib[:-1]))
        yield header


def _boolean_icon(field_val):
    if field_val is True:
        css_class = 'boolean-true'
    elif field_val is False:
        css_class = 'boolean-false'
    else:
        css_class = 'boolean-none'
    return mark_safe(
        format_html(
            '<span class="boolean {0}"></span>',
            css_class,
        )
    )


@register.inclusion_tag('admin/search_form.html', takes_context=True)
def search_form(context, cl):
    """
    Displays a search form for searching the list.
    """
    return {
        'cl': cl,
        'unique_page_id': context['unique_page_id'],
        'show_result_count': cl.result_count != cl.full_result_count,
        'clear_search_url': cl.params.get(SEARCH_VAR) and cl.get_query_string(remove=[SEARCH_VAR]),
        'search_var': SEARCH_VAR,
    }


@register.inclusion_tag('admin/includes/search_input.html', takes_context=True)
def search_input(context, cl):
    return {
        'cl': cl,
        'unique_page_id': context['unique_page_id'],
        'show_result_count': cl.result_count != cl.full_result_count,
        'clear_search_url': cl.params.get(SEARCH_VAR) and cl.get_query_string(remove=[SEARCH_VAR]),
        'search_var': SEARCH_VAR,
    }


@register.simple_tag(takes_context=True)
def admin_list_filter(context, cl, spec):  # noqa: F811
    tpl = get_template(spec.template)
    return tpl.render(
        {
            'unique_page_id': context['unique_page_id'],
            'title': spec.title,
            'choices': list(spec.choices(cl)),
            'spec': spec,
        }
    )


@register.inclusion_tag('admin/includes/filter_selection.html')
def admin_list_filter_selection(cl, spec):
    selected = {}
    choices = list(spec.choices(cl))
    for choice in choices[1:]:
        if choice['selected']:
            selected = {
                'spec': spec,
                'choice': choice,
                'clear_url': choices[0]['query_string'],
            }
    return selected


# List attributes
# http://django-suit.readthedocs.org/en/develop/list_attributes.html
# https://github.com/darklow/django-suit/blob/develop/suit/templatetags/suit_list.py
def dict_to_attrs(attrs):
    return mark_safe(' ' + ' '.join([f'{k}="{v}"' for k, v in list(attrs.items())]))


class ResultList(list):  # noqa: F811
    # Wrapper class used to return items in a list_editable
    # changelist, annotated with the form object for error
    # reporting purposes. Needed to maintain backwards
    # compatibility with existing admin templates.
    def __init__(self, form, pk, *items):
        self.form = form
        self.pk = pk
        super().__init__(*items)


def results(cl):  # noqa: F811
    if cl.formset:
        for res, form in zip(cl.result_list, cl.formset.forms, strict=False):
            yield ResultList(form, res.pk, items_for_result(cl, res, form))
    else:
        for res in cl.result_list:
            yield ResultList(None, res.pk, items_for_result(cl, res, None))


@register.inclusion_tag('admin/change_list_results.html')
def result_list(cl):  # noqa: F811
    """
    Displays the headers and data list together
    """
    headers = list(result_headers(cl))
    num_sorted_fields = 0
    for h in headers:
        if h['sortable'] and h['sorted']:
            num_sorted_fields += 1
    return {
        'cl': cl,
        'result_hidden_fields': list(result_hidden_fields(cl)),
        'result_headers': headers,
        'num_sorted_fields': num_sorted_fields,
        'results': list(results(cl)),
    }


@register.inclusion_tag('admin/change_list_results.html', takes_context=True)
def result_list_with_context(context, cl):
    """
    Wraps Djangos default result_list to ammend the context with the request.
    This gives us access to the request in change_list_results.
    """
    res = result_list(cl)
    res['request'] = context['request']
    res['manual_ordering'] = context.get('manual_ordering')
    for i, field_name in enumerate(cl.list_display):
        func = getattr(cl.model_admin, f'get_{field_name}_column_info', None)
        if func:
            res['result_headers'][i]['column_info'] = func(context['request'])
    return res


@register.simple_tag(takes_context=True)
def result_row_attrs(context, cl, row_index, form):
    """
    Returns row attributes based on object instance
    """
    row_index -= 1
    attrs = {'class': 'row1' if row_index % 2 == 0 else 'row2'}
    suit_row_attributes = getattr(cl.model_admin, 'row_attributes', None)
    if not suit_row_attributes:
        return dict_to_attrs(attrs)

    instance = cl.result_list[row_index]

    # Backwards compatibility for suit_row_attributes without request argument
    args = getfullargspec(suit_row_attributes)
    if 'request' in args[0]:
        new_attrs = suit_row_attributes(instance, context['request'])
    else:
        new_attrs = suit_row_attributes(instance)

    if not new_attrs:
        return dict_to_attrs(attrs)

    # Validate
    if not isinstance(new_attrs, dict):
        raise TypeError('"row_attrs" must return dict. Got: {}: {}'.format(new_attrs.__class__.__name__, new_attrs))

    # Merge 'class' attribute
    if 'class' in new_attrs:
        attrs['class'] += ' ' + new_attrs.pop('class')

    if form and form.errors:
        attrs['class'] += ' danger'

    attrs.update(new_attrs)
    return dict_to_attrs(attrs)


@register.filter
def cells_handler(results, cl):
    """
    Changes result cell attributes based on object instance and field name
    """
    suit_cell_attributes = getattr(cl.model_admin, 'get_cell_attributes', None)
    if not suit_cell_attributes:
        return results

    class_pattern = 'class="'
    td_pattern = '<td'
    th_pattern = '<th'
    for row, result in enumerate(results):
        instance = cl.result_list[row]
        for col, item in enumerate(result):
            field_name = cl.list_display[col]

            attrs = copy(suit_cell_attributes(instance, field_name))
            attrs = attrs or {}

            align = cl.model_admin.get_column_align(field_name)
            if align:
                if 'class' not in attrs:
                    attrs['class'] = ''
                attrs['class'] += f' column-align-{align}'

            hidden = cl.model_admin.get_column_hidden(field_name)
            if hidden:
                if 'class' not in attrs:
                    attrs['class'] = ''
                attrs['class'] += ' hidden'

            if not attrs:
                continue

            # Validate
            if not isinstance(attrs, dict):
                raise TypeError('"cell_attrs" must return dict. ' 'Got: {}: {}'.format(attrs.__class__.__name__, attrs))

            # Merge 'class' attribute
            if class_pattern in item.split('>')[0] and 'class' in attrs:
                css_class = attrs.pop('class')
                replacement = f'{class_pattern}{css_class} '
                result[col] = mark_safe(item.replace(class_pattern, replacement))

            # Add rest of attributes if any left
            if attrs:
                cell_pattern = td_pattern if item.startswith(td_pattern) else th_pattern

                result[col] = mark_safe(result[col].replace(cell_pattern, td_pattern + dict_to_attrs(attrs)))

    return results
