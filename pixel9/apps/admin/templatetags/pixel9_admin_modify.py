from bootstrap3.forms import render_field as bs3_render_field
from django import template
from django.contrib.admin.helpers import Fieldset
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.fields.related import ForeignKey
from django.template.context import Context
from django.template.loader import render_to_string

register = template.Library()


@register.inclusion_tag('admin/prepopulated_fields_js.html', takes_context=True)
def prepopulated_fields_js(context):
    """
    Creates a list of prepopulated_fields that should render Javascript for
    the prepopulated fields for both the admin form and inlines.
    """
    prepopulated_fields = []
    if context['add'] and 'adminform' in context:
        prepopulated_fields.extend(context['adminform'].prepopulated_fields)
    if 'inline_admin_formsets' in context:
        for inline_admin_formset in context['inline_admin_formsets']:
            for inline_admin_form in inline_admin_formset:
                if inline_admin_form.original is None:
                    prepopulated_fields.extend(inline_admin_form.prepopulated_fields)
    context.update({'prepopulated_fields': prepopulated_fields})
    return context


@register.inclusion_tag('admin/submit_line.html', takes_context=True)
def submit_row(context):
    """
    Displays the row of buttons for delete and save.
    """
    opts = context['opts']
    add = context['add']
    change = context['change']
    is_popup = context.get('is_popup', False)
    save_as = context['save_as']
    save = context.get('save')
    show_save = context.get('show_save', True)
    show_save_and_continue = context.get('show_save_and_continue', True)
    has_add_permission = context['has_add_permission']
    has_change_permission = context['has_change_permission']
    has_view_permission = context['has_view_permission']
    has_editable_inline_admin_formsets = context['has_editable_inline_admin_formsets']
    can_save = (has_change_permission and change) or (has_add_permission and add) or has_editable_inline_admin_formsets
    can_save_and_continue = not is_popup and can_save and has_view_permission and show_save_and_continue
    can_change = has_change_permission or has_editable_inline_admin_formsets
    ctx = Context(context)
    ctx = {
        'opts': opts,
        'can_change': can_change,
        'show_delete_link': (
            not is_popup and context['has_delete_permission'] and change and context.get('show_delete', True)
        ),
        'show_save_as_new': not is_popup and has_change_permission and change and save_as,
        'show_save_and_add_another': (has_add_permission and not is_popup and (not save_as or add) and can_save),
        'show_save_and_continue': can_save_and_continue,
        'show_save': save and show_save and can_save,
        'show_close': not (show_save and can_save),
        'show_add': add,
        'is_popup': is_popup,
        'preserved_filters': context.get('preserved_filters'),
        'is_bottom': context.get('is_bottom'),
        'is_top': context.get('is_top'),
        'return_url': context.get('return_url'),
        'adminform': context.get('adminform'),
        'related': context.get('related'),
    }
    if context.get('original') is not None:
        ctx['original'] = context['original']
    return ctx


@register.filter
def cell_count(inline_admin_form):
    """Returns the number of cells used in a tabular inline"""
    count = 1  # Hidden cell with hidden 'id' field
    for fieldset in inline_admin_form:
        # Loop through all the fields (one per cell)
        for line in fieldset:
            for _field in line:
                count += 1
    if inline_admin_form.formset.can_delete:
        # Delete checkbox
        count += 1
    return count


def get_bs3_field_opts(field, admin_field_opts, **kwargs):
    field_opts = getattr(field.field, 'bootstrap3', {})

    widget_opts = {}
    if hasattr(field.field.widget, 'widget'):
        # RelatedFieldWidgetWrapper
        widget_opts = getattr(field.field.widget.widget, 'bootstrap3', {})
    else:
        widget_opts = getattr(field.field.widget, 'bootstrap3', {})

    bs3_opts = widget_opts
    bs3_opts.update(field_opts)
    bs3_opts.update(admin_field_opts.get('bootstrap3', {}))
    bs3_opts.update(kwargs)
    if 'layout' not in bs3_opts:
        bs3_opts['layout'] = 'admin'

    return bs3_opts


@register.simple_tag
def field(field, **kwargs):
    model_admin = kwargs.pop('model_admin', None)
    if model_admin:
        admin_field_opts = getattr(model_admin, 'formfield_options', {}).get(field.name, {})
    else:
        admin_field_opts = {}

    bs3_opts = get_bs3_field_opts(field, admin_field_opts, **kwargs)

    valid_formfield_attrs = ['help_text', 'label']
    valid_widget_attrs = ['placeholder', 'cols', 'rows']

    for override in admin_field_opts:
        if override in valid_formfield_attrs:
            setattr(field, override, admin_field_opts[override])
        elif override in valid_widget_attrs:
            field.field.widget.attrs[override] = admin_field_opts[override]
    return bs3_render_field(field, **bs3_opts)


@register.simple_tag(takes_context=True)
def readonly_field_contents(context, readonly_field):
    from django.contrib.admin.utils import lookup_field

    field, obj, model_admin = (
        readonly_field.field['field'],
        readonly_field.form.instance,
        readonly_field.model_admin,
    )
    contents = readonly_field.contents()

    try:
        f, attr, value = lookup_field(field, obj, model_admin)
    except (AttributeError, ValueError, ObjectDoesNotExist):
        pass
    else:
        if value and isinstance(f, ForeignKey):
            admin_html_repr = getattr(value, 'admin_html_repr', None)
            if admin_html_repr:
                contents = admin_html_repr()
    return contents


@register.simple_tag(takes_context=True)
def fieldset(context, adminform, index, template=None):
    """
    Render a fieldset without having to iterate over the AdminForm

    <div class="col">
      {% fieldset adminform 2 %}
    </div>
    <div class="col2">
      {% fieldset adminform 0 %}
    </div>
    """
    template = template or 'admin/includes/fieldset.html'
    name, options = adminform.fieldsets[index]

    return render_to_string(
        template,
        {
            'request': context['request'],
            'unique_page_id': context['unique_page_id'],
            'fieldset': Fieldset(
                adminform.form,
                name,
                readonly_fields=adminform.readonly_fields,
                model_admin=adminform.model_admin,
                **options,
            ),
        },
    )
