import json
from functools import update_wrapper
from urllib.parse import urlparse

try:
    from urllib.parse import parse_qsl
except ImportError:
    from urllib.parse import parse_qsl
from django.conf import settings
from django.contrib import admin, messages
from django.contrib.admin import options
from django.contrib.admin.options import InlineModelAdmin
from django.contrib.admin.utils import label_for_field, quote, unquote
from django.contrib.admin.views import main
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.signals import post_save
from django.http import Http404, HttpResponseRedirect, JsonResponse, QueryDict
from django.http.response import HttpResponseRedirectBase
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.urls import path, re_path, resolve, reverse
from django.utils import translation
from django.utils.decorators import method_decorator
from django.utils.encoding import force_str
from django.utils.functional import Promise
from django.utils.html import format_html
from django.utils.http import url_has_allowed_host_and_scheme, urlencode
from django.utils.text import Truncator
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_protect
from import_export.admin import ExportMixin, ImportMixin
from import_export.signals import post_import
from positions.fields import PositionField

from pixel9 import forms
from pixel9.db import models
from pixel9.utils.http import (
    remove_query_parameters,
    set_query_parameter,
    set_query_parameters,
)

from .actions import ActionHelpersMixin
from .forms import widgets
from .forms.fields import AdminSplitDateTimeField
from .forms.forms import TreeFormAdmin
from .templatetags.pixel9_admin_urls import add_preserved_filters


def get_ul_class(radio_style):
    return 'radio{}'.format(' radio-inline' if radio_style == admin.options.HORIZONTAL else '')


FILTER_PRESET_VAR = 'fp'
NEXT_VAR = 'next'
MANUAL_ORDERING_VAR = 'mo'
DISPLAY_VAR = 'd'
DATE_HIERARCHY_VAR = 'dh'
CONTENT_LANGUAGE_VAR = 'cl'

main.IGNORED_PARAMS = main.IGNORED_PARAMS + (
    CONTENT_LANGUAGE_VAR,
    FILTER_PRESET_VAR,
    NEXT_VAR,
    MANUAL_ORDERING_VAR,
    DATE_HIERARCHY_VAR,
    DISPLAY_VAR,
)

main.EMPTY_CHANGELIST_VALUE = '—'

csrf_protect_m = method_decorator(csrf_protect)


class ManualOrderingInlineFormSetMixin:
    def _construct_form(self, i, **kwargs):
        # we need to set the pk field to required False otherwise
        # django will try to validate the pk field if there was a ValidationError
        # and the form index in less than formset.initial_form_count()
        form = super()._construct_form(i, **kwargs)
        form.fields[self.model._meta.pk.name].required = False
        return form


class ManualOrderingMixin:
    # TODO add check: manual_ordering_field exists
    # TODO add check manual_ordering_field is PositionField ?
    manual_ordering_field = None
    list_max_show_all = 500
    list_display_hidden = None

    def get_urls(self):
        urlpatterns = super().get_urls()
        info = self.model._meta.app_label, self.model._meta.model_name

        if self.support_manual_ordering:
            urlpatterns.insert(
                0,
                path(
                    'position/',
                    self.admin_site.admin_view(self.position_view),
                    name='{}_{}_position'.format(*info),
                ),
            )
        return urlpatterns

    def get_ordering(self, request):
        if self.support_manual_ordering and request.GET.get(MANUAL_ORDERING_VAR):
            return (self.manual_ordering_field,)
        else:
            return super().get_ordering(request)

    def get_actions(self, request):
        if self.support_manual_ordering and request.GET.get(MANUAL_ORDERING_VAR):
            return []
        else:
            return super().get_actions(request)

    @property
    def support_manual_ordering(self):
        return bool(self.manual_ordering_field)

    def position_view(self, request):
        pk = request.POST.get('pk')
        position = request.POST.get('position')

        if request.method != 'POST' or not pk or not position:
            raise Http404

        obj = self.get_object(request, unquote(pk))
        self.update_position(request, obj, int(position))
        return JsonResponse({})

    def update_position(self, request, object, position):
        "requires `manual_ordering_field` to be an instance of `PositionField`"
        setattr(object, self.manual_ordering_field, position)
        object.save()

    def get_manual_ordering_url(self, request):
        if not self.support_manual_ordering:
            return None
        return set_query_parameters(
            request.path,
            {
                MANUAL_ORDERING_VAR: '1',
                main.ALL_VAR: '1',
            },
        )

    def get_manual_ordering_return_url(self, request):
        if not self.support_manual_ordering:
            return None
        return remove_query_parameters(
            request.path,
            [
                MANUAL_ORDERING_VAR,
                main.ALL_VAR,
            ],
        )

    def save_model(self, request, obj, form, change):
        """
        When we modify a field in the change list the ordering field is set to None
        PositionField set the value as -1 instead of the current value
        """

        if self.support_manual_ordering and form.cleaned_data.get(self.manual_ordering_field) is None:
            setattr(
                obj,
                self.manual_ordering_field,
                getattr(obj, self.manual_ordering_field),
            )
        super().save_model(request, obj, form, change)

    @csrf_protect_m
    def changelist_view(self, request, extra_context=None):
        if self.support_manual_ordering:
            context = {
                'manual_ordering': request.GET.get(MANUAL_ORDERING_VAR),
                'manual_ordering_url': self.get_manual_ordering_url(request),
                'manual_ordering_return_url': self.get_manual_ordering_return_url(request),
            }
        else:
            context = {}
        context.update(extra_context or {})
        return super().changelist_view(request, context)

    def get_formset(self, request, obj=None, **kwargs):
        form_class = self.form
        model = self.model
        manual_ordering_field = self.manual_ordering_field

        if (
            self.support_manual_ordering
            and isinstance(self, InlineModelAdmin)
            and self.has_change_permission(request, obj=obj)
        ):

            class ManualOrderingModelForm(form_class):
                def __init__(self, *args, **kwargs):
                    super().__init__(*args, **kwargs)
                    self.fields[manual_ordering_field].widget = forms.HiddenInput()

                def save(self, commit=True):
                    position_field = model._meta.get_field(manual_ordering_field)
                    if isinstance(position_field, PositionField):
                        post_save.disconnect(position_field.update_on_save, sender=model)

                    instance = super().save(commit=commit)

                    if isinstance(position_field, PositionField):
                        post_save.connect(position_field.update_on_save, sender=model)

                    return instance

            class ManualOrderingInlineFormSet(ManualOrderingInlineFormSetMixin, self.formset):
                pass

            kwargs['form'] = ManualOrderingModelForm
            kwargs['formset'] = ManualOrderingInlineFormSet

        formset = super().get_formset(request, obj, **kwargs)

        if self.support_manual_ordering:
            formset.manual_ordering = self.manual_ordering_field
            formset.manual_ordering_field = self.manual_ordering_field

        self.form = form_class
        return formset

    def get_fields(self, request, obj=None):
        fields = super().get_fields(request, obj)
        if (
            self.support_manual_ordering
            and isinstance(self, InlineModelAdmin)
            and self.has_change_permission(request, obj=obj)
        ):
            if self.manual_ordering_field not in fields:
                fields = list(fields)
                fields.append(self.manual_ordering_field)
            return fields
        return fields


class ModelAdminMixin(ManualOrderingMixin, ActionHelpersMixin):
    save_on_top = True

    # not in django
    save = True
    save_on_bottom = True
    save_as_new = True
    save_and_add_another = False
    show_add = True
    show_prev_next = True
    list_aggregates = None
    can_access_changelist = True
    related_wrapper_excludes = []
    search_typeahead = None
    is_related_model_admin = False
    show_changelist_content_language_toggle = False

    _default_formfield_overrides = {
        models.DateTimeField: {
            'form_class': AdminSplitDateTimeField,
            'widget': widgets.AdminSplitDateTimeWidget,
        },
        models.FileField: {'widget': widgets.AdminFileWidget},
        models.ImageField: {'widget': widgets.AdminImageWidget},
        models.DateField: {'widget': widgets.AdminDateWidget},
        models.TimeField: {'widget': widgets.AdminTimeWidget},
        models.TextField: {'widget': widgets.AdminTextareaWidget},
        models.ManyToManyField: {'widget': widgets.AdminSelectMultiple},
    }

    def __init__(self, model, admin_site):
        overrides = self._default_formfield_overrides.copy()
        overrides.update(self.formfield_overrides)
        self.formfield_overrides = overrides

        super().__init__(model, admin_site)

        self.register_related_admins()

    def register_related_admins(self):
        """
        to register related admin  and keep compatibility with django.contrib.admin
        class CustomAdmin(admin.Modeladmin):
            related_admins = {
                'related': (RelatedModel, RelatedModelAdmin),
            }
        """
        if hasattr(self, 'related_admins'):
            if not hasattr(self, '_related_registry'):
                self._related_registry = []
            site = self.admin_site
            for name, model_and_admin in list(self.related_admins.items()):
                model, admin = model_and_admin

                if model not in site._registry:
                    site.register(model, admin)
                    self._related_registry.append({name: model_and_admin})

    def get_urls(self):
        from django.urls import path

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        urls = super().get_urls()
        opts = self.model._meta
        urls.insert(
            0,
            path(
                'raw_id_label/',
                wrap(self.raw_id_label_view),
                name=f'{opts.app_label}_{opts.model_name}_raw_id_label',
            ),
        )
        return self.get_custom_views_urls() + urls

    def get_custom_view_default_regex(self, name):
        return rf'^(?:(?P<object_id>[^/]+)/)?{name}/$'

    def get_custom_view_options(self, link):
        if isinstance(link, str):
            name = link
            custom_options = {}
            regex = None
        else:
            name = link['name']
            custom_options = link
            regex = link['url']

        options = {
            'name': name,
            'method_name': f'{name}_view',
            'regex': regex or self.get_custom_view_default_regex(name),
            # accepts one optional positional argument by default
            # useful for object_id
            'perms': [],
        }
        options.update(custom_options)
        return options

    def get_custom_views_urls(self):
        from django.contrib.auth.decorators import permission_required

        """
        https://github.com/frankwiles/django-admin-views

        custom_views = (
            'detail',
            # or
            {
                'name': 'detail2',
                'method_name': 'detail2_view',
                'url': r'(.+)/detail2/$',
                'perms': ('apps.model_permission',),
            },
        )
        """

        urls = []
        opts = self.model._meta
        for link in getattr(self, 'custom_views', []):
            options = self.get_custom_view_options(link)
            view_func = getattr(self, options['method_name'])

            for perm in options['perms']:
                view_func = permission_required(perm, raise_exception=True)(view_func)

            urls.append(
                re_path(
                    options['regex'],
                    self.admin_site.admin_view(view_func),
                    name='{}_{}_{}'.format(opts.app_label, opts.model_name, options['name']),
                )
            )
        return urls

    def get_changelist(self, request, **kwargs):
        """
        Return the ChangeList class for use on the changelist page.
        """
        from pixel9.apps.admin.views.main import ChangeList

        return ChangeList

    def get_list_display(self, request):
        d = request.GET.get(DISPLAY_VAR)
        if d is not None:
            list_display = []
            for field_name in d.split('.'):
                if field_name in self.list_display or field_name in self.get_list_display_hidden(request):
                    list_display.append(field_name)
            return list_display
        else:
            return self.list_display

    def get_list_display_hidden(self, request):
        return self.list_display_hidden

    def show_list_display_choices(self, request):
        return bool(self.get_list_display_hidden(request))

    def get_list_display_choices(self, request):
        d = request.GET.get(DISPLAY_VAR)
        choices = list(self.list_display)
        list_display_hidden = self.get_list_display_hidden(request)
        if list_display_hidden:
            choices += list(list_display_hidden)
        if d is not None:
            _choices = []
            for field_name in d.split('.'):
                _choices.append(field_name)

            for choice in _choices:
                for i, c in enumerate(choices):
                    if c == choice:
                        del choices[i]

            choices = _choices + choices
        return choices

    def _get_list_display_choices(self, request):
        choices = []
        for field_name in self.get_list_display_choices(request):
            choices.append(
                {
                    'field_name': field_name,
                    'text': label_for_field(field_name, self.model, model_admin=self),
                    'active': field_name in self.get_list_display(request),
                }
            )
        return choices

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Get a form Field for a ForeignKey.
        """
        db = kwargs.get('using')

        if db_field.name in self.get_autocomplete_fields(request):
            kwargs['widget'] = widgets.CustomAutocompleteSelect(db_field, self.admin_site, using=db)
        elif db_field.name in self.raw_id_fields:
            kwargs['widget'] = widgets.CustomForeignKeyRawIdWidget(db_field.remote_field, self.admin_site, using=db)
        elif db_field.name in self.radio_fields:
            kwargs['widget'] = widgets.AdminRadioSelect(
                attrs={
                    'class': get_ul_class(self.radio_fields[db_field.name]),
                }
            )
            kwargs['empty_label'] = _('None') if db_field.blank else None

        if 'queryset' not in kwargs:
            queryset = self.get_field_queryset(db, db_field, request)
            if queryset is not None:
                kwargs['queryset'] = queryset

        return db_field.formfield(**kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """
        Get a form Field for a ManyToManyField.
        """
        # If it uses an intermediary model that isn't auto created, don't show
        # a field in admin.
        if not db_field.remote_field.through._meta.auto_created:
            return None
        db = kwargs.get('using')

        autocomplete_fields = self.get_autocomplete_fields(request)
        if db_field.name in autocomplete_fields:
            kwargs['widget'] = widgets.CustomAutocompleteSelectMultiple(db_field, self.admin_site, using=db)
        elif db_field.name in self.raw_id_fields:
            kwargs['widget'] = widgets.ManyToManyRawIdWidget(db_field.remote_field, self.admin_site, using=db)
        elif db_field.name in list(self.filter_vertical) + list(self.filter_horizontal):
            kwargs['widget'] = admin.widgets.FilteredSelectMultiple(
                db_field.verbose_name, db_field.name in self.filter_vertical
            )

        if 'queryset' not in kwargs:
            queryset = self.get_field_queryset(db, db_field, request)
            if queryset is not None:
                kwargs['queryset'] = queryset

        form_field = db_field.formfield(**kwargs)
        return form_field

    def formfield_for_dbfield(self, db_field, request, **kwargs):
        formfield = super().formfield_for_dbfield(db_field, request, **kwargs)
        if isinstance(db_field, models.ManyToManyField) or isinstance(db_field, models.ForeignKey):
            if formfield and db_field.name in self.raw_id_fields:
                related_modeladmin = self.admin_site._registry.get(db_field.remote_field.model)
                wrapper_kwargs = {}
                if related_modeladmin:
                    wrapper_kwargs.update(
                        can_add_related=related_modeladmin.has_add_permission(request),
                        can_change_related=related_modeladmin.has_change_permission(request),
                        can_delete_related=related_modeladmin.has_delete_permission(request),
                    )
                formfield.widget = widgets.RelatedFieldWidgetWrapper(
                    formfield.widget,
                    db_field.remote_field,
                    self.admin_site,
                    **wrapper_kwargs,
                )

        if db_field.name in self.related_wrapper_excludes:
            if hasattr(formfield.widget, 'widget'):
                formfield.widget = formfield.widget.widget

        return formfield

    def raw_id_label_view(self, request, object=None):
        object_id = request.GET.get('object_id')
        obj = self.get_object(request, unquote(object_id))
        if hasattr(obj, 'raw_id_field_html_repr'):
            repr = obj.raw_id_field_html_repr()
        else:
            repr = format_html('<span class="raw-id-label">{}</span>', Truncator(obj).words(14))
        return {'label': repr}

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj=obj, change=change, **kwargs)

        def add_prefix(self, field_name):
            """
            Ensure every formfield has a unique id because related dialogs are
            loaded within the same page, not in a popup.
            """
            if self.prefix:
                prefix = f'{request.unique_page_id}-{self.prefix}'
            else:
                prefix = request.unique_page_id

            field_name = f'{prefix}-{field_name}'
            return field_name

        form.add_prefix = add_prefix
        return form

    def get_formsets_with_inlines(self, request, obj=None):
        formsets_with_inlines = []
        for FormSet, inline in super().get_formsets_with_inlines(request, obj):

            @classmethod
            def get_default_prefix(cls):
                return f'{request.unique_page_id}-{super(cls, cls).get_default_prefix()}'

            FormSet.get_default_prefix = get_default_prefix
            formsets_with_inlines.append((FormSet, inline))
        return formsets_with_inlines

    def cell_attributes(self, obj, column):
        pass

    def get_cell_attributes(self, obj, column):
        return self.cell_attributes(obj, column) or {}

    def get_column_align(self, column):
        return None

    def get_column_hidden(self, column):
        return None

    def get_filter_presets(self, request):
        presets = []
        filter_presets = getattr(self, 'filter_presets', [])
        current_params = {}
        current_preset_var = request.GET.get(FILTER_PRESET_VAR, 'default')
        for filter_preset in filter_presets:
            if filter_preset[0] == current_preset_var:
                current_params = dict(**filter_preset[2])

        for preset in filter_presets:
            params = dict(**preset[2])

            url = request.get_full_path()

            url = remove_query_parameters(url, [main.PAGE_VAR, main.ALL_VAR])

            if preset[0] == 'default':
                url = remove_query_parameters(url, [FILTER_PRESET_VAR])
            else:
                url = set_query_parameter(url, FILTER_PRESET_VAR, preset[0])

            for k, _v in current_params.items():
                url = remove_query_parameters(url, [k])

            for k, v in params.items():
                if v == '__all__':
                    url = remove_query_parameters(url, [k])
                else:
                    url = set_query_parameter(url, k, v)

            presets.append(
                {
                    'name': preset[0],
                    'label': preset[1],
                    'active': preset[0] == current_preset_var,
                    'url': url,
                }
            )
        return presets

    def get_changelist_instance(self, request):
        if hasattr(request, '_changelist_instance'):
            return request._changelist_instance
        cl = super().get_changelist_instance(request)
        request._changelist_instance = cl
        return cl

    def get_list_aggregates(self, request):
        declared_aggregates = getattr(self, 'list_aggregates', None)
        if not declared_aggregates:
            return None

        aggregates = getattr(request, '_aggregates_cache', None)

        if aggregates is None:
            cl = self.get_changelist_instance(request)
            queryset = cl.get_queryset(request)

            if isinstance(declared_aggregates, list):
                aggregates = queryset.aggregate(*declared_aggregates)

            if isinstance(declared_aggregates, dict):
                aggregates = queryset.aggregate(**declared_aggregates)

            request._aggregates_cache = aggregates

        return aggregates

    def get_preserved_changelist(self, request):
        """Get a ChangeList with preserved_filters from another view"""
        real_get = request.GET
        fake_get = QueryDict(mutable=True)
        preserved_filters = request.GET.get('_changelist_filters')

        if preserved_filters:
            fake_get.update(dict(parse_qsl(preserved_filters, keep_blank_values=True)))

        request.GET = fake_get
        cl = self.get_changelist_instance(request)
        request.GET = real_get
        return cl

    def get_prev_next_pks(self, request, obj, queryset=None):
        prev = None
        next = None
        cl = self.get_preserved_changelist(request)

        if queryset is None:
            queryset = cl.queryset

        field_names = ['pk']
        # we might need the annotated fields for ordering
        annotations = queryset.query.annotations
        if annotations:
            field_names.extend(list(annotations.keys()))

        pk_list = [o[0] for o in queryset.values_list(*field_names)]

        try:
            index = pk_list.index(obj.pk)
        except ValueError:
            pass
        else:
            if index > 0:
                try:
                    prev = pk_list[index - 1]
                except IndexError:
                    pass
            if index + 1 < len(pk_list):
                try:
                    next = pk_list[index + 1]
                except IndexError:
                    pass
        return prev, next

    def get_search_typeahead(self, request):
        # to support reverse_lazy:
        class LazyEncoder(DjangoJSONEncoder):
            def default(self, obj):
                if isinstance(obj, Promise):
                    return force_str(obj)
                return super().default(obj)

        if self.search_typeahead:
            return json.dumps(self.search_typeahead, cls=LazyEncoder)

    def get_clear_all_filters_url(self, request, changelist):
        # TODO Django 3.1 has builtin supports for this
        return remove_query_parameters(request.get_full_path(), self.get_filters_expected_parameters(changelist))

    def has_active_filters(self, request, changelist):
        # TODO Django 3.1 has builtin supports for this
        for spec in changelist.filter_specs:
            for choice in list(spec.choices(changelist))[1:]:
                if choice['selected']:
                    return True
        return False

    def get_filters_expected_parameters(self, changelist):
        expected_parameters = []
        for spec in changelist.filter_specs:
            if spec.expected_parameters():
                expected_parameters.extend(spec.expected_parameters())
        return expected_parameters

    def changelist_view(self, request, extra_context=None):
        if not self.can_access_changelist:
            raise Http404

        filter_presets = self.get_filter_presets(request)
        if request.method == 'GET' and filter_presets and FILTER_PRESET_VAR not in request.GET:
            for filter_preset in filter_presets:
                if filter_preset['name'] == 'default':
                    request_url = remove_query_parameters(request.get_full_path(), [main.PAGE_VAR, main.ALL_VAR])
                    filter_url = remove_query_parameters(filter_preset['url'], [main.PAGE_VAR, main.ALL_VAR])
                    if request_url != filter_url:
                        page_var = request.GET.get(main.PAGE_VAR)
                        if page_var:
                            filter_preset['url'] = set_query_parameter(filter_preset['url'], main.PAGE_VAR, page_var)
                        all_var = request.GET.get(main.ALL_VAR)
                        if all_var:
                            filter_preset['url'] = set_query_parameter(filter_preset['url'], main.ALL_VAR, all_var)
                        return redirect(filter_preset['url'])

        cl = self.get_changelist_instance(request)

        content_language = request.GET.get(CONTENT_LANGUAGE_VAR, translation.get_language())
        content_language_choices = []
        for language in settings.LANGUAGES:
            choice = {
                'name': language[1],
                'label': language[0],
                'link': set_query_parameter(request.get_full_path(), CONTENT_LANGUAGE_VAR, language[0]),
                'active': language[0] == content_language,
            }
            content_language_choices.append(choice)

        context = {
            'add_url': add_preserved_filters(
                {
                    'preserved_filters': self.get_preserved_filters(request),
                    'opts': self.model._meta,
                },
                self._get_add_url(request),
            ),
            'show_add': getattr(self, 'show_add', None),
            'search_typeahead': self.get_search_typeahead(request),
            'filter_presets': filter_presets,
            'filters_expected_parameters': self.get_filters_expected_parameters(cl),
            'clear_all_filters_url': self.get_clear_all_filters_url(request, cl),
            'has_active_filters': self.has_active_filters(request, cl),
            'list_display_choices': self._get_list_display_choices(request),
            'show_list_display_choices': self.show_list_display_choices(request),
            'list_display_choices_base_url': remove_query_parameters(
                request.get_full_path(), [main.ORDER_VAR, DISPLAY_VAR]
            ),
            'content_language': request.GET.get(CONTENT_LANGUAGE_VAR, translation.get_language()),
            'content_language_choices': content_language_choices,
            'show_changelist_content_language_toggle': self.show_changelist_content_language_toggle,
        }

        show_date_hierarchy = False
        if request.GET.get(DATE_HIERARCHY_VAR):
            show_date_hierarchy = True
        context['show_date_hierarchy'] = show_date_hierarchy
        if show_date_hierarchy:
            year_field = '%s__year' % self.date_hierarchy
            month_field = '%s__month' % self.date_hierarchy
            day_field = '%s__day' % self.date_hierarchy
            context['date_hierarchy_url'] = cl.get_query_string(
                None, [DATE_HIERARCHY_VAR, year_field, month_field, day_field]
            )
        else:
            context['date_hierarchy_url'] = cl.get_query_string({DATE_HIERARCHY_VAR: '1'})

        context.update(extra_context or {})

        return super().changelist_view(request, context)

    def changeform_view(self, request, object_id, form_url='', extra_context=None):
        _popup = options.IS_POPUP_VAR
        options.IS_POPUP_VAR = '__INVALID'
        actions = self.get_actions(request)
        options.IS_POPUP_VAR = _popup

        if actions and request.method == 'POST' and 'action' in request.POST:
            query_string = request.GET.get('_changelist_filters')
            request.GET = QueryDict(query_string, mutable=True)
            request.META['QUERY_STRING'] = query_string
            return self.changelist_view(request)

        response = super().changeform_view(request, object_id, form_url, extra_context=extra_context)
        add = object_id is None

        if isinstance(response, TemplateResponse):
            if not add:
                obj = self.get_object(request, unquote(object_id))

                if self.show_prev_next:
                    prev, next = self.get_prev_next_pks(request, obj)
                    response.context_data.update(
                        {
                            'show_prev_next': self.show_prev_next,
                            'prev': prev,
                            'next': next,
                        }
                    )

            all = None
            preserved_filters = response.context_data.get('preserved_filters')
            preserved_filters = dict(parse_qsl(preserved_filters, keep_blank_values=True))
            changelist_filters = preserved_filters.get('_changelist_filters', '')
            if changelist_filters.startswith('all=') or '&all=' in changelist_filters:
                all = True

            changelist_url = add_preserved_filters(
                response.context_data,
                self._get_changelist_url(request),
                all=all,
            )

            response.context_data.update(
                {
                    'save': self.save,
                    'save_as_new': self.save_as_new,
                    'save_and_add_another': self.save_and_add_another,
                    'show_add': getattr(self, 'show_add', None),
                    'can_access_changelist': self.can_access_changelist,
                    'changelist_url': changelist_url,
                    'action_choices': self.get_action_choices(request),
                }
            )

            response.context_data.update(extra_context or {})

        return response

    def response_action(self, request, queryset):
        response = super().response_action(request, queryset)
        if response:
            # if the action returns a valid response that is not a redirect
            # like `delete_selected` show the response in a dialog
            # because the url will not change
            status_code = getattr(response, 'status', getattr(response, 'status_code', None))

            if status_code == 200:
                if isinstance(response, TemplateResponse):
                    content = response.render().content
                else:
                    content = response.content

                if response.headers.get('Content-Disposition'):
                    return response

                return JsonResponse(
                    {
                        'dialog': 'action-dialog',
                        'content': content.decode('utf-8'),
                    }
                )

        return response

    def response_add(self, request, obj, post_url_continue=None):
        next = request.POST.get('next')
        if next and url_has_allowed_host_and_scheme(next, request.get_host()):
            opts = obj._meta
            msg_dict = {'name': force_str(opts.verbose_name), 'obj': force_str(obj)}
            msg = _('The %(name)s "%(obj)s" was added successfully.') % msg_dict
            self.message_user(request, msg, messages.SUCCESS)
            return HttpResponseRedirect(next)
        else:
            return super().response_add(request, obj, post_url_continue)

    def response_change(self, request, obj):
        next = request.POST.get('next')
        if next and url_has_allowed_host_and_scheme(next, request.get_host()):
            opts = obj._meta
            msg_dict = {'name': force_str(opts.verbose_name), 'obj': force_str(obj)}
            msg = _('The %(name)s "%(obj)s" was changed successfully.') % msg_dict
            self.message_user(request, msg, messages.SUCCESS)
            return HttpResponseRedirect(next)
        else:
            return super().response_change(request, obj)

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        form_url = form_url or request.get_full_path()
        return super().render_change_form(request, context, add=add, change=change, form_url=form_url, obj=obj)

    def _get_changelist_url(self, request):
        return reverse(
            'admin:{}_{}_changelist'.format(
                self.model._meta.app_label,
                self.model._meta.model_name,
            )
        )

    def _get_add_url(self, request):
        return reverse(
            'admin:{}_{}_add'.format(
                self.model._meta.app_label,
                self.model._meta.model_name,
            )
        )


class RelatedModelAdminMixin:
    rel = None
    is_related_model_admin = True
    redirect_to_related_on_delete = False

    def get_urls(self):
        from django.urls import path

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        urlpatterns = [
            path(
                'add/<str:related_id>/',
                wrap(self.add_view),
                name='{}_{}_related_add'.format(*info),
            ),
            path(
                '<str:object_id>/delete/<str:related_id>/',
                wrap(self.delete_view),
                name='{}_{}_related_delete'.format(*info),
            ),
            path(
                '<str:object_id>/change/<str:related_id>/',
                wrap(self.change_view),
                name='{}_{}_related_change'.format(*info),
            ),
            path(
                'changelist/<str:related_id>/',
                wrap(self.changelist_view),
                name='{}_{}_related_changelist'.format(*info),
            ),
        ]

        return urlpatterns + super().get_urls()

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        related_id = self.get_related_id(request)
        if related_id:
            queryset = queryset.filter(**{self.rel: related_id})
        return queryset

    def get_relation(self):
        return self.model._meta.get_field(self.rel).remote_field

    def get_related_id(self, request):
        return request.resolver_match.kwargs.get('related_id')

    def get_related_model(self):
        return self.get_relation().model

    def get_related_object(self, request):
        id = self.get_related_id(request)
        related_model = self.get_related_model()
        relation = related_model.objects.get(pk=int(id))
        return relation

    def get_changeform_initial_data(self, request):
        initial = super().get_changeform_initial_data(request)
        initial[self.rel] = self.get_related_id(request)
        return initial

    def get_form(self, request, obj=None, change=False, **kwargs):
        kwargs.setdefault('widgets', {})
        kwargs['widgets'][self.rel] = forms.HiddenInput()
        return super().get_form(request, obj=obj, change=change, **kwargs)

    def response_add(self, request, obj, post_url_continue=None):
        opts = obj._meta
        related_id = self.get_related_id(request)
        if related_id:
            post_url_continue = post_url_continue or reverse(
                f'admin:{opts.app_label}_{opts.model_name}_related_change',
                kwargs={'object_id': quote(obj.pk), 'related_id': related_id},
            )
        response = super().response_add(request, obj, post_url_continue)
        self.patch_redirect(request, response)
        return response

    def response_change(self, request, obj):
        response = super().response_change(request, obj)
        self.patch_redirect(request, response)
        return response

    def response_delete(self, request, obj_display, obj_id):
        response = super().response_delete(request, obj_display, obj_id)
        self.patch_redirect(request, response)
        return response

    def patch_redirect(self, request, response):
        opts = self.model._meta
        suffixes = ['change', 'add', 'history', 'delete', 'changelist']
        view_names = [f'admin:{opts.app_label}_{opts.model_name}_{s}' for s in suffixes]
        related_id = self.get_related_id(request)

        if isinstance(response, HttpResponseRedirectBase) and related_id:
            if url_has_allowed_host_and_scheme(response['Location'], request.get_host()):
                parse = urlparse(response['Location'])
                match = resolve(parse.path)
                if match.view_name in view_names:
                    prefix, suffix = match.view_name.rsplit('_', 1)
                    kwargs = match.kwargs or {}
                    kwargs['related_id'] = quote(related_id)
                    new_path = reverse(f'{prefix}_related_{suffix}', kwargs=kwargs)
                    new_location = '{}{}{}'.format(
                        new_path,
                        parse.query and f'?{parse.query}' or '',
                        parse.fragment and f'#{parse.fragment}' or '',
                    )
                    response['Location'] = new_location

    def get_related_context(self, request):
        rel_obj = self.get_related_object(request)

        add_url = add_preserved_filters(
            {
                'preserved_filters': self.get_preserved_filters(request),
                'opts': self.model._meta,
            },
            reverse(
                f'admin:{self.model._meta.app_label}_{self.model._meta.model_name}_related_add',
                kwargs={'related_id': rel_obj.pk},
            ),
        )

        related_changelist_url = reverse(f'admin:{rel_obj._meta.app_label}_{rel_obj._meta.model_name}_changelist')
        related_add_url = reverse(f'admin:{rel_obj._meta.app_label}_{rel_obj._meta.model_name}_add')
        related_change_url = reverse(
            f'admin:{rel_obj._meta.app_label}_{rel_obj._meta.model_name}_change',
            kwargs={'object_id': rel_obj.pk},
        )
        related_delete_url = reverse(
            f'admin:{rel_obj._meta.app_label}_{rel_obj._meta.model_name}_delete',
            kwargs={'object_id': rel_obj.pk},
        )

        return {
            'related': {
                'object': rel_obj,
                'name': self.rel,
                'opts': rel_obj._meta,
                'id': rel_obj.pk,
                'changelist_url': related_changelist_url,
                'changelist_url_with_preserved_filters': add_preserved_filters(
                    {
                        'preserved_filters': self.get_preserved_filters(request),
                        'opts': rel_obj._meta,
                    },
                    related_changelist_url,
                ),
                'add_url': related_add_url,
                'add_url_with_preserved_filters': add_preserved_filters(
                    {
                        'preserved_filters': self.get_preserved_filters(request),
                        'opts': rel_obj._meta,
                    },
                    related_add_url,
                ),
                'change_url': related_change_url,
                'change_url_with_preserved_filters': add_preserved_filters(
                    {
                        'preserved_filters': self.get_preserved_filters(request),
                        'opts': rel_obj._meta,
                    },
                    related_change_url,
                ),
                'delete_url': related_delete_url,
                'delete_url_with_preserved_filters': add_preserved_filters(
                    {
                        'preserved_filters': self.get_preserved_filters(request),
                        'opts': rel_obj._meta,
                    },
                    related_delete_url,
                ),
            },
            self.rel: rel_obj,
            'add_url': add_url,
        }

    def delete_view(self, request, object_id, related_id=None, extra_context=None):
        if not related_id:
            obj = self.get_object(request, unquote(object_id))
            if obj:
                redirect_url = reverse(
                    f'admin:{self.model._meta.app_label}_{self.model._meta.model_name}_related_delete',
                    kwargs={
                        'object_id': unquote(object_id),
                        'related_id': getattr(obj, f'{self.rel}_id'),
                    },
                )
                redirect_url = add_preserved_filters(
                    {
                        'preserved_filters': self.get_preserved_filters(request),
                        'opts': obj._meta,
                    },
                    redirect_url,
                )
                return redirect(redirect_url)

        related_object = self.get_related_object(request)
        context = self.get_related_context(request)
        if extra_context:
            context.update(extra_context)

        response = super().delete_view(request, object_id, context)

        if self.redirect_to_related_on_delete and isinstance(response, HttpResponseRedirect):
            response['Location'] = reverse(
                f'admin:{related_object._meta.app_label}_{related_object._meta.model_name}_change',
                kwargs={'object_id': related_object.pk},
            )
        return response

    def change_view(self, request, object_id, related_id=None, form_url='', extra_context=None):
        if not related_id:
            obj = self.get_object(request, unquote(object_id))
            if obj:
                redirect_url = reverse(
                    f'admin:{self.model._meta.app_label}_{self.model._meta.model_name}_related_change',
                    kwargs={
                        'object_id': unquote(object_id),
                        'related_id': getattr(obj, f'{self.rel}_id'),
                    },
                )
                redirect_url = add_preserved_filters(
                    {
                        'preserved_filters': self.get_preserved_filters(request),
                        'opts': obj._meta,
                    },
                    redirect_url,
                )
                return redirect(redirect_url)
        context = self.get_related_context(request)
        if extra_context:
            context.update(extra_context)
        return super().change_view(request, object_id, form_url, context)

    def add_view(self, request, related_id=None, form_url='', extra_context=None):
        context = self.get_related_context(request)
        if extra_context:
            context.update(extra_context)
        return super().add_view(request, form_url, context)

    def changelist_view(self, request, related_id=None, extra_context=None):
        context = self.get_related_context(request)
        if extra_context:
            context.update(extra_context)
        return super().changelist_view(request, context)

    def _get_changelist_url(self, request):
        return reverse(
            'admin:{}_{}_related_changelist'.format(
                self.model._meta.app_label,
                self.model._meta.model_name,
            ),
            kwargs={'related_id': self.get_related_id(request)},
        )

    def _get_add_url(self, request):
        return reverse(
            'admin:{}_{}_related_add'.format(
                self.model._meta.app_label,
                self.model._meta.model_name,
            ),
            kwargs={'related_id': self.get_related_id(request)},
        )

    def get_custom_view_default_regex(self, name):
        return rf'^(?:(?P<object_id>[^/]+)/)?{name}/(?:(?P<related_id>[^/]+)/)?$'

    def get_preserved_filters(self, request):
        match = request.resolver_match
        if self.preserve_filters and match:
            opts = self.model._meta
            current_url = f'{match.app_name}:{match.url_name}'
            changelist_url = f'admin:{opts.app_label}_{opts.model_name}_changelist'
            related_changelist_url = f'admin:{opts.app_label}_{opts.model_name}_related_changelist'
            if current_url in (changelist_url, related_changelist_url):
                preserved_filters = request.GET.urlencode()
            else:
                preserved_filters = request.GET.get('_changelist_filters')

            if preserved_filters:
                return urlencode({'_changelist_filters': preserved_filters})
        return ''


class ModelAdmin(ModelAdminMixin, admin.ModelAdmin):
    pass


class RelatedModelAdmin(RelatedModelAdminMixin, ModelAdmin):
    pass


class RelatedImportMixin(ImportMixin):
    def get_form_kwargs(self, form, *args, **kwargs):
        if 'related_id' in kwargs:
            kwargs.pop('related_id')
        return kwargs

    def get_urls(self):
        urls = super().get_urls()
        info = self.get_model_info()
        urls[0] = path(
            'process_import/<str:related_id>',
            self.admin_site.admin_view(self.process_import),
            name='{}_{}_related_process_import'.format(*info),
        )
        urls[1] = path(
            'import/<str:related_id>',
            self.admin_site.admin_view(self.import_action),
            name='{}_{}_related_import'.format(*info),
        )
        return urls

    def process_result(self, result, request):
        self.generate_log_entries(result, request)
        self.add_success_message(result, request)
        post_import.send(sender=None, model=self.model)

        url = reverse(
            'admin:{}_{}_related_changelist'.format(*self.get_model_info()),
            current_app=self.admin_site.name,
            kwargs={'related_id': self.get_related_id(request)},
        )
        return HttpResponseRedirect(url)

    def import_action(self, request, *args, **kwargs):
        response = super().import_action(request, *kwargs, **kwargs)
        if isinstance(response, TemplateResponse):
            response.context_data.update(self.get_related_context(request))
        return response


class RelatedExportMixin(ExportMixin):
    def get_urls(self):
        urls = super().get_urls()
        urls[0] = path(
            'export/<str:related_id>',
            self.admin_site.admin_view(self.export_action),
            name='{}_{}_related_export'.format(*self.get_model_info()),
        )

        return urls

    def export_action(self, request, *args, **kwargs):
        response = super().export_action(request, *kwargs, **kwargs)
        if isinstance(response, TemplateResponse):
            response.context_data.update(self.get_related_context(request))
        return response


class RelatedImportExportMixin(RelatedImportMixin, RelatedExportMixin):
    change_list_template = 'admin/import_export/change_list_import_export.html'


class TreeModelAdmin(ModelAdmin):
    """Manages treebeard model."""

    form = TreeFormAdmin

    def get_queryset(self, request):
        # exclude the root nodes
        qs = self.model._default_manager.get_queryset().exclude(
            pk__in=[root.pk for root in self.model.get_root_nodes()]
        )
        # TODO: this should be handled by some parameter to the ChangeList.
        ordering = self.ordering or ()  # otherwise we might try to *None, which is bad ;)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs


class InlineModelAdminMixin(ModelAdminMixin):
    pass


class StackedInline(InlineModelAdminMixin, admin.options.StackedInline):
    extra = 0


class TabularInline(InlineModelAdminMixin, admin.options.TabularInline):
    extra = 0
    show_original = False

    _default_formfield_overrides = ModelAdminMixin._default_formfield_overrides.copy()
    _default_formfield_overrides.update(
        {
            models.ImageField: {'widget': widgets.AdminImageWidgetSmall},
        }
    )


class TabularGridInline(TabularInline):
    template = 'admin/edit_inline/tabular_grid.html'
