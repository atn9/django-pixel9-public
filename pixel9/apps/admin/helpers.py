from operator import itemgetter

from django.contrib.admin.helpers import (
    AdminField,
    AdminReadonlyField,
    Fieldline,
    InlineAdminFormSet,
)
from django.contrib.admin.utils import (
    flatten_fieldsets,
    help_text_for_field,
    label_for_field,
)


class CustomInlineAdminFormSet(InlineAdminFormSet):
    def _get_admin_forms(self):
        forms = []
        for form in super().__iter__():
            forms.append(form)

        # return forms ordered even if there was a ValidationError
        manual_ordering_field = getattr(self.opts, 'manual_ordering_field', None)
        if manual_ordering_field:
            forms_with_ordering = []
            originals = {}
            for form in forms:
                if form.original:
                    originals[str(form.original.id)] = form.original
                for fieldset in form:
                    for line in fieldset:
                        for field in line:
                            if field.is_readonly:
                                name = field.field['name']
                                if field.form.is_bound:
                                    value = field.form.cleaned_data.get(name)
                                else:
                                    value = field.form.initial.get(name)
                            else:
                                name = field.field.name
                                value = field.field.value()
                            if name == manual_ordering_field:
                                if value is None:
                                    value = 99999999
                                value = int(value)
                                forms_with_ordering.append((value, form))

            if forms_with_ordering:
                forms = [f[1] for f in sorted(forms_with_ordering, key=itemgetter(0))]
                for form in forms:
                    pk_value = form.pk_field().field.value()
                    if pk_value:
                        pk_value = str(pk_value)
                    if pk_value and pk_value in originals:
                        form.original = originals[pk_value]
                    else:
                        form.original = None

        return forms

    def __iter__(self):
        admin_forms = self._get_admin_forms()
        yield from admin_forms

    def fields(self):
        fk = getattr(self.formset, 'fk', None)
        for _, field_name in enumerate(flatten_fieldsets(self.fieldsets)):
            if fk and fk.name == field_name:
                continue
            if field_name in self.readonly_fields:
                yield {
                    'name': field_name,
                    'align': self.opts.get_column_align(field_name),
                    'visually_hidden': self.opts.get_column_hidden(field_name),
                    'label': label_for_field(field_name, self.opts.model, self.opts),
                    'widget': {'is_hidden': False},
                    'required': False,
                    'help_text': help_text_for_field(field_name, self.opts.model),
                }
            else:
                form_field = self.formset.empty_form.fields[field_name]
                label = form_field.label
                if label is None:
                    label = label_for_field(field_name, self.opts.model, self.opts)
                yield {
                    'name': field_name,
                    'align': self.opts.get_column_align(field_name),
                    'visually_hidden': self.opts.get_column_hidden(field_name),
                    'label': label,
                    'widget': form_field.widget,
                    'required': form_field.required,
                    'help_text': form_field.help_text,
                }


class CustomAdminReadonlyField(AdminReadonlyField):
    def __init__(self, form, field, is_first, model_admin=None):
        super().__init__(form, field, is_first, model_admin)
        if model_admin:
            self.field['cell_attributes'] = model_admin.get_cell_attributes(form.instance, self.field['name'])
            if 'class' not in self.field['cell_attributes']:
                self.field['cell_attributes']['class'] = ''
            self.field['cell_attributes']['class'] += ' readonly'
            self.field['cell_attributes']['class'] += ' field-{}'.format(self.field['name'])
            if self.field.get('errors') or form.non_field_errors():
                self.field['cell_attributes']['class'] += ' has-error'
            align = model_admin.get_column_align(self.field['name'])
            if align:
                self.field['cell_attributes']['class'] += f' column-align-{align}'

            hidden = model_admin.get_column_hidden(self.field['name'])
            if hidden:
                self.field['cell_attributes']['class'] += ' hidden'


class CustomAdminField(AdminField):
    def __init__(self, form, field, is_first, model_admin=None):
        super().__init__(form, field, is_first)
        if model_admin:
            self.field.cell_attributes = model_admin.get_cell_attributes(form.instance, self.field.name)
            if 'class' not in self.field.cell_attributes:
                self.field.cell_attributes['class'] = ''
            self.field.cell_attributes['class'] += f' field-{self.field.name}'

            if self.field.errors or form.non_field_errors():
                self.field.cell_attributes['class'] += ' has-error'

            align = model_admin.get_column_align(self.field.name)
            if align:
                self.field.cell_attributes['class'] += f' column-align-{align}'

            hidden = model_admin.get_column_hidden(self.field.name)
            if hidden:
                self.field.cell_attributes['class'] += ' hidden'


class CustomFieldline(Fieldline):
    def __iter__(self):
        for i, field in enumerate(self.fields):
            if field in self.readonly_fields:
                yield CustomAdminReadonlyField(self.form, field, is_first=(i == 0), model_admin=self.model_admin)
            else:
                yield CustomAdminField(self.form, field, is_first=(i == 0), model_admin=self.model_admin)
