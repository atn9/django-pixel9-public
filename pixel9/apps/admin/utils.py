import datetime
import decimal
import json
import re

from django.db import models
from django.http import HttpResponse
from django.utils import formats, timezone
from django.utils.encoding import force_str, smart_str
from django.utils.html import format_html

msg_re = re.compile(r'^\s*(?:\[\s*(?P<title>.*)\s*\])?\s*(?P<msg>.*)\s*$', re.DOTALL)


def json_view(func):
    def wrap(admin, request, *args, **kwargs):
        response = func(admin, request, *args, **kwargs)
        return HttpResponse(json.dumps(response), content_type='application/json')

    return wrap


def get_form_errors_dict(form, formsets=None):
    errors = {}
    for field in form:
        if field.errors:
            errors['id_' + field.html_name] = str(field.errors)

    if formsets is not None:
        for formset in formsets:
            for form in formset.forms:
                for field in form:
                    if field.errors:
                        errors['id_' + field.html_name] = str(field.errors)
    return errors


def display_for_field(value, field, empty_value_display):
    from django.contrib.admin.templatetags.admin_list import _boolean_icon

    if getattr(field, 'flatchoices', None):
        return dict(field.flatchoices).get(value, empty_value_display)
    # NullBooleanField needs special-case null-handling, so it comes
    # before the general null test.
    elif isinstance(field, models.BooleanField) or isinstance(field, models.NullBooleanField):
        return _boolean_icon(value)
    elif value is None:
        return empty_value_display
    elif isinstance(field, models.DateTimeField):
        return formats.localize(timezone.template_localtime(value))
    elif isinstance(field, (models.DateField, models.TimeField)):
        return formats.localize(value)
    elif isinstance(field, models.DecimalField):
        return formats.number_format(value)
    elif isinstance(field, (models.IntegerField, models.FloatField)):
        return formats.number_format(value)
    elif isinstance(field, models.FileField) and value:
        return format_html('<a href="{}">{}</a>', value.url, value)
    else:
        return display_for_value(value, empty_value_display)


def display_for_value(value, empty_value_display, boolean=False):
    from django.contrib.admin.templatetags.admin_list import _boolean_icon

    if boolean:
        return _boolean_icon(value)
    elif value is None:
        return empty_value_display
    elif isinstance(value, datetime.datetime):
        return formats.localize(timezone.template_localtime(value))
    elif isinstance(value, (datetime.date, datetime.time)):
        return formats.localize(value)
    elif isinstance(value, (int, decimal.Decimal, float)):
        return formats.number_format(value)
    elif isinstance(value, (list, tuple)):
        return ', '.join(force_str(v) for v in value)
    else:
        return smart_str(value)
