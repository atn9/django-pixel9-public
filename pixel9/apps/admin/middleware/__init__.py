from django import http

from pixel9.apps import admin


class StaffOnlyMiddleware:
    def process_view(self, request, view_func, view_args, view_kwargs):
        request._staff_only_processed = True
        return admin.site.admin_view(view_func)(request, *view_args, **view_kwargs)

    def process_response(self, request, response):
        if response.status_code not in (301, 302, 500):
            if not hasattr(request, '_staff_only_processed') and not request.user.is_staff:
                return http.HttpResponseForbidden('Accès interdit')
        return response
