from django.apps import AppConfig


class AdminConfig(AppConfig):
    name = 'pixel9.apps.admin'
    label = 'pixel9_admin'

    def ready(self):
        # monkey patching:

        from django.contrib.admin import utils

        from .utils import display_for_field, display_for_value

        utils.display_for_field = display_for_field
        utils.display_for_value = display_for_value

        from admin_auto_filters import filters as auto_filters
        from django.contrib import admin
        from django.contrib.admin.templatetags import admin_list

        from . import filters  # do not remove
        from .forms.widgets import CustomAutocompleteSelect
        from .helpers import (
            CustomAdminField,
            CustomAdminReadonlyField,
            CustomFieldline,
            CustomInlineAdminFormSet,
        )
        from .options import ModelAdmin, StackedInline, TabularInline
        from .sites import site
        from .templatetags.pixel9_admin_list import _boolean_icon

        admin_list._boolean_icon = _boolean_icon
        admin.sites.site = site
        admin.site = site
        admin.ModelAdmin = ModelAdmin
        admin.StackedInline = StackedInline
        admin.TabularInline = TabularInline
        admin.helpers.InlineAdminFormSet = CustomInlineAdminFormSet
        admin.helpers.AdminReadonlyField = CustomAdminReadonlyField
        admin.helpers.Fieldline = CustomFieldline
        admin.helpers.AdminField = CustomAdminField

        auto_filters.AutocompleteSelect = CustomAutocompleteSelect

        super().ready()
