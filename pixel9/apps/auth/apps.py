from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'pixel9.apps.auth'
    label = 'pixel9_auth'
