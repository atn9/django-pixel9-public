import itertools
import re

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import int_to_base36
from django.utils.translation import gettext_lazy as _

SHA1_RE = re.compile('^[a-f0-9]{40}$')

SECURE_COOKIE_KEY = getattr(settings, 'SECURE_COOKIE_KEY', '_secure')  # secure is a reserved key


def get_unique_username_from_email(email):
    username = email[:140]
    count = itertools.count(1)
    while User.objects.filter(username=username).exists():
        username = f'{next(count)}_{username}'
    return username


def create_email_user(email, password=None, **extra_fields):
    UserModel = get_user_model()
    return UserModel.objects.create_user(
        username=get_unique_username_from_email(email),
        email=email,
        password=password,
        **extra_fields,
    )


def create_email_superuser(email, password, **extra_fields):
    UserModel = get_user_model()
    return UserModel.objects.create_superuser(
        username=get_unique_username_from_email(email),
        email=email,
        password=password,
        **extra_fields,
    )


def send_create_password_invitation(user, request):
    current_site = get_current_site(request)
    subject = _('Mot de passe %s') % current_site.name
    msg = render_to_string(
        'auth/choose_password_email.html',
        {
            'email': user.email,
            'domain': current_site.domain,
            'site_name': current_site.name,
            'uid': int_to_base36(user.id),
            'user': user,
            'token': default_token_generator.make_token(user),
            'protocol': 'http',
        },
    )
    user.email_user(subject, msg, settings.DEFAULT_FROM_EMAIL)
