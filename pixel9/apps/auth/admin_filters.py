from pixel9.apps.admin import filters


class GroupPermissionFilter(filters.AutocompleteFilter):
    title = 'Permission'
    field_name = 'permissions'


class UserPermissionFilter(filters.AutocompleteFilter):
    title = 'Permission'
    field_name = 'user_permissions'


class UserGroupFilter(filters.AutocompleteFilter):
    title = 'Groupe'
    field_name = 'groups'


class PermissionGroupFilter(filters.AutocompleteFilter):
    title = 'Groupe'
    field_name = 'group'


class PermissionUserFilter(filters.AutocompleteFilter):
    title = 'Utilisateur'
    field_name = 'user'
