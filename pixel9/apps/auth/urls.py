from django.urls import path, re_path

from . import views

urlpatterns = [
    path('login', views.login, name='auth_login'),
    path('logout', views.logout, name='auth_logout'),
    path('register', views.register, name='auth_register'),
    path('password/reset', views.password_reset, name='auth_password_reset'),
    re_path(
        r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)$',
        views.password_reset_confirm,
        name='auth_password_reset_confirm',
    ),
]
