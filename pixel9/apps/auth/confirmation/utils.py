from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import activate, get_language
from django.utils.translation import gettext as _

from pixel9.apps.emails.utils import render_html_email
from pixel9.utils.queues import enqueue_or_call

from .models import ConfirmationKey

SEND_HTML = getattr(settings, 'CONFIRMATION_SEND_HTML', False)


def send_confirmation_mail(
    recipient,
    key,
    confirm_url_domain,
    language=None,
    next_url=None,
    confirm_url_protocol='http://',
):
    text_template_name = 'auth/confirmation_email.txt'
    html_template_name = 'auth/confirmation_email.html'

    current_language = get_language()

    activate(language or settings.LANGUAGE_CODE)
    url = reverse('confirmation_confirm', args=[key])
    # XXX we have to call `activate` again because the active language
    # is overriden after calling `reverse` from an rq worker
    activate(language or settings.LANGUAGE_CODE)
    if next_url:
        url += '?next=' + next_url
    confirm_url = confirm_url_protocol + confirm_url_domain + url

    text_content = render_to_string(text_template_name, {'confirm_url': confirm_url})
    html_content = render_html_email(html_template_name, {'confirm_url': confirm_url})

    msg = EmailMultiAlternatives(
        subject=_('Confirmation de votre adresse courriel'),
        body=text_content,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=[recipient],
    )
    if SEND_HTML:
        msg.attach_alternative(html_content, 'text/html')

    msg.send()
    activate(current_language)


def send_confirmation(user, confirm_url_domain, confirm_url_is_secure, next_url):
    key = ConfirmationKey.objects.get_or_create(email=user.email, user=user)[0]
    if not key.confirmed:
        enqueue_or_call(
            send_confirmation_mail,
            kwargs={
                'recipient': key.email,
                'key': key.key,
                'language': get_language(),
                'next_url': next_url,
                'confirm_url_domain': confirm_url_domain,
                'confirm_url_protocol': confirm_url_is_secure and 'https://' or 'http://',
            },
        )


def user_email_is_confirmed(user):
    try:
        key = ConfirmationKey.objects.get(email=user.email, user=user)
    except ConfirmationKey.DoesNotExist:
        return False
    if key.confirmed:
        return True
    return False
