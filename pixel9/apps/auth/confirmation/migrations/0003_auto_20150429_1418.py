from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('confirmation', '0002_auto_20150409_1309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='confirmationkey',
            name='email',
            field=models.EmailField(max_length=254),
        ),
    ]
