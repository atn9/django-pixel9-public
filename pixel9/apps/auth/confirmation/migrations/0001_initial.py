from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ConfirmationKey',
            fields=[
                (
                    'id',
                    models.AutoField(
                        verbose_name='ID',
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ('email', models.EmailField(unique=True, max_length=75)),
                ('confirmed', models.BooleanField(default=False)),
                ('key', models.CharField(unique=True, max_length=15)),
                (
                    'user',
                    models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
    ]
