from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test

from .utils import user_email_is_confirmed


def confirmation_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, confirmation_url=None):
    actual_decorator = user_passes_test(
        user_email_is_confirmed,
        login_url=confirmation_url or settings.CONFIRMATION_URL,
        redirect_field_name=redirect_field_name,
    )
    if function:
        return actual_decorator(function)
    return actual_decorator
