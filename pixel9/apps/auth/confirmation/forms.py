from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

from pixel9 import forms


class EmailForm(forms.Form):
    email = forms.EmailField(label=_('Adresse courriel'))

    def clean_email(self):
        if not User.objects.filter(email__iexact=self.cleaned_data['email']):
            return self.cleaned_data['email']
        raise forms.ValidationError(_('Cette adresse courriel est déjà enregistré.'))
