from django.urls import path, re_path

from . import views

urlpatterns = [
    path('required', views.confirmation_required, name='confirmation_required'),
    # url(r'^change$',    views.change_email_address,  name='confirmation_change_email_address'),
    re_path(r'^(\w{14})$', views.confirm, name='confirmation_confirm'),
    # url(r'^_test$',     views.test,                  name='confirmation_test'),
]
