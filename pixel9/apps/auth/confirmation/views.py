from django import http
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import get_language
from django.utils.translation import gettext as _
from django.views.generic import FormView, TemplateView, View
from django_ratelimit.decorators import ratelimit

from pixel9.views import PageMixin, RedirectableMixin

from ..mixins import LoginRequiredMixin
from .decorators import confirmation_required
from .forms import EmailForm
from .models import ConfirmationKey
from .signals import email_confirmed
from .utils import send_confirmation


class ConfirmationRequiredView(LoginRequiredMixin, RedirectableMixin, PageMixin, TemplateView):
    template_name = 'auth/confirmation_required.html'
    page_key = 'auth_confimation_required'

    def get(self, request, *args, **kwargs):
        key = ConfirmationKey.objects.get_or_create(email=self.request.user.email, user=self.request.user)[0]
        if key.confirmed:
            return redirect(self.get_redirect_url())
        return super().get(request, *args, **kwargs)

    @method_decorator(ratelimit(key='ip', rate='10/d', method='POST'))
    @method_decorator(ratelimit(key='user', rate='10/d', method='POST'))
    def post(self, request, *args, **kwargs):
        send_confirmation(
            user=request.user,
            confirm_url_domain=get_current_site(request).domain,
            confirm_url_is_secure=request.is_secure(),
            next_url=self.get_redirect_url(),
        )
        messages.warning(
            request,
            _("Un message a été envoyé à l'adresse {0} " 'avec les instructions pour la valider.').format(
                request.user.email
            ),
        )

        return redirect(self.get_redirect_url())

    def get_default_page_title(self):
        return _('Confirmation de votre adresse courriel')

    def get_default_redirect_url(self):
        return reverse('index')


class ChangeEmailAddress(LoginRequiredMixin, RedirectableMixin, PageMixin, FormView):
    template_name = 'auth/change_email_address.html'
    form_class = EmailForm
    page_key = 'auth_confirmation_change_email_address'

    @method_decorator(ratelimit(key='ip', rate='10/d', method='POST'))
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        self.request.user.email = form.cleaned_data['email']
        self.request.user.save()
        send_confirmation(
            user=self.request.user,
            confirm_url_domain=get_current_site(self.request).domain,
            confirm_url_is_secure=self.request.is_secure(),
            next_url=self.get_redirect_url(),
        )
        messages.warning(
            self.request,
            _("Un message a été envoyé à l'adresse {0} " 'avec les instructions pour la valider.').format(
                self.request.user.email
            ),
        )
        return redirect(self.get_redirect_url())

    def get_default_page_title(self):
        return _("Changement d'adresse courriel")


class ConfirmView(RedirectableMixin, View):
    @method_decorator(ratelimit(key='ip', rate='10/d', method='GET'))
    def get(self, request, secret, *args, **kwargs):
        try:
            key = ConfirmationKey.objects.get(key=secret)
        except ConfirmationKey.DoesNotExist as e:
            raise http.Http404 from e

        if not key.confirmed:
            key.confirmed = True
            key.save()
            email_confirmed.send(
                sender=key,
                email=key.email,
                language=get_language()[:2],
                ip=request.META.get('REMOTE_ADDR'),
            )

        messages.info(self.request, _("Merci d'avoir confirmé votre adresse courriel"))

        return redirect(self.get_redirect_url())

    def get_default_redirect_url(self):
        return reverse('index')


@confirmation_required
def test(request):
    return render(request, 'base.html')


confirm = ConfirmView.as_view()
confirmation_required = ConfirmationRequiredView.as_view()
change_email_address = ChangeEmailAddress.as_view()
