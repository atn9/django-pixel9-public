from django.conf import settings
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import ImproperlyConfigured, PermissionDenied
from django.utils.encoding import force_str

from ..mixins import AccessMixin, LoginRequiredMixin
from .utils import user_email_is_confirmed


class ConfirmationRequiredMixin(AccessMixin):
    confirmation_url = None

    def get_confirmation_url(self):
        """
        Override this method to customize the confirmation_url.
        """
        confirmation_url = self.confirmation_url or settings.CONFIRMATION_URL
        if not confirmation_url:
            raise ImproperlyConfigured(
                'Define {cls}.confirmation_url or settings.CONFIRMATION_URL or override '
                '{cls}.get_confirmation_url().'.format(cls=self.__class__.__name__)
            )

        return force_str(confirmation_url)

    def dispatch(self, request, *args, **kwargs):
        if not user_email_is_confirmed(request.user):
            if self.raise_exception:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect_to_login(
                    request.get_full_path(),
                    self.get_confirmation_url(),
                    self.get_redirect_field_name(),
                )

        return super().dispatch(request, *args, **kwargs)


class LoginAndConfirmationRequiredMixin(LoginRequiredMixin, ConfirmationRequiredMixin):
    pass
