import hashlib
from random import random

from django.conf import settings

from pixel9.db import models


def create_salted_key(string):
    salt = hashlib.sha512(str(random()).encode('utf-8')).hexdigest()[:5]  # noqa: S311
    salt = salt.encode('utf-8')
    if isinstance(string, str):
        string = string.encode('utf-8')
    return hashlib.sha512(salt + string).hexdigest()[:14]


class ConfirmationKey(models.Model):
    email = models.EmailField()
    confirmed = models.BooleanField(blank=True, default=False)
    key = models.CharField(max_length=15, unique=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = create_salted_key(self.email)
        super().save(*args, **kwargs)
