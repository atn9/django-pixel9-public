from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    password_validation,
)
from django.contrib.auth.forms import UserChangeForm
from django.utils.translation import gettext_lazy as _

from pixel9 import forms
from pixel9.apps.auth.utils import create_email_user

User = get_user_model()


class EmailRegistrationForm(forms.Form):
    email = forms.EmailField(label=_('Votre adresse courriel'))
    password = forms.CharField(
        widget=forms.PasswordInput,
        label=_('Choisissez un mot de passe'),
        strip=False,
    )

    def __init__(self, request, *args, **kwargs):
        self.user = None
        self.request = request
        super().__init__(*args, **kwargs)

    def clean_password(self):
        password = self.cleaned_data.get('password')
        password_validation.validate_password(
            password,
            User(email=self.cleaned_data.get('email')),
        )
        return password

    def clean_email(self):
        email = self.cleaned_data['email'].strip().lower()
        if User.objects.filter(email__iexact=email):
            raise forms.ValidationError(_('Cette adresse courriel est déjà enregistrée.'))
        return email

    def save(self):
        email = self.cleaned_data['email']
        self.user = create_email_user(
            email=email,
            password=self.cleaned_data['password'],
        )

    def log_user_in(self):
        self.user = authenticate(
            username=self.cleaned_data['email'],
            password=self.cleaned_data['password'],
        )
        login(self.request, self.user)


class ConfirmPasswordEmailRegistrationForm(EmailRegistrationForm):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }

    password2 = forms.CharField(
        widget=forms.PasswordInput,
        label=_('Mot de passe (à nouveau)'),
        strip=False,
    )

    def clean_password2(self):
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')
        if password and password2 and password != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2


class EmailAuthenticationForm(forms.Form):
    email = forms.EmailField(label=_('Votre adresse courriel'), max_length=75, min_length=6)
    password = forms.CharField(label=_('Votre mot de passe'), widget=forms.PasswordInput)

    def __init__(self, request, *args, **kwargs):
        self.user = None
        self.request = request
        super().__init__(*args, **kwargs)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if email and password:
            self.user = authenticate(username=email, password=password)
            if self.user is None:
                raise forms.ValidationError(_('Adresse ou mot de passe invalide.'))

        return self.cleaned_data

    def save(self):
        login(self.request, self.user)


class PasswordResetForm(forms.Form):
    email = forms.EmailField(label=_('Votre adresse courriel'), max_length=75)

    def clean_email(self):
        """
        Validates that a user exists with the given e-mail address.
        """
        email = self.cleaned_data['email']
        self.users = User.objects.filter(email__iexact=email)
        if len(self.users) == 0:
            raise forms.ValidationError(
                _("That e-mail address doesn't have an associated user account. Are you sure you've registered?")
            )
        return email


class UserProfileForm(UserChangeForm):
    class Meta:
        model = User
        exclude = (
            'password',
            'is_staff',
            'is_active',
            'is_superuser',
            'last_login',
            'date_joined',
            'groups',
            'user_permissions',
        )
