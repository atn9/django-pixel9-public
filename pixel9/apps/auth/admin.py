from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin as DjangoGroupAdmin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.auth.models import Group, Permission, User

from .admin_filters import (
    GroupPermissionFilter,
    PermissionGroupFilter,
    PermissionUserFilter,
    UserGroupFilter,
    UserPermissionFilter,
)

admin.site.unregister(Group)
admin.site.unregister(User)


@admin.register(Group)
class GroupAdmin(DjangoGroupAdmin):
    list_filter = (GroupPermissionFilter,)
    autocomplete_fields = ('permissions',)


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    list_filter = (
        'is_staff',
        'is_superuser',
        'is_active',
        UserGroupFilter,
        UserPermissionFilter,
    )
    autocomplete_fields = (
        'groups',
        'user_permissions',
    )


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    search_fields = (
        'name',
        'codename',
        'content_type__app_label',
        'content_type__model',
    )
    list_filter = (
        'content_type__app_label',
        'content_type__model',
        PermissionGroupFilter,
        PermissionUserFilter,
    )
    list_display = ('name', 'get_app_label', 'content_type', 'codename')
    autocomplete_fields = ('content_type',)

    @admin.display(
        description='Application',
        ordering='content_type__app_label',
    )
    def get_app_label(self, obj):
        return obj.content_type.app_label
