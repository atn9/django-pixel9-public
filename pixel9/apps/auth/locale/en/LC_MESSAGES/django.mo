��    #      4  /   L        �   	     �     �  !   �  /   �          +  &   F  	   m     w  "     -   �     �     �  .   �  7        Q     a     {     �  !   �  R   �     &     =  I   X     �     �     �  $   �  b  
      m  #   �  @   �  h   �  1  \	  q   �
                     2     Q     k     }     �     �     �     �     �     �  +   �  "   !     D     P     a     u     �  ;   �     �     �  6   �     4     G     [     i  2  z     �     �     �  j   �                                	                                                     "                             !          #   
                              
   Afin de s'assurer que vous êtes le propriétaire de l'adresse courriel que vous nous avez fournie, nous devons la confirmer avec vous.
   Adresse confirmé Adresse courriel Adresse ou mot de passe invalide. Cette adresse courriel est déjà enregistrée. Changement d'adresse courriel Choisissez un mot de passe Confirmation de votre adresse courriel Connexion Envoyer Envoyer un message de confirmation Impossible de valider votre adresse courriel. Inscription Lien invalide Merci d'avoir confirmé votre adresse courriel Merci, votre adresse courriel est maintenant confirmé. Mot de passe %s Mot de passe (à nouveau) Mot de passe oublié ? Récupération de mot de passe Récupération de mot de passe %s Un message a été envoyé à l'adresse {0} avec les instructions pour la valider. Utiliser cette adresse Utiliser une autre adresse Veuillez suivre le lien suivant afin de confirmer votre adresse courriel. Votre adresse courriel Votre adresse courriel : Votre mot de passe Votre mot de passe a été modifié. Vous recevez ce message parce que vous avez demandé de récupéré le mot de passe de votre compte sur %(domain)s.

Visitez l'adresse suivante afin de choisir votre nouveau mot de passe :
%(reset_url)s

Si vous n'avez pas demandé de récupérer le mot de passe de votre compte sur %(domain)s vous pouvez ignorer ce message.

--
L'équipe de %(domain)s
 Vous êtes maintenant connecté. Vous êtes maintenant déconnecté. Vérifiez la boite de réception de votre adresse de messagerie. Vérifiez que l'adresse de cette page est identique au lien contenu dans le message que vous avez reçu. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-07-03 15:29-0500
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.6
 
In order to ensure that you are the owner of the email address you submitted, we need to confirm it with you.
   Address confirmed Email Invalid email or password This email already registered. Change your email address Choose a password Email address verification Login Send Send confirmation message Unable to validate your email Registration Invalid link Thank you for confirming your email address Thank you, your email is confirmed Password %s Password (again) Password forgotten? Password reset Password reset %s A message was sent to {0} with instructions to validate it. Use this address Use another email address Click on the link below to validate your email address Your email address Your email address: Your password Password updated You received this message because you requested to recover the password of your account on %(domain)s.

Visit the following address to choose a new password :
%(reset_url)s

If you did not request to recover the password of your account on%(domain)s you can ignore this message.

--
The team of %(domain)s
 You are now logged in You are now logged out Check your inbox Verify that the address of this page is identical to the link contained in the message that you received.  