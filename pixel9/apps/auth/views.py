from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.http import base36_to_int, int_to_base36
from django.utils.translation import gettext as _
from django.views.generic import FormView, RedirectView
from django_ratelimit.decorators import ratelimit

from pixel9.utils.http import set_query_parameter
from pixel9.views import PageMixin, RedirectableMixin

from .forms import EmailAuthenticationForm, EmailRegistrationForm, PasswordResetForm


class RegisterView(PageMixin, RedirectableMixin, FormView):
    page_key = 'register'
    template_name = 'auth/register.html'
    form_class = EmailRegistrationForm

    @method_decorator(ratelimit(key='ip', rate='12/d', method='POST'))
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def get_default_page_title(self):
        return _('Inscription')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        form.save()
        form.log_user_in()

        if 'pixel9.apps.auth.confirmation' in settings.INSTALLED_APPS:
            from .confirmation.utils import send_confirmation

            send_confirmation(
                user=self.request.user,
                confirm_url_domain=get_current_site(self.request).domain,
                confirm_url_is_secure=self.request.is_secure(),
                next_url=self.get_redirect_url(),
            )
            messages.warning(
                self.request,
                _("Un message a été envoyé à l'adresse {0} " 'avec les instructions pour la valider.').format(
                    self.request.user.email
                ),
            )
        return redirect(self.get_redirect_url())


class LoginView(PageMixin, RedirectableMixin, FormView):
    page_key = 'login'
    template_name = 'auth/login.html'
    form_class = EmailAuthenticationForm
    remember_field_name = 'remember'

    @method_decorator(ratelimit(key='ip', rate='12/d', method='POST'))
    @method_decorator(ratelimit(key='post:email', rate='12/d', method='POST'))
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def get_default_page_title(self):
        return _('Connexion')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def success_message(self):
        messages.info(self.request, _('Vous êtes maintenant connecté.'))

    def form_valid(self, form):
        form.save()
        self.success_message()
        return redirect(self.get_redirect_url())


class LogoutView(RedirectableMixin, RedirectView):
    page_key = 'logout'
    permanent = False
    redirect_field_name = 'next'

    def success_message(self):
        messages.info(self.request, _('Vous êtes maintenant déconnecté.'))

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            auth_logout(request)
            self.success_message()
        # prevent infinite redirection loops
        next = self.get_redirect_url()
        if next in (request.path, request.build_absolute_uri()):
            next = reverse('index')
        return redirect(set_query_parameter(next, 'logout', '1'))


class PasswordResetView(PageMixin, FormView):
    page_key = 'password_reset'
    template_name = 'auth/password_reset.html'
    email_template_name = 'auth/password_reset_email.html'
    form_class = PasswordResetForm
    token_generator = default_token_generator
    from_email = settings.DEFAULT_FROM_EMAIL
    reset_urlpattern_name = 'auth_password_reset_confirm'

    @method_decorator(ratelimit(key='ip', rate='12/d', method='POST'))
    @method_decorator(ratelimit(key='post:email', rate='12/d', method='POST'))
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def get_default_page_title(self):
        return _('Récupération de mot de passe')

    def success_message(self):
        messages.info(
            self.request,
            _('Vérifiez la boite de réception de votre adresse de messagerie.'),
        )

    def get_reset_url(self, user):
        path = reverse(
            self.reset_urlpattern_name,
            kwargs={
                'uidb36': int_to_base36(user.id),
                'token': self.token_generator.make_token(user),
            },
        )
        return self.request.scheme + '://' + get_current_site(self.request).domain + path

    def get_email_subject(self, user):
        return _('Récupération de mot de passe %s') % self.request.get_host()

    def get_email_body(self, user):
        return render_to_string(
            self.email_template_name,
            {
                'user': user,
                'reset_url': self.get_reset_url(user),
                'domain': self.request.get_host(),
            },
        )

    def send_email(self, user):
        msg = EmailMessage(
            subject=self.get_email_subject(user),
            body=self.get_email_body(user),
            from_email=self.from_email,
            to=(user.email,),
        )
        msg.send(fail_silently=not settings.DEBUG)

    def form_valid(self, form):
        email = form.cleaned_data['email']
        users = User.objects.filter(email__iexact=email)
        for user in users:
            self.send_email(user)
        self.success_message()
        return redirect(self.request.path)


class PasswordResetConfirmView(PageMixin, FormView):
    page_key = 'password_reset_confirm'
    template_name = 'auth/password_reset_confirm.html'
    form_class = SetPasswordForm
    token_generator = default_token_generator

    def get_default_page_title(self):
        return _('Récupération de mot de passe')

    def dispatch(self, request, *args, **kwargs):
        try:
            self.user = User.objects.get(id=base36_to_int(kwargs['uidb36']))
        except (ValueError, User.DoesNotExist):
            self.user = None

        if self.user is not None and self.token_generator.check_token(self.user, kwargs['token']):
            self.valid_link = True
        else:
            self.valid_link = False
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        # set the next page to the home page to avoid confusion
        # because the user will be redirected to the last visited page
        # (this page) which will be invalid once the user has logged in
        return reverse('auth_login') + '?next=/'

    def success_message(self):
        messages.info(self.request, _('Votre mot de passe a été modifié.'))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.user
        return kwargs

    def form_valid(self, form):
        form.save()
        self.success_message()
        return redirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['valid_link'] = self.valid_link
        return context


login = LoginView.as_view()
logout = LogoutView.as_view()
register = RegisterView.as_view()
password_reset = PasswordResetView.as_view()
password_reset_confirm = PasswordResetConfirmView.as_view()
