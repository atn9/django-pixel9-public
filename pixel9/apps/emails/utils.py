from django.core.mail import EmailMultiAlternatives, get_connection
from django.template.loader import render_to_string

from .css_inliners import inline_css as _inline_css


def send_mail(
    subject,
    message,
    from_email,
    recipient_list,
    fail_silently=False,
    auth_user=None,
    auth_password=None,
    connection=None,
    html_message=None,
):
    'from django 1.7'
    connection = connection or get_connection(username=auth_user, password=auth_password, fail_silently=fail_silently)

    msg = EmailMultiAlternatives(
        subject,
        message,
        from_email=from_email,
        to=recipient_list,
        connection=connection,
    )

    if html_message:
        msg.attach_alternative(html_message, 'text/html')

    msg.send()


def render_html_email(template, context, inline_css=True):
    html = render_to_string(template, context)
    if inline_css:
        html = _inline_css(html)
    return html
