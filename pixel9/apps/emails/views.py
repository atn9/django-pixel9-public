from django import http
from django.views.generic import TemplateView

from pixel9.apps.auth.mixins import RequestPassesTestMixin

from .utils import render_html_email


class RenderHtmlEmailView(RequestPassesTestMixin, TemplateView):
    def test_request_function(__, request):  # noqa: PT019
        return request.user.is_superuser

    def get_template_names(self):
        return ['%s.html' % self.args[0]]

    def render_to_response(self, context, **response_kwargs):
        return http.HttpResponse(render_html_email(self.get_template_names(), context))


render_email = RenderHtmlEmailView.as_view()
