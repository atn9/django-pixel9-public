from django.urls import re_path

from . import views

urlpatterns = [
    re_path(r'render/(.+)$', views.render_email, name='emails_render_email'),
]
