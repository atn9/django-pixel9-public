import logging
import operator
import re

from django.conf import settings

PSEUDO_TO_IGNORE = [
    ':hover',
    ':visited',
    ':active',
    ':focus',
    ':target',
    ':before',
    ':after',
]

DEFAULT_PARSER = getattr(settings, 'CSSINLINER_DEFAULT_PARSER', 'html5lib')


def unique_attr(obj_list, attr, reversed=False):
    """return a list with unique attribute and preserve the order
    of the original list"""
    seen = set()
    unique = []

    if reversed:
        obj_list = obj_list[::-1]

    for obj in obj_list:
        if getattr(obj, attr) not in seen:
            unique.append(obj)
            seen.add(getattr(obj, attr))

    if reversed:
        unique = unique[::-1]
    return unique


def parse_rules(soup):
    import cssutils

    rules = []

    for style_tag in soup.find_all('style'):
        # If we have a media attribute whose value is anything other than
        # 'screen', ignore the ruleset.
        media = style_tag.get('media')
        if media and media != 'screen':
            continue

        sheet = cssutils.parseString(style_tag.text)
        for rule in sheet:
            if rule.type == rule.STYLE_RULE:
                for selector in rule.selectorList:
                    ignore = False

                    for pseudo in PSEUDO_TO_IGNORE:
                        if pseudo in selector.selectorText:
                            ignore = True

                    if not ignore:
                        rules.append(
                            {
                                'specificity': selector.specificity,
                                'selector': selector.selectorText,
                                'prop_list': list(rule.style),
                            }
                        )

    rules.sort(key=operator.itemgetter('specificity'))
    return rules


def inline_css(html, parser=None):
    from bs4 import BeautifulSoup
    import cssutils

    current_log_level = cssutils.log.getEffectiveLevel()
    cssutils.log.setLevel(logging.FATAL)

    if parser is None:
        parser = DEFAULT_PARSER

    soup = BeautifulSoup(html, parser)
    inline_styles = {}
    styles = {}

    for rule in parse_rules(soup):
        for item in soup.select(rule['selector']):
            item_id = id(item)

            # gather style for this item
            if item_id in styles:
                styles[item_id][1] = styles[item_id][1] + rule['prop_list']
            else:
                styles[item_id] = [item, rule['prop_list']]

            # gather inline style for this item
            if item.get('style') and item_id not in inline_styles:
                style = cssutils.css.CSSStyleRule(style=item.get('style')).style
                inline_styles[item_id] = [item, list(style)]

    # inline styles need an higher specificity,
    # put them after all the other properties
    for item_id in inline_styles:
        if item_id in styles:
            styles[item_id][1] = styles[item_id][1] + inline_styles[item_id][1]
        else:
            styles[item_id] = inline_styles[item_id]

    for item, prop_list in list(styles.values()):
        style = ''
        # properties are ordered by specificity, keep only the one with the
        # higher specificity (the last one) for each unique property name
        for prop in unique_attr(prop_list, 'name', reversed=True):
            # cssutils normalize hexadecimal colors: #333333 becomes #333
            # some email clients ignore hexadecimal values with only 3 characters
            value = re.sub(r'#([0-9a-fA-F]{3})\b', r'#\1\1', prop.value)
            important = ''
            if prop.priority:
                important = ' !important'

            prop_text = '{name}:{value}{important}; '.format(
                name=prop.name,
                value=value,
                important=important,
            )
            style += prop_text
        item['style'] = style.strip()

    cssutils.log.setLevel(current_log_level)

    return str(soup)
