import hashlib
import json
import logging
import urllib.error
import urllib.parse
import urllib.request

from django.conf import settings
from django.core.cache import cache
from django.utils.encoding import smart_str
from django.utils.module_loading import import_string

from . import cssinliner as _cssinliner

logger = logging.getLogger(__name__)

CSS_INLINERS = getattr(
    settings,
    'CSS_INLINERS',
    [
        # robust and predictable, no api
        'pixel9.apps.emails.css_inliners.cssinliner',
        # robust and predictable, no api
        #'pixel9.apps.emails.css_inliners.cm_inliner',
        # robust and predictable, requires a mailchimp api key
        #'pixel9.apps.emails.css_inliners.mailchimp_api',
        # ok but does a lot of small unexpected changes, open source
        # text-align: => align="", inline all media query content
        #'pixel9.apps.emails.css_inliners.premailer_api',
        # premailer fork, https://github.com/zurb/ink/issues/59
        # remove style tags content
        #'pixel9.apps.emails.css_inliners.ink_inliner',
    ],
)

USE_CACHE = getattr(settings, 'CSS_INLINERS_USE_CACHE', False)
CACHE_TIMEOUT = getattr(settings, 'CSS_INLINERS_CACHE_TIMEOUT', 60 * 60 * 24 * 30)


class CSSInlinerError(Exception):
    pass


def inline_css(html, use_cache=None, cache_timeout=None):
    if use_cache is None:
        use_cache = USE_CACHE

    if cache_timeout is None:
        cache_timeout = CACHE_TIMEOUT

    inlined_html = None
    if use_cache:
        cache_key = 'cssinliner' + hashlib.md5(smart_str(html)).hexdigest()  # noqa S324
        inlined_html = cache.get(cache_key)

    if not inlined_html:
        for path in CSS_INLINERS:
            inline_func = import_string(path)
            try:
                inlined_html = inline_func(html)
                break
            except Exception:
                logger.exception(f'CSS Inliner {path} failed')
        else:
            raise CSSInlinerError

    if use_cache:
        cache.set(cache_key, inlined_html, cache_timeout)

    return inlined_html


def cssinliner(html):
    return _cssinliner.inline_css(html)


def cm_inliner(html):
    url = 'http://inliner.cm/inline.php'
    request = urllib.request.Request(  # noqa: S310
        url,
        urllib.parse.urlencode(
            {
                'code': smart_str(html),
            }
        ),
    )
    response = urllib.request.urlopen(request, timeout=2)  # noqa: S310
    logger.info('POST: ' + url)
    data = json.loads(response.read())
    return data['HTML']


def ink_inliner(html):
    url = 'http://zurb.com/ink/skate-proxy.php'
    request = urllib.request.Request(  # noqa: S310
        url,
        urllib.parse.urlencode(
            {
                'source': smart_str(html),
            }
        ),
    )
    response = urllib.request.urlopen(request, timeout=2)  # noqa: S310
    logger.info('POST: ' + url)
    data = json.loads(response.read())
    return data['html']


def premailer_api(html):
    url = 'http://premailer.dialect.ca/api/0.1/documents'
    request = urllib.request.Request(  # noqa: S310
        url,
        urllib.parse.urlencode(
            {
                'html': smart_str(html),
            }
        ),
    )
    response = urllib.request.urlopen(request, timeout=2)  # noqa: S310
    logger.info('POST: ' + url)

    headers = dict(list(response.info().items()))
    response2 = urllib.request.urlopen(headers['location'], timeout=2)  # noqa: S310
    return response2.read()


def mailchimp_api(html):
    datacenter = settings.MAILCHIMP_API_KEY.split('-')[1]
    url = f'https://{datacenter}.api.mailchimp.com/2.0/helper/inline-css.json'

    request = urllib.request.Request(  # noqa: S310
        url,
        urllib.parse.urlencode(
            {
                'html': smart_str(html),
                'apikey': settings.MAILCHIMP_API_KEY,
            }
        ),
    )
    response = urllib.request.urlopen(request, timeout=2)  # noqa: S310
    logger.info('POST: ' + url)

    data = json.loads(response.read())
    return data['html']
