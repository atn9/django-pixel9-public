from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'pixel9.apps.core'
    label = 'pixel9_core'
