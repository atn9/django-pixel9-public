from decimal import Decimal

from django import template
from django.conf import settings
from django.utils.formats import localize
from django.utils.translation import get_language

register = template.Library()


@register.filter()
def currency(value, arg=None):
    if not value:
        value = Decimal('0.00')
    value = Decimal(value).quantize(Decimal('0.01'))
    return currency_format(value, arg)


def currency_format(price, lang=None):
    if lang is None:
        lang = get_language()
    lprice = localize(price)
    return settings.CURRENCIES_FORMAT[lang] % lprice


@register.filter()
def cents(price):
    if isinstance(price, Decimal):
        return int(price * 100)


@register.filter()
def from_cents(price):
    if isinstance(price, int):
        price = Decimal(price)
        price = price / Decimal('100')
    return price
