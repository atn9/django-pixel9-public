from django.template import Library
from django.template.defaultfilters import stringfilter

register = Library()


@register.filter
@stringfilter
def escape_dots(value):
    return value.replace('.', r'\.')
