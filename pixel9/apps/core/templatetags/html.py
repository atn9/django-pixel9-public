import re

from django.template import Library
from django.template.defaultfilters import stringfilter
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

from pixel9.utils.html import json_script as _json_script
from pixel9.utils.html import meta_description_filter

register = Library()


@register.filter
@stringfilter
def meta_description(value):
    return meta_description_filter(value)


@register.filter(is_safe=True)
def json_script(value, element_id):
    """
    Output value JSON-encoded, wrapped in a <script type="application/json">
    tag.
    """
    return _json_script(value, element_id)


@register.simple_tag
@register.filter
def obfuscate_email(email, linktext=None, autoescape=None):
    """
    Given a string representing an email address,
        returns a mailto link with rot13 JavaScript obfuscation.

    Accepts an optional argument to use as the link text;
        otherwise uses the email address itself.
    """
    if autoescape:
        esc = conditional_escape
    else:

        def esc(x):
            return x

    email = re.sub('@', '\\\\100', re.sub(r'\.', '\\\\056', esc(email))).encode('rot13')

    if linktext:
        linktext = esc(linktext).encode('rot13')
    else:
        linktext = email

    rotten_link = """<script type="text/javascript">document.write \
("<n uers=\\\"znvygb:{}\\\">{}<\\057n>".replace(/[a-zA-Z]/g, \
function(c){{return String.fromCharCode((c<="Z"?90:122)>=\
(c=c.charCodeAt(0)+13)?c:c-26);}}));</script>""".format(email, linktext)
    return mark_safe(rotten_link)


obfuscate_email.needs_autoescape = True
