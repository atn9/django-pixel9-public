from django import template
from django.template.defaultfilters import stringfilter
from django.utils.encoding import force_str
from django.utils.safestring import mark_safe
from markdown import markdown

register = template.Library()


class MarkDownNode(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        return markdown(self.nodelist.render(context))


@register.tag('markdown')
def markdown_tag(parser, token):
    nodelist = parser.parse(('endmarkdown',))
    # need to do this otherwise we get big fail
    parser.delete_first_token()
    return MarkDownNode(nodelist)


@register.filter('markdown', is_safe=True)
@stringfilter
def markdown_filter(value):
    return mark_safe(markdown(force_str(value)))
