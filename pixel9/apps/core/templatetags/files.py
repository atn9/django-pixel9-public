from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def static(path):
    if 'django.contrib.staticfiles' in settings.INSTALLED_APPS:
        from django.contrib.staticfiles.storage import staticfiles_storage

        url = staticfiles_storage.url(path)
    else:
        url = settings.STATIC_URL + path
    return url
