"""
http://github.com/simonw/django-html
"""
from django import template

register = template.Library()


@register.filter
def is_checkbox(field):
    return field.field.widget.__class__.__name__.lower() == 'checkboxinput'


class WidgetNode(template.Node):
    def __init__(self, field_var, extra_attrs):
        self.field_var = field_var
        self.extra_attrs = extra_attrs

    def render(self, context):
        field = template.resolve_variable(self.field_var, context)
        # Caling bound_field.as_widget() returns the HTML, but we need to
        # intercept this to manipulate the attributes - so we have to
        # duplicate the logic from as_widget here.
        widget = field.field.widget

        attrs = dict(self.extra_attrs) or {}  # create a copy
        for k, attr in list(attrs.items()):
            if not isinstance(attr, str):
                attrs[k] = attr.resolve(context)

        auto_id = field.auto_id
        if auto_id and 'id' not in attrs and 'id' not in widget.attrs:
            attrs['id'] = auto_id

        required = field.field.required
        if required and 'required' not in attrs and 'required' not in widget.attrs:
            attrs['required'] = ''

        if not field.form.is_bound:
            data = field.form.initial.get(field.name, field.field.initial)
            if callable(data):
                data = data()
        else:
            data = field.data
        html = widget.render(field.html_name, data, attrs=attrs)
        return html


@register.tag('widget')
def do_widget(parser, token):
    field_var, extra_attrs = parse_tag(parser, token)
    return WidgetNode(field_var, extra_attrs)


def parse_tag(parser, token):
    # Can't use split_contents here as we need to process 'class="foo"' etc
    bits = token.split_contents()
    if len(bits) == 1:
        raise template.TemplateSyntaxError('%r tag takes arguments' % bits[0])
    field_var = bits[1]
    extra_attrs = {}
    if len(bits) > 2:
        # There are extra name="value" arguments to consume
        extra_attrs = split_args(bits[2:])
        for k, attr in list(extra_attrs.items()):
            extra_attrs[k] = parser.compile_filter(attr)

    return field_var, extra_attrs


def split_args(args):
    """
    Split a list of argument strings into a dictionary where each key is an
    argument name.

    An argument looks like ``crop``, ``crop="some option"`` or ``crop=my_var``.
    Arguments which provide no value get a value of ``None``.
    """
    if not args:
        return {}
    # Separate out the key and value for each argument.
    args_dict = {}
    for arg in args:
        split_arg = arg.split('=', 1)
        value = len(split_arg) > 1 and split_arg[1] or None
        args_dict[split_arg[0]] = value
    return args_dict
