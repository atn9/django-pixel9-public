from django.contrib import admin
from django.contrib.contenttypes.models import ContentType


@admin.register(ContentType)
class ContentTypeAdmin(admin.ModelAdmin):
    search_fields = (
        'app_label',
        'model',
    )
    list_filter = ('app_label',)
    list_display = ('app_label', 'model')
