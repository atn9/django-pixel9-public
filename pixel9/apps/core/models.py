from django.utils.translation import gettext_lazy as _

from pixel9.db import models


class Address(models.Model):
    name = models.CharField(_('nom'), max_length=200, blank=True)
    line1 = models.CharField(_('adresse ligne 1'), max_length=200, blank=True)
    line2 = models.CharField(_('adresse ligne 2'), max_length=200, blank=True)
    city = models.CharField(_('ville'), max_length=200, blank=True)
    state = models.CharField(_('province'), max_length=100, blank=True)
    country = models.CountryField(_('pays'), blank=True)
    postcode = models.CharField(_('code postale'), max_length=10, blank=True)
    created_at = models.CreationDateTimeField()
    updated_at = models.ModificationDateTimeField()

    class Meta:
        abstract = True
