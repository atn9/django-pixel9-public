from django import template
from django.utils import translation

from pixel9.utils.markup import custom_markdown


def get_page(page_key, site=None):
    from pixel9.apps.pages.models import StandardPage

    queryset = StandardPage.current_site.filter(page_key=page_key, language=translation.get_language())
    try:
        page = queryset[0]
    except IndexError:
        return None
    return page


def process_html_content(raw_content):
    html_content = template.Template(raw_content).render(template.Context({}))
    return custom_markdown(html_content)
