from django.conf.urls import patterns, url

urlpatterns = patterns(
    'pixel9.apps.pages.views',
    url(r'^(.+)$', 'default', name='page'),
)
