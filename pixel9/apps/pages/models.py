from django.conf import settings
from django.contrib.sites.managers import CurrentSiteManager
from django.core.cache import cache
from django.template.defaultfilters import slugify
from django.urls import NoReverseMatch, reverse
from django.utils import translation
from django.utils.safestring import mark_safe

from pixel9.apps.pages.utils import process_html_content
from pixel9.db import models
from pixel9.utils.html import meta_description_filter

_ = translation.gettext_lazy

MARKUP_CHOICES = (
    (0, 'html'),
    (1, 'markdown'),
)

SITE_CHOICES = getattr(settings, 'SITE_CHOICES', [])


class BasePage(models.Model):
    name = models.CharField(_('nom'), max_length=100)
    title = models.CharField(_('titre'), max_length=100, blank=True)
    slug = models.SlugField(default='', blank=True, editable=False)
    static_slug = models.SlugField(default='', blank=True)
    raw_content = models.TextField(_('contenu'), blank=True)
    custom_description = models.CharField(
        _('meta description'),
        max_length=255,
        blank=True,
        help_text=_('Si laissé vide, la meta description sera généré a partir du contenu de la page.'),
    )
    page_key = models.CharField(
        _('identifiant'),
        max_length=100,
        blank=True,
        default='',
        help_text=_('Modifier ce champ risque de briser certaines fonctionnalités.'),
    )
    language = models.LanguageField(_('langue'))
    site = models.ForeignKey('sites.Site', default=settings.SITE_ID, on_delete=models.CASCADE)
    created_at = models.CreationDateTimeField()
    updated_at = models.ModificationDateTimeField()
    search_weight = models.PositiveSmallIntegerField(_('importance'), default=10)
    is_searchable = models.BooleanField(_('peut être cherché'), default=True)

    default_template = 'base.html'
    result_template = 'pages/page_result.html'

    class Meta:
        ordering = ['name']

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)[:49]

        if not self.static_slug:
            self.static_slug = self.slug

        if self.custom_description:
            self.custom_description = meta_description_filter(self.custom_description)

        cache.delete(self.html_cache_key)
        cache.delete(self.meta_desc_cache_key)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

    def get_translation(self, language):
        try:
            return type(self).objects.filter(page_key=self.page_key, language=language)[0]
        except IndexError:
            return

    @property
    def url(self):
        with translation.override(self.language):
            url = None
            if self.page_key:
                try:
                    url = reverse(self.page_key)
                except NoReverseMatch:
                    pass
            if url is None:
                try:
                    url = reverse('page', args=[self.slug])
                except NoReverseMatch:
                    return
            return url

    @property
    def meta_desc_cache_key(self):
        return f'meta_description.{self._meta.app_label}.{self._meta.model_name}.{self.pk}'

    @property
    def html_cache_key(self):
        return f'html_content.{self._meta.app_label}.{self._meta.model_name}.{self.pk}'

    @property
    def meta_description(self):
        meta_desc_cache = cache.get(self.meta_desc_cache_key)
        if meta_desc_cache is not None:
            return meta_desc_cache
        meta_desc = self.custom_description or meta_description_filter(self.get_content())
        cache.set(self.meta_desc_cache_key, meta_desc)
        return meta_desc

    def get_content(self):
        html_cache = cache.get(self.html_cache_key)
        if html_cache is not None:
            return html_cache
        html_content = process_html_content(self.raw_content)
        cache.set(self.html_cache_key, html_content)
        return html_content

    @property
    def content(self):
        'backward compatibility'
        return mark_safe(self.get_content())


class Page(BasePage):
    objects = models.Manager()
    current_site = CurrentSiteManager()

    class Meta:
        abstract = True


class StandardPage(Page):
    class Meta:
        verbose_name = _('page')
        ordering = ['name']


class PageContent(models.Model):
    page = models.ForeignKey('BasePage', related_name='contents', on_delete=models.CASCADE)
    title = models.CharField(_('titre'), max_length=100, blank=True, default='')
    raw_content = models.TextField(_('contenu'), blank=True)
    type = models.ForeignKey(
        'PageContentType',
        verbose_name=_('type de contenu'),
        related_name='page_contents',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )
    position = models.PositionField(collection=('page', 'type'))

    class Meta:
        verbose_name = _('contenu')
        ordering = ['page', 'type', 'position']

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        cache.delete(self.html_cache_key)

    def __str__(self):
        text = self.title or self.raw_content[:50]
        if self.type:
            return f'[{self.type!s}] - {text}'
        return text

    @property
    def html_cache_key(self):
        return f'html_content.{self._meta.app_label}.{self._meta.model_name}.{self.pk}'

    def get_content(self):
        html_cache = cache.get(self.html_cache_key)
        if html_cache:
            return html_cache
        html_content = process_html_content(self.raw_content)
        cache.set(self.html_cache_key, html_content)
        return html_content

    @property
    def content(self):
        'backward compatibility'
        return mark_safe(self.get_content())


class PageContentType(models.Model):
    name = models.CharField(_('nom'), max_length=100, default='')
    slug = models.SlugField(_('identifiant'), default='')

    class Meta:
        verbose_name = _('type de contenu')
        verbose_name_plural = _('types de contenu')

    def __str__(self):
        return self.name
