from django import http
from django.shortcuts import render
from django.utils.translation import get_language

from pixel9.apps.pages.models import StandardPage


def default(request, slug, language=None):
    if not language:
        language = get_language()
    try:
        page = StandardPage.objects.filter(slug=slug, language=get_language())[0]
    except IndexError as e:
        raise http.Http404 from e
    return render(request, 'base.html', {'page': page})


def by_key(request, key, language=None, template='base.html'):
    if not language:
        language = get_language()
    try:
        page = StandardPage.objects.filter(page_key=key, language=get_language())[0]
    except IndexError as e:
        raise http.Http404 from e
    return render(request, template, {'page': page})
