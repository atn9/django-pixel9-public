import datetime
import re

import django.core.validators
import django.db.models.deletion
from django.db import migrations, models

import pixel9.db.fields


class Migration(migrations.Migration):
    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BasePage',
            fields=[
                (
                    'id',
                    models.AutoField(
                        verbose_name='ID',
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                (
                    'title',
                    models.CharField(max_length=100, verbose_name='titel', blank=True),
                ),
                ('slug', models.SlugField(default='', editable=False, blank=True)),
                ('static_slug', models.SlugField(default='', blank=True)),
                ('raw_content', models.TextField(verbose_name='content', blank=True)),
                (
                    'custom_description',
                    models.CharField(
                        help_text='Si laiss\xe9 vide, la meta description sera g\xe9n\xe9r\xe9 a partir du contenu de la page.',
                        max_length=255,
                        verbose_name='meta description',
                        blank=True,
                    ),
                ),
                (
                    'page_key',
                    models.CharField(
                        default='',
                        help_text='Modifier ce champ risque de briser certaines fonctionnalit\xe9s.',
                        max_length=100,
                        verbose_name='id',
                        blank=True,
                    ),
                ),
                (
                    'language',
                    pixel9.db.fields.LanguageField(default='fr', max_length=5, verbose_name='language'),
                ),
                (
                    'created_at',
                    pixel9.db.fields.CreationDateTimeField(default=datetime.datetime.now, editable=False, blank=True),
                ),
                (
                    'updated_at',
                    pixel9.db.fields.ModificationDateTimeField(
                        default=datetime.datetime.now, editable=False, blank=True
                    ),
                ),
                (
                    'search_weight',
                    models.PositiveSmallIntegerField(default=10, verbose_name='importance'),
                ),
                (
                    'is_searchable',
                    models.BooleanField(default=True, verbose_name='peut \xeatre cherch\xe9'),
                ),
            ],
            options={
                'ordering': ['name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PageContent',
            fields=[
                (
                    'id',
                    models.AutoField(
                        verbose_name='ID',
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    'title',
                    models.CharField(default='', max_length=100, verbose_name='titel', blank=True),
                ),
                ('raw_content', models.TextField(verbose_name='content', blank=True)),
                ('position', pixel9.db.fields.PositionField(default=-1)),
            ],
            options={
                'ordering': ['page', 'type', 'position'],
                'verbose_name': 'content',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PageContentType',
            fields=[
                (
                    'id',
                    models.AutoField(
                        verbose_name='ID',
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    'name',
                    models.CharField(default='', max_length=100, verbose_name='name'),
                ),
                ('slug', models.SlugField(default='', verbose_name='id')),
            ],
            options={
                'verbose_name': 'type de contenu',
                'verbose_name_plural': 'types de contenu',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Redirection',
            fields=[
                (
                    'id',
                    models.AutoField(
                        verbose_name='ID',
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                (
                    'from_url',
                    models.CharField(
                        help_text='Cette url sera redirig\xe9 vers la page actuel.',
                        max_length=255,
                        verbose_name='url \xe0 rediriger',
                        db_index=True,
                        validators=[
                            django.core.validators.RegexValidator(
                                re.compile('^/'),
                                'Ce champ doit commencer par un "/"',
                                'invalid',
                            )
                        ],
                    ),
                ),
                (
                    'created_at',
                    pixel9.db.fields.CreationDateTimeField(default=datetime.datetime.now, editable=False, blank=True),
                ),
                (
                    'updated_at',
                    pixel9.db.fields.ModificationDateTimeField(
                        default=datetime.datetime.now, editable=False, blank=True
                    ),
                ),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='StandardPage',
            fields=[
                (
                    'basepage_ptr',
                    models.OneToOneField(
                        parent_link=True,
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        to='pages.BasePage',
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'page',
            },
            bases=('pages.basepage',),
        ),
        migrations.AddField(
            model_name='redirection',
            name='page',
            field=models.ForeignKey(
                related_name='redirections',
                to='pages.BasePage',
                on_delete=models.CASCADE,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pagecontent',
            name='page',
            field=models.ForeignKey(related_name='contents', to='pages.BasePage', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='pagecontent',
            name='type',
            field=models.ForeignKey(
                related_name='page_contents',
                on_delete=django.db.models.deletion.SET_NULL,
                verbose_name='type de contenu',
                blank=True,
                to='pages.PageContentType',
                null=True,
            ),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='basepage',
            name='site',
            field=models.ForeignKey(default=1, to='sites.Site', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
