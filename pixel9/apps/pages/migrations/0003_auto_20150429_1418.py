import django.contrib.sites.managers
import django.db.models.manager
from django.db import migrations, models


class Migration(migrations.Migration):
    import pixel9.db.fields

    dependencies = [
        ('pages', '0002_auto_20141208_1455'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pagecontent',
            options={
                'ordering': ['page', 'type', 'position'],
                'verbose_name': 'contenu',
            },
        ),
        migrations.AlterModelManagers(
            name='standardpage',
            managers=[
                ('objects', django.db.models.manager.Manager()),
                ('current_site', django.contrib.sites.managers.CurrentSiteManager()),
            ],
        ),
        migrations.AlterField(
            model_name='basepage',
            name='language',
            field=pixel9.db.fields.LanguageField(default='fr', max_length=5, verbose_name='langue'),
        ),
        migrations.AlterField(
            model_name='basepage',
            name='name',
            field=models.CharField(max_length=100, verbose_name='nom'),
        ),
        migrations.AlterField(
            model_name='basepage',
            name='page_key',
            field=models.CharField(
                default='',
                help_text='Modifier ce champ risque de briser certaines fonctionnalit\xe9s.',
                max_length=100,
                verbose_name='identifiant',
                blank=True,
            ),
        ),
        migrations.AlterField(
            model_name='basepage',
            name='raw_content',
            field=models.TextField(verbose_name='contenu', blank=True),
        ),
        migrations.AlterField(
            model_name='basepage',
            name='title',
            field=models.CharField(max_length=100, verbose_name='titre', blank=True),
        ),
        migrations.AlterField(
            model_name='pagecontent',
            name='raw_content',
            field=models.TextField(verbose_name='contenu', blank=True),
        ),
        migrations.AlterField(
            model_name='pagecontent',
            name='title',
            field=models.CharField(default='', max_length=100, verbose_name='titre', blank=True),
        ),
        migrations.AlterField(
            model_name='pagecontenttype',
            name='name',
            field=models.CharField(default='', max_length=100, verbose_name='nom'),
        ),
        migrations.AlterField(
            model_name='pagecontenttype',
            name='slug',
            field=models.SlugField(default='', verbose_name='identifiant'),
        ),
    ]
