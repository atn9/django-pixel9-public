from django.conf import settings
from django.utils.translation import get_language
from haystack import indexes

from pixel9.apps.pages.models import StandardPage


class PageIndex(indexes.SearchIndex):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField(model_attr='title')
    language = indexes.CharField(model_attr='language')

    def get_updated_field(self):
        return 'updated_at'


class StandardPageIndex(PageIndex, indexes.Indexable):
    def get_model(self):
        return StandardPage

    def index_queryset(self, using=None):
        if using and using in [lang[0] for lang in settings.LANGUAGES]:
            lang = using
        else:
            lang = get_language()
            if not lang:
                lang = settings.LANGUAGE_CODE
        return StandardPage.current_site.filter(is_searchable=True, language=lang)
