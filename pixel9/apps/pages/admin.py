from django.conf import settings
from django.contrib import admin
from django.utils import translation

from pixel9.apps.pages.models import PageContent, PageContentType, StandardPage

_ = translation.gettext_lazy


class PageContentInline(admin.StackedInline):
    model = PageContent


class PageAdmin(admin.ModelAdmin):
    inlines = [PageContentInline]
    list_display = ['name', 'get_title', 'page_key']
    search_fields = ['name', 'title', 'static_slug', 'slug']
    list_filter = tuple()
    readonly_fields = []  # PageAdmin.get_form !!!

    fieldsets = [
        (
            None,
            {
                'fields': [('name', 'title'), 'raw_content'],
            },
        ),
        (
            _('Options avancées'),
            {
                'classes': ('collapsable collapsed',),
                'fields': (
                    'custom_description',
                    'static_slug',
                    'page_key',
                    'search_weight',
                    'is_searchable',
                ),
            },
        ),
    ]
    if hasattr(settings, 'USE_SITES') and settings.USE_SITES:
        fieldsets[0][1]['fields'].append('site')
        list_filter += ('site',)
        list_display += ('get_site',)

    if settings.USE_I18N and len(settings.LANGUAGES) > 1:
        fieldsets[0][1]['fields'].append('language')
        list_filter += ('language',)
        list_display.insert(2, 'language')

    @admin.display(
        description='titre',
        ordering='title',
    )
    def get_title(self, obj):
        return obj.title[:45]

    @admin.display(
        description='site',
        ordering='site',
    )
    def get_site(self, obj):
        return obj.site and obj.site.name or ''


@admin.register(StandardPage)
class StandardPageAdmin(PageAdmin):
    ordering = ('-updated_at',)

    def view_on_site(self, obj):
        return obj.url


admin.site.register(PageContent)
admin.site.register(PageContentType)
