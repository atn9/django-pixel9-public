from haystack.management.commands.rebuild_index import Command as RebuildIndexCommand


class Command(RebuildIndexCommand):
    leave_locale_alone = True
