from haystack.management.commands.clear_index import Command as ClearIndexCommand


class Command(ClearIndexCommand):
    leave_locale_alone = True
