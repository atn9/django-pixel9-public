from haystack.management.commands.update_index import Command as UpdateIndexCommand


class Command(UpdateIndexCommand):
    leave_locale_alone = True
