from django.apps import AppConfig


class SearchConfig(AppConfig):
    name = 'pixel9.apps.search'
    label = 'pixel9_search'
