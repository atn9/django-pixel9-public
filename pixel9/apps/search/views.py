from haystack.views import search_view_factory


def search(request):
    return search_view_factory(
        template='search/results.html',
    )(request)
