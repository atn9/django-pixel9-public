from django.utils.translation import get_language
from haystack import routers


class MultilingualRouter(routers.BaseRouter):
    def for_read(self, **hints):
        lang = get_language()
        if lang:
            lang = lang[:2]
        return lang

    def for_write(self, **hints):
        lang = get_language()
        if lang:
            lang = lang[:2]
        return lang
