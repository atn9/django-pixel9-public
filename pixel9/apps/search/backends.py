from haystack.backends.whoosh_backend import WhooshEngine, WhooshSearchBackend
from whoosh.analysis import (
    CharsetFilter,
    LowercaseFilter,
    PyStemmerFilter,
    RegexTokenizer,
    StemmingAnalyzer,
    StopFilter,
    default_pattern,
)
from whoosh.fields import TEXT
from whoosh.support.charset import accent_map

FRENCH_STOP_WORDS = frozenset(
    (
        'je',
        'tu',
        'il',
        'nous',
        'vous',
        'ils',
        'sont',
        'au',
        'aux',
        'avec',
        'ce',
        'ces',
        'dans',
        'de',
        'des',
        'du',
        'en',
        'et',
        'eux',
        'la',
        'that',
        'le',
        'leur',
        'lui',
        'ma',
        'mes',
        'ne',
        'nos',
        'notre',
        'on',
        'ou',
        'par',
        'pas',
        'pour',
        'qu',
        'que',
        'qui',
        'sa',
        'se',
        'ses',
        'son',
        'sur',
        'ta',
        'te',
        'tes',
        'toi',
        'ton',
        'un',
        'une',
        'vos',
        'votre',
        'c',
        'd',
        'j',
        'l',
        'a',
        'à',
        'n',
        's',
        'y',
        't',
    )
)


def FrenchStemmingAnalyzer(
    expression=default_pattern,
    stoplist=FRENCH_STOP_WORDS,
    minsize=2,
    maxsize=None,
    gaps=False,
    ignore=None,
    cachesize=50000,
):
    ret = RegexTokenizer(expression=expression, gaps=gaps)
    chain = ret | LowercaseFilter()
    if stoplist is not None:
        chain = chain | StopFilter(stoplist=stoplist, minsize=minsize, maxsize=maxsize)
    return chain | PyStemmerFilter(lang='french', ignore=ignore, cachesize=cachesize)


FrenchStemmingAnalyzer.__inittypes__ = dict(expression=str, gaps=bool, stoplist=list, minsize=int, maxsize=int)


class FrenchWhooshSearchBackend(WhooshSearchBackend):
    def build_schema(self, fields):
        """replace accented character"""
        name, schema = super().build_schema(fields)
        schema.remove('text')
        schema.add(
            'text',
            TEXT(
                stored=True,
                analyzer=FrenchStemmingAnalyzer() | CharsetFilter(accent_map),
                field_boost=fields['text'].boost,
            ),
        )
        return (name, schema)


class EnglishWhooshSearchBackend(WhooshSearchBackend):
    def build_schema(self, fields):
        """replace accented character"""
        name, schema = super().build_schema(fields)
        schema.remove('text')
        schema.add(
            'text',
            TEXT(
                stored=True,
                analyzer=StemmingAnalyzer() | CharsetFilter(accent_map),
                field_boost=fields['text'].boost,
            ),
        )
        return (name, schema)


class FrenchWhooshEngine(WhooshEngine):
    backend = FrenchWhooshSearchBackend


class EnglishWhooshEngine(WhooshEngine):
    backend = EnglishWhooshSearchBackend
