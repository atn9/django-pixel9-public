from django.urls import path, re_path

from pixel9.apps.development import views

urlpatterns = [
    path('', views.index, name='dev_index'),
    path('forms/left', views.form, name='dev_form_left'),
    path(
        'forms/right',
        views.form,
        {'extra_context': {'html_class': 'right-aligned-form'}},
        name='dev_form_right',
    ),
    path(
        'forms/vertical',
        views.form,
        {'extra_context': {'html_class': 'vertical-form'}},
        name='dev_form_vertical',
    ),
    re_path(r'(?P<template>.*)$', views.default, name='dev_default'),
]
