from django.http import Http404
from django.shortcuts import redirect, render
from django.template import TemplateDoesNotExist, loader

from pixel9 import forms

CHOICES = (
    (1, 'choix 1'),
    (2, 'choix 2'),
    (3, 'choix 3'),
)


class DevelopmentForm(forms.Form):
    text = forms.CharField(help_text='Help text...')
    password = forms.CharField(widget=forms.PasswordInput)
    textarea = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Enter some text here ...'}))
    bool = forms.BooleanField()
    choice = forms.ChoiceField(choices=CHOICES)
    radio = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)
    date = forms.DateField()
    datetime = forms.DateTimeField()
    email = forms.EmailField()
    URL = forms.URLField()
    integer = forms.IntegerField()
    placeholder = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Some text ...'}))
    file = forms.FileField()
    multiple = forms.MultipleChoiceField(choices=CHOICES)
    hidden = forms.CharField(required=False, widget=forms.HiddenInput, initial='initial value')


def index(
    request,
    extra_context=None,
):
    template_path = 'development/index.html'
    return render(request, template_path, extra_context)


def default(request, template, extra_context=None):
    template_path = 'development/%s.html' % template
    try:
        loader.get_template(template_path)
    except TemplateDoesNotExist as e:
        raise Http404('The template "%s" doesn\'t exist' % template_path) from e
    return render(request, template_path, extra_context)


def form(request, form_class=DevelopmentForm, extra_context=None):
    form = form_class()
    if request.method == 'POST':
        form = form_class(request.POST)
        if form.is_valid():
            form = redirect(request.path)
    context = {
        'form': form,
    }
    if extra_context is not None:
        context.update(extra_context)
    return default(request, 'form', context)
