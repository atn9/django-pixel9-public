from django.conf.urls import patterns, url

urlpatterns = patterns(
    'pixel9.apps.contactus.views',
    url(r'^$', 'index', name='contactus'),
    url(r'^/success$', 'success', name='contactus_success'),
)
