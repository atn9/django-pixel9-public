from django.conf import settings
from django.contrib.sites.models import get_current_site
from django.core.mail.message import EmailMessage
from django.utils.translation import gettext_lazy as _

from pixel9 import forms


class ContactForm(forms.Form):
    name = forms.CharField(max_length=100, label=_('Votre nom'))
    email = forms.EmailField(label=_('Votre adresse électronique'))
    body = forms.CharField(label=_('Message'), widget=forms.Textarea())

    from_email = settings.DEFAULT_FROM_EMAIL
    recipient_list = [mail_tuple[1] for mail_tuple in settings.MANAGERS]

    def __init__(self, data=None, files=None, request=None, **kwargs):
        if request is None:
            raise TypeError("ContactForm instance require the keyword argument 'request'.")
        super().__init__(data=data, files=files, **kwargs)
        self.request = request

    def save(self):
        to = settings.DEBUG and [admin[1] for admin in settings.ADMINS] or self.recipient_list
        reply_to = '{}<{}>'.format(self.cleaned_data.get('name', ''), self.cleaned_data.get('email', ''))

        msg = EmailMessage(
            subject='[Contactez-nous] %s' % get_current_site(self.request).name,
            body=self.cleaned_data.get('body', ''),
            from_email=self.from_email,
            to=to,
            headers={'Reply-To': reply_to},
        )
        msg.send(fail_silently=not settings.DEBUG)


class HoneyPotContactForm(ContactForm):
    __name = forms.HoneyPotField()
