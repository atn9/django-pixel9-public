from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.translation import gettext as _

from pixel9.apps.contactus.forms import ContactForm
from pixel9.apps.pages.utils import get_page


def index(request, page=None, form_class=ContactForm, template='contactus/index.html'):
    page = page or get_page('contactus')
    if not page:
        page = {'title': _('Contactez-nous')}
    if request.method == 'POST':
        form = form_class(request.POST, request.FILES, request=request)
        if form.is_valid():
            if hasattr(form, 'save'):
                form.save()
            messages.info(
                request,
                _('Merci, nous tenterons de vous répondre dans les plus brefs délais.'),
            )
            return HttpResponseRedirect(request.path)
    else:
        form = form_class(request=request)

    context = {
        'page': page,
        'form': form,
    }
    return render(request, template, context)


def success(request, template_name='contactus/success.html'):
    return render(request, template_name)
