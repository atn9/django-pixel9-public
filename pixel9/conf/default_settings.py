import os
from pathlib import Path

import pixel9


def gettext_noop(s):
    return s


INSTALLED_APPS = (
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.redirects',
    'pixel9.apps.core',
    'treebeard',
    'compressor',
    'gunicorn',
    'sorl.thumbnail',
    'bootstrap3',
    'admin_auto_filters',
)

MIDDLEWARE = (
    'pixel9.middleware.http.XForwardedForMiddleware',  # required by RatelimitMiddleware
    'django_ratelimit.middleware.RatelimitMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'pixel9.middleware.stats.StatsIgnoreCookieMiddleware',
    'pixel9.middleware.http.UserTracebackMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
)

ADMINS = (('Etienne', 'etienne@px9.ca'),)
MANAGERS = (('Etienne', 'etienne@px9.ca'),)

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

###############
# DIRECTORIES #
###############
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.environ['BASE_DIR']
# A directory outside BASE_DIR to store permanent files (media, collected static, backups, ...).
STORAGE_DIR = os.environ.setdefault('STORAGE_DIR', Path(BASE_DIR) / 'storage')
PIXEL9_DIR = Path(pixel9.__file__).parent

LOCALE_PATHS = (
    str(Path(PIXEL9_DIR) / 'conf' / 'locale'),
    str(Path(PIXEL9_DIR) / '' / ''),
)
################
# STATIC FILES #
################
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'compressor.finders.CompressorFinder',
)
STATICFILES_DIRS = (str(Path(BASE_DIR) / 'static'),)
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'
STATIC_ROOT = os.environ.setdefault('STATIC_ROOT', str(Path(STORAGE_DIR) / 'static'))
STATIC_URL = '/static/'

################
# MEDIA FILES #
################
MEDIA_ROOT = os.environ.setdefault('MEDIA_ROOT', str(Path(STORAGE_DIR) / 'media'))
MEDIA_URL = '/media/'

##############
# COMPRESSOR #
##############
# COMPRESS_OFFLINE = True
COMPRESS_OUTPUT_DIR = 'min'
COMPRESS_CSS_FILTERS = [
    'compressor.filters.cssmin.CSSMinFilter',
    'compressor.filters.css_default.CssAbsoluteFilter',
]
COMPRESS_JS_FILTERS = ['compressor.filters.jsmin.JSMinFilter']

###########
# LOGGING #
###########
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {'require_debug_false': {'()': 'django.utils.log.RequireDebugFalse'}},
    'root': {
        'level': 'INFO',
        'handlers': ['stdout', 'mail_admins'],
    },
    'loggers': {
        'django': {
            'level': 'INFO',
            'handlers': [
                'null',
            ],
        },
        'rq.worker': {
            'level': 'INFO',
            'handlers': ['rq_console', 'mail_admins'],
            'propagate': False,
            'exclude': ['%(asctime)s'],
        },
        'rq_scheduler.scheduler': {
            'level': 'INFO',
            'handlers': ['stdout', 'mail_admins'],
            'propagate': False,
        },
    },
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
        },
        'stdout': {
            'class': 'logging.StreamHandler',
            'formatter': 'httpd',
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'console',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
            'filters': ['require_debug_false'],
        },
        'rq_console': {
            'level': 'DEBUG',
            'class': 'rq.utils.ColorizingStreamHandler',
            'formatter': 'rq_console',
            'exclude': ['%(asctime)s'],
        },
    },
    'formatters': {
        'httpd': {
            'format': '%(asctime)s, [%(levelname)s] %(filename)s:%(lineno)s %(message)s',
        },
        'simple': {
            'format': '%(levelname)s %(message)s',
        },
        'console': {
            '()': 'pixel9.utils.log.ColorizeFormatter',
            'format': '%(levelname)s %(message)s',
        },
        'rq_console': {
            'format': '%(message)s',
            'datefmt': '%H:%M:%S',
        },
    },
}


###############
# IDENTIFIERS #
###############
SITE_ID = 1

##########
# EMAILS #
##########
SERVER_EMAIL = DEFAULT_FROM_EMAIL = 'pixel9 <noreply@px9.ca>'
SEND_BROKEN_LINK_EMAILS = False

# SMTP
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# POSTMARK
POSTMARK_SENDER = 'noreply@px9.ca'
POSTMARK_API_KEY = '07dca761-ac0e-4cd0-8ff1-a29887b22fdd'
EMAIL_BACKEND = 'pixel9.mail.backends.postmark.EmailBackend'  # TRANSACTIONAL ONLY!!

#############
# TEMPLATES #
#############
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [str(Path(BASE_DIR) / 'templates')],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.csrf',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'pixel9.context_processors.google_analytics',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
        },
    },
]

#########
# FILES #
#########
FILE_UPLOAD_PERMISSIONS = 0o644
STATIC_FILES = (
    'favicon.ico',
    'robots.txt',
    'crossdomain.xml',
)

###############
# I18N & L10N #
###############
USE_I18N = True
USE_TZ = True
TIME_ZONE = 'America/Montreal'
LANGUAGE_CODE = 'fr'
LANGUAGES = (
    ('fr', gettext_noop('French')),
    # ('en', gettext_noop('English')),
)
CURRENCIES_FORMAT = {
    'fr': '%s $',
    'fr-ca': '%s $',
    'en-us': '$%s',
    'en-ca': '$%s',
    'en': '$%s',
}
LOCALE_PATHS = (str(Path(BASE_DIR) / 'locale'),)

#########
# CACHE #
#########
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'KEY_PREFIX': os.getenv('CACHE_PREFIX', ''),
        'TIMEOUT': 60 * 60 * 24 * 30,  # 1 month
        'OPTIONS': {
            'MAX_ENTRIES': 10000,
        },
    },
}

##############
# THUMBNAILS #
##############
THUMBNAIL_PREFIX = 'thumbs/'
THUMBNAIL_UPSCALE = False
THUMBNAIL_PRESERVE_FORMAT = True
THUMBNAIL_KEY_PREFIX = '{}:sorl-thumbnail'.format(os.getenv('CACHE_PREFIX', 'default'))
THUMBNAIL_BACKEND = 'pixel9.utils.thumbnails.CustomThumbnailBackend'
THUMBNAIL_STORAGE = 'pixel9.utils.thumbnails.OverwriteStorage'

############
# SESSIONS #
############
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
SESSION_COOKIE_NAME = 'session'
SESSION_COOKIE_AGE = 60 * 60 * 24 * 365  # 1 year
SESSION_COOKIE_HTTPONLY = True
SESSION_EXPIRE_AT_BROWSER_CLOSE = False

########
# AUTH #
########
AUTHENTICATION_BACKENDS = ('pixel9.apps.auth.backends.EmailAuthenticationBackend',)
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 7,
        },
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
PASSWORD_RESET_TIMEOUT = 60 * 60 * 24 * 10
LOGIN_URL = 'auth_login'
LOGIN_REDIRECT_URL = '/'
CONFIRMATION_URL = 'confirmation_required'

##########
# SEARCH #
##########
HAYSTACK_ROUTERS = [
    'pixel9.apps.search.routers.MultilingualRouter',
    'haystack.routers.DefaultRouter',
]

#######
# SSL #
#######
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

##########
# OEMBED #
##########
OEMBED_MAX_WIDTH = 400
OEMBED_MAX_HEIGHT = 400
OEMBED_REGEXES = [
    r'http://\S*.youtube.com/watch\S*',
    r'http://vimeo.com/\S*',
]

#############
# RATELIMIT #
#############
RATELIMIT_VIEW = 'pixel9.views.ratelimited'

############
# REDACTOR #
############
REDACTOR_OPTIONS = {
    'autoresize': True,  # not compatible with iframe
    'buttons': [
        'link',
        'formatting',
        'bold',
        'italic',
        'unorderedlist',
        'orderedlist',
        'image',
        'video',
        'table',
        'alignment',
        'horizontalrule',
        'html',
    ],
    'convertDivs': False,
    'css': '/static/admin/lib/redactor/redactor-iframe.css',
    'formattingTags': ['h2', 'h3', 'h4', 'h5', 'h6', 'p', 'blockquote'],
    'iframe': True,
    'lang': 'fr',
    'linkProtocol': False,
    'linkSize': False,
    'minHeight': 300,
    'paragraphy': False,
    'pastePlainText': True,
    'plugins': ['fullscreen'],  # lots of small bugs
    #'removeEmptyTags': False,
    'shortcuts': {
        'ctrl+m, meta+m': "this.execCommand('removeFormat', false)",
        'ctrl+b, meta+b': "this.execCommand('bold', false)",
        'ctrl+i, meta+i': "this.execCommand('italic', false)",
        'ctrl+h, meta+h': "this.execCommand('superscript', false)",
        'ctrl+l, meta+l': "this.execCommand('subscript', false)",
        'ctrl+k, meta+k': 'this.linkShow()',
        'ctrl+shift+7, meta+shift+7': "this.execCommand('insertorderedlist', false)",
        'ctrl+shift+8, meta+shift+8': "this.execCommand('insertunorderedlist', false)",
        'ctrl+u, meta+u': 'this.toggle(true)',
    },
}

BOOTSTRAP3 = {
    'set_placeholder': False,
    'required_css_class': 'required',
    'field_renderers': {
        'default': 'pixel9.bootstrap3.CustomFieldRenderer',
        'inline': 'pixel9.bootstrap3.CustomInlineFieldRenderer',
        'admin': 'pixel9.apps.admin.bootstrap3.AdminFieldRenderer',
        'admin_inline_tabular': 'pixel9.apps.admin.bootstrap3.AdminInlineTabularFieldRenderer',
    },
}

CRON_SCHEDULER_QUEUE = 'default'
