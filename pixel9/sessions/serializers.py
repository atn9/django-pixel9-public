"""
https://github.com/rconradharris/jsondate
"""
import datetime
import json

DATE_FMT = '%Y-%m-%d'
ISO8601_FMT = '%Y-%m-%dT%H:%M:%SZ'


def _datetime_encoder(obj):
    if isinstance(obj, datetime.datetime):
        return obj.strftime(ISO8601_FMT)
    elif isinstance(obj, datetime.date):
        return obj.strftime(DATE_FMT)

    raise TypeError


def _datetime_decoder(value):
    try:
        datetime_obj = datetime.datetime.strptime(value, ISO8601_FMT)
        return datetime_obj
    except (ValueError, TypeError):
        try:
            date_obj = datetime.datetime.strptime(value, DATE_FMT)
            return date_obj.date()
        except (ValueError, TypeError):
            return value


class DateAwareJSONDecoder(json.JSONDecoder):
    def decode(self, s):
        s = _datetime_decoder(s)
        return super().decode(s)


class DateAwareJSONSerializer:
    def dumps(self, obj):
        return json.dumps(obj, separators=(',', ':'), default=_datetime_encoder).encode('latin-1')

    def loads(self, data):
        return json.loads(data.decode('latin-1'), cls=DateAwareJSONDecoder)
