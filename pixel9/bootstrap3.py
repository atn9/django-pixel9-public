from bootstrap3.renderers import FieldRenderer, InlineFieldRenderer
from bootstrap3.utils import add_css_class


class CustomFieldRendererMixin:
    def get_form_group_class(self):
        "Don't add .has-success to every valid fields"
        form_group_class = super().get_form_group_class()
        if self.field.form.is_bound:
            form_group_class = form_group_class.replace('has-success', '')
        return form_group_class

    def list_to_class(self, html, klass):
        classes = add_css_class(klass, self.get_size_class())
        mapping = [
            ('<div>', f'<div class="{classes}">'),
        ]
        for k, v in mapping:
            html = html.replace(k, v)
        return html


class CustomFieldRenderer(CustomFieldRendererMixin, FieldRenderer):
    pass


class CustomInlineFieldRenderer(CustomFieldRendererMixin, InlineFieldRenderer):
    pass
