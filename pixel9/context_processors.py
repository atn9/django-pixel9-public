from django.conf import settings


def google_analytics(request):
    id = getattr(settings, 'GOOGLE_ANALYTICS_ID', None)
    show = True
    if not id:
        show = False
    elif request.COOKIES.get('stats_ignore'):
        show = False
    elif settings.DEBUG:
        show = False
    return {
        'google_analytics_id': id,
        'google_analytics_show': show,
    }


def clicky(request):
    id = getattr(settings, 'CLICKY_ID', None)
    show = True
    if not id:
        show = False
    elif request.COOKIES.get('stats_ignore'):
        show = False
    elif settings.DEBUG:
        show = False
    return {
        'clicky_id': id,
        'clicky_show': show,
    }
