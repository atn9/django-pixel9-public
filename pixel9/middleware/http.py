class XForwardedForMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if 'x-forwarded-for' in request.headers:
            request.META['HTTP_X_PROXY_REMOTE_ADDR'] = request.META['REMOTE_ADDR']
            parts = request.headers['x-forwarded-for'].split(',', 1)
            request.META['REMOTE_ADDR'] = parts[0]
        response = self.get_response(request)
        return response


class UserTracebackMiddleware:
    """
    Adds user to request context during request processing, so that they
    show up in the error emails.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_exception(self, request, exception):
        if request.user.is_authenticated:
            request.META['AUTH_USER_ID'] = str(request.user.id)
            request.META['AUTH_USER_EMAIL'] = str(request.user.email)
        else:
            request.META['AUTH_USER_ID'] = 'Anonymous User'
            request.META['AUTH_USER_EMAIL'] = 'Anonymous User'
