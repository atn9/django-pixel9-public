from django.db import connection


class SqlProfilerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        str(response)
        total_time = 0.0
        print('========================')  # noqa: T201
        print(request.path)  # noqa: T201
        for query in connection.queries:
            print('SQL: ' + query['sql'])  # noqa: T201
            print('Time: ' + query['time'])  # noqa: T201
            print()  # noqa: T201
            total_time += float(query['time'])
        print(f'{len(connection.queries)} queries took {total_time} seconds')  # noqa: T201
        print('========================')  # noqa: T201
        return response
