from datetime import datetime


class StatsIgnoreCookieMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if hasattr(request, 'user') and getattr(request.user, 'is_staff', False):
            if request.COOKIES.get('stats_ignore', None) is None:
                response.set_cookie('stats_ignore', '1', expires=datetime(2030, 0o1, 0o1))
        return response
