from django.forms import ValidationError
from django.forms.fields import *  # noqa: F403
from django.forms.fields import Field
from django.utils.translation import gettext_lazy as _

from pixel9.forms import widgets

EMPTY_VALUES = (None, '')


class InvalidHoneyPot(ValidationError):
    pass


class HoneyPotField(Field):
    widget = widgets.HoneyPotInput

    def clean(self, value):
        if self.initial in EMPTY_VALUES and value in EMPTY_VALUES or value == self.initial:
            return value
        raise InvalidHoneyPot(_('Formulaire invalide, un champ caché à été modifié.'))
