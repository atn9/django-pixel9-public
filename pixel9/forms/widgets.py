from django.forms.widgets import *  # noqa: F403
from django.forms.widgets import TextInput


class HoneyPotInput(TextInput):
    is_hidden = True

    def __init__(self, attrs=None, html_comment=False, *args, **kwargs):
        self.html_comment = html_comment
        super().__init__(attrs, *args, **kwargs)
        if 'class' not in self.attrs:
            self.attrs['style'] = 'display:none'
