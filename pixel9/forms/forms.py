from django.forms.forms import *  # noqa: F403
from django.forms.forms import Form


class Form(Form):
    default_css_class = 'field'
    required_css_class = 'field-required'
    error_css_class = 'has-error'
    # required_field_indicator = '&nbsp;<span class="required-field-indicator">*</span>'
    required_field_indicator = ''
    optional_field_indicator = ''
