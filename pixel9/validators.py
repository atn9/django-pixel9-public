import re

from django.core.validators import RegexValidator
from django.utils.translation import gettext_lazy as _

validate_startswith_slash = RegexValidator(re.compile(r'^/'), _('Ce champ doit commencer par un "/"'), 'invalid')
