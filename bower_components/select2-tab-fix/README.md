# select2-tab-fix

A jquery plugin that fixes the select2 packages tab index problem, where the tab index is broken when focus is set to a select2 input

### Install
Install with bower.
```
bower select2-tab-fix
```

### Include
Include these files
```
<script src="bower_components/select2-tab-fix/src/select2-tab-fix.js"></script>
```