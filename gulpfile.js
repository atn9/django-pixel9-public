var gulp = require('gulp');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var mainBowerFiles = require('main-bower-files');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');

var adminStatic = '/app/static/admin';

gulp.task('bower-files', function(){
  return gulp.src(mainBowerFiles(), {base:'./bower_components'}).pipe(gulp.dest(adminStatic + '/lib'));
});

gulp.task('admin:less', function(){
  return gulp.src(adminStatic + '/main.less')
    .pipe(plumber())
    .pipe(sourcemaps.init())

    .pipe(less())
    .pipe(autoprefixer("last 2 versions", "> 1%"))
    .pipe(cleanCSS())

    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(adminStatic));
});

gulp.task('admin:js:lib', function(){
  return gulp.src(adminStatic + '/lib/**/*.js', {base: adminStatic + '/lib'})
    .pipe(plumber())
    .pipe(sourcemaps.init())

    .pipe(uglify())

    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(adminStatic + '/dist'));
});

gulp.task('admin:js:app', function(){
  return gulp.src(adminStatic + '/js/**/*.js', {base: adminStatic + '/js'})
    .pipe(plumber())
    .pipe(sourcemaps.init())

    .pipe(uglify())

    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(adminStatic + '/dist'));
});


gulp.task('watch', function(){
  gulp.watch(adminStatic + '/**/*.less', ['admin:less']);
  gulp.watch(adminStatic + '/js/**/*.js', ['admin:js:app']);
  gulp.watch(adminStatic + '/lib/**/*.js', ['admin:js:lib']);
});

